/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************
 * DNS protocol handling                                                    *
 ****************************************************************************/

#include "dns.h"
#include "logging.h"
#include "util.h"

#include <tuple>

DNSLabel::DNSLabel(const std::string &other)
	: base(other.cbegin(), other.cend())
{}

std::ostream& operator << (std::ostream &stream, const DNSLabel &label)
{
	return stream << reinterpret_cast<const std::string&>(label);
}

DNSName::DNSName(const std::string &string)
{
	size_t start = 0, end = 0;
	while (start != std::string::npos) {
		end = string.find('.', start+1);
		emplace_back(&string.at(start), &string[(end == std::string::npos)?string.size():end]);
		start = (end == std::string::npos)?end:end+1;
	}
}

DNSName DNSName::make_reverse(const struct in6_addr &addr, unsigned short halfbytes)
{
	static const char * const table = "0123456789abcdef";
	std::vector<DNSLabel> result {"arpa", "ip6"};
	if (halfbytes > 32)
		throw std::invalid_argument("DNSName::make_reverse(): halfbytes must be <= 32");
	
	uint8_t bytes = halfbytes/2;
	int i;
	for (i = 0; i < 16 && i < bytes; ++i) {
		result.push_back({table[addr.s6_addr[i] >> 4]});
		result.push_back({table[addr.s6_addr[i] & 0xF]});
	}
	if (halfbytes & 0x1)
		result.push_back({table[addr.s6_addr[i] >> 4]});
	return {result.crbegin(), result.crend()};
}

bool DNSName::endswith(const DNSName &other) const
{
	if (other.size() > size())
		return false;
	for (auto it1 = crbegin(), it2 = other.crbegin(); it1 != crend() and it2 != other.crend(); ++it1, ++it2) {
		if (*it1 != *it2)
			return false;
	}
	return true;
}

std::ostream& operator << (std::ostream &stream, const DNSName &name)
{
	for (const auto &label: name) {
		stream << label << '.';
	}
	return stream;
}

class DNSNameTree final {
	struct node final {
		std::vector<node> children;
		DNSLabel label;
		uint16_t offset;
		node() {}
		node(node&&) noexcept;
		node(const DNSLabel&, uint16_t);
		node(DNSLabel&&, uint16_t);
	};
	node root;
public:
	DNSNameTree() {}
	DNSNameTree(DNSNameTree&&) noexcept;
	bool add(const DNSName &, uint16_t);
	std::pair<size_t, uint16_t> find(const DNSName &) noexcept;
};

DNSNameTree::node::node(node &&other) noexcept
	: children(std::move(other.children)),
	  label(std::move(other.label)),
	  offset(other.offset)
{}

DNSNameTree::node::node(const DNSLabel &label, uint16_t offset)
	: label(label), offset(offset)
{}

DNSNameTree::node::node(DNSLabel &&label, uint16_t offset)
	: label(label), offset(offset)
{}

DNSNameTree::DNSNameTree(DNSNameTree &&other) noexcept
	: root(std::move(other.root))
{}

bool DNSNameTree::add(const DNSName &name, uint16_t offset)
{
	node *nodeptr = &root;
	bool found = false;
	if (offset > 0x3fff)
		return false;
	// Move offset to end
	for (auto it = name.begin(); it != name.end(); ++it)
		offset += it->length()+1;
	// Check if all labels are in tree
	for (auto it = name.rbegin(); it != name.rend(); ++it) {
		offset -= it->length()+1;
		found = false;
		for (auto &item: nodeptr->children) {
			if (item.label == *it) {
				nodeptr = &item;
				found = true;
				break;
			}
		}
		if (!found) {
			nodeptr->children.emplace_back(*it, offset);
			nodeptr = &*nodeptr->children.rbegin();
		}
	}
	return (!found);
}

std::pair<size_t, uint16_t> DNSNameTree::find(const DNSName &name) noexcept
{
	size_t counter = 0;
	bool found = false;
	node *nodeptr = &root;
	for (auto it = name.rbegin(); it != name.rend(); ++it) {
		found = false;
		for (auto &item: nodeptr->children) {
			if (item.label == *it) {
				nodeptr = &item;
				counter++;
				found = true;
				break;
			}
		}
		if (!found) {
			break;
		}
	}
	return std::make_pair(counter, nodeptr->offset);
}

static std::string build_name(size_t offset, const DNSName &name, DNSNameTree &compr)
{
	size_t found_count, count = name.size();
	uint16_t found_offset;
	std::string result;
	std::tie(found_count, found_offset) = compr.find(name);
	count -= found_count;
	for (auto it = name.begin(); count != 0; --count, ++it) {
		size_t length = it->length();
		if (length > 63)
			throw std::invalid_argument("DNS labels cannot be longer than 63 chars");
		result.push_back(static_cast<unsigned char>(length));
		result.append(reinterpret_cast<const std::string&>(*it));
	}
	if (found_count) {
		result.push_back(static_cast<unsigned char>(0xC0 | found_offset>>8));
		result.push_back(static_cast<unsigned char>(found_offset));
	} else {
		result.push_back(0);
	}
	if (found_count != name.size())
		compr.add(name, offset);
	return result;
}

void DNSQuestion::build_into(std::vector<uint8_t> &buffer, DNSNameTree &compr) const
{
	// append dns name
	std::string tmp = build_name(buffer.size(), name, compr);
	buffer.insert(buffer.end(), tmp.begin(), tmp.end());
	// append fixed size fields
	buffer.insert(buffer.end(), {
		static_cast<uint8_t>(type >> 8),
		static_cast<uint8_t>(type),
		static_cast<uint8_t>(class_ >> 8),
		static_cast<uint8_t>(class_),
	});
}

static void append_u32n(std::vector<uint8_t> &buffer, uint32_t value)
{
	buffer.insert(buffer.end(), {
		static_cast<uint8_t>(value >> 24),
		static_cast<uint8_t>(value >> 16),
		static_cast<uint8_t>(value >> 8),
		static_cast<uint8_t>(value)
	});
}

void DNSRecord::build_into(std::vector<uint8_t> &buffer, DNSNameTree &compr) const
{
	DNSQuestion::build_into(buffer, compr);
	append_u32n(buffer, ttl);
	// append type-specific rdata
	rdata->build_into(buffer, compr);
}

std::unique_ptr<RData> RDataRaw::clone() const
{
	return std::unique_ptr<RData>(new RDataRaw(*this));
}

void RDataRaw::build_into(std::vector<uint8_t> &buffer, DNSNameTree &) const
{
	buffer.insert(buffer.end(), {static_cast<uint8_t>(rdata.size()>>8), static_cast<uint8_t>(rdata.size())});
	buffer.insert(buffer.end(), rdata.begin(), rdata.end());
}

std::unique_ptr<RData> RDataA::clone() const
{
	return std::unique_ptr<RData>(new RDataA(*this));
}

void RDataA::build_into(std::vector<uint8_t> &buffer, DNSNameTree &) const
{
	buffer.insert(buffer.end(), {0, static_cast<uint8_t>(sizeof(in_addr))});
	const uint8_t *ptr = reinterpret_cast<const uint8_t*>(&address.s_addr);
	buffer.insert(buffer.end(), ptr, ptr+sizeof(in_addr));
}

std::unique_ptr<RData> RDataAAAA::clone() const
{
	return std::unique_ptr<RData>(new RDataAAAA(*this));
}

void RDataAAAA::build_into(std::vector<uint8_t> &buffer, DNSNameTree &) const
{
	buffer.insert(buffer.end(), {0, static_cast<uint8_t>(sizeof(address.s6_addr))});
	buffer.insert(buffer.end(), &address.s6_addr[0], &address.s6_addr[16]);
}

std::unique_ptr<RData> RDataNAME::clone() const
{
	return std::unique_ptr<RData>(new RDataNAME(*this));
}

void RDataNAME::build_into(std::vector<uint8_t> &buffer, DNSNameTree &compr) const
{
	std::string tmp = build_name(buffer.size(), name, compr);
	buffer.insert(buffer.end(), {static_cast<uint8_t>(tmp.size()>>8), static_cast<uint8_t>(tmp.size())});
	buffer.insert(buffer.end(), tmp.begin(), tmp.end());
}

std::unique_ptr<RData> RDataTXT::clone() const
{
	std::unique_ptr<RDataTXT> copy(new RDataTXT(*this));
	return std::move(copy);
}

void RDataTXT::build_into(std::vector<uint8_t> &buffer, DNSNameTree &) const
{
	size_t size = 0;
	for (const auto &item: txt_data) {
		if (item.length() > 255)
			throw std::range_error("TXT entry too big");
		size += 1+item.length();
	}
	if (size > 65535)
		throw std::range_error("TXT record too big");
	buffer.insert(buffer.end(), {static_cast<uint8_t>(size>>8), static_cast<uint8_t>(size)});
	for (const auto &item: txt_data) {
		buffer.insert(buffer.end(), static_cast<uint8_t>(item.length()));
		buffer.insert(buffer.end(), item.begin(), item.end());
	}
}

std::unique_ptr<RData> RDataSOA::clone() const
{
	std::unique_ptr<RDataSOA> copy(new RDataSOA(*this));
	return std::move(copy);
}

void RDataSOA::build_into(std::vector<uint8_t> &buffer, DNSNameTree &compr) const
{
	std::string mname_data = build_name(buffer.size(), mname, compr);
	std::string rname_data = build_name(buffer.size(), rname, compr);
	size_t size = mname_data.size() + rname_data.size() + 20;
	buffer.insert(buffer.end(), {static_cast<uint8_t>(size>>8), static_cast<uint8_t>(size)});
	buffer.insert(buffer.end(), mname_data.begin(), mname_data.end());
	buffer.insert(buffer.end(), rname_data.begin(), rname_data.end());
	append_u32n(buffer, serial);
	append_u32n(buffer, refresh);
	append_u32n(buffer, retry);
	append_u32n(buffer, expire);
	append_u32n(buffer, minimum);
}

DNSPacket::DNSPacket() {}
DNSPacket::DNSPacket(DNSPacket &&other) noexcept
	: qd(std::move(other.qd)), an(std::move(other.an)),
	  ns(std::move(other.ns)), ar(std::move(other.ar))
{}

static void parse_name(DNSName &name, ConstPacketBufferView &view, const std::vector<uint8_t> &packet)
{
	while (1) {
		DNSLabel part;
		uint8_t n = view.peek<uint8_t>() & 0xC0;
		if (n == 0x00) {
			ssize_t len = view.next<uint8_t>() & 0x3F;
			if (!len)
				break;
			for (int i = 0; i < len; ++i)
				part.push_back(view.next<uint8_t>());
			name.emplace_back(std::move(part));
		} else if (n == 0xC0) {
			// Recursion
			ConstPacketBufferView view2(packet, ntohs(view.next<uint16_t>())&0x3FFF);
			parse_name(name, view2, packet);
			return;
		} else {
			throw std::invalid_argument("invalid DNS name found");
		}
	}
}

DNSPacket::DNSPacket(const std::vector<uint8_t> &packet)
{
	ConstPacketBufferView view(packet);
	// Get header
	const DNSHeader &h = view.next<DNSHeader>();
	header.dns_id = h.dns_id;
	header.dns_op_flags = h.dns_op_flags;
	header.dns_qdcount = h.dns_qdcount;
	header.dns_ancount = h.dns_ancount;
	header.dns_nscount = h.dns_nscount;
	header.dns_arcount = h.dns_arcount;
	
	// Read questions
	for (int i = 0; i < header.qdcount(); ++i) {
		DNSName name;
		parse_name(name, view, packet);
		uint16_t type = ntohs(view.next<uint16_t>());
		uint16_t class_ = ntohs(view.next<uint16_t>());
		qd.add({std::move(name), type, class_});
	}
	if (header.dns_ancount or header.dns_nscount or header.dns_ancount)
		// cannot parse DNS responses
		return;
}

std::vector<uint8_t> DNSPacket::build()
{
	std::vector<uint8_t> buffer;
	build(buffer);
	return buffer;
}

void DNSPacket::build(std::vector<uint8_t> &buffer)
{
	std::string tmp;
	DNSNameTree compr;
	const uint8_t *ptr = reinterpret_cast<const uint8_t*>(&header);
	size_t offset = 0;
	buffer.resize(0);
	// create header
	header.qdcount(qd.size());
	header.ancount(an.size());
	header.nscount(ns.size());
	header.arcount(ar.size());
	buffer.insert(buffer.end(), ptr, ptr+sizeof(header));
	offset += sizeof(DNSHeader);
	for (auto &question: qd) {
		question.build_into(buffer, compr);
	}
	// Records
	for (accessor<DNSRecord> *sec: {&an, &ns, &ar}) {
		for (auto &record: *sec) {
			record.build_into(buffer, compr);
		}
	}
}

