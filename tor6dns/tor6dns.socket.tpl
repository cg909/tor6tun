[Unit]
Description=tor6dns server socket

[Socket]
ListenDatagram=5300

[Install]
WantedBy=multi-user.target
