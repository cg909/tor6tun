#ifndef _DNS_H
#define _DNS_H

#include "ip_addr.h"
#include "packetbuffer.h"
#include "util.h"

#include <stdexcept>
#include <initializer_list>
#include <memory>
#include <vector>

struct dns_char_traits : public std::char_traits<char> {
	static bool eq(char c1, char c2) { return std::tolower(c1) == tolower(c2); }
	static bool ne(char c1, char c2) { return std::tolower(c1) != std::tolower(c2); }
	static bool lt(char c1, char c2) { return std::tolower(c1) <  std::tolower(c2); }
	static int compare(const char* s1, const char* s2, size_t n) {
		while( n-- != 0 ) {
			if( std::tolower(*s1) < std::tolower(*s2) ) return -1;
			if( std::tolower(*s1) > std::tolower(*s2) ) return 1;
			++s1; ++s2;
		}
		return 0;
	}
	static const char* find(const char* s, int n, char a) {
		while( n-- > 0 && std::tolower(*s) != std::tolower(a) ) {
			++s;
		}
		return s;
	}
};

//typedef std::basic_string<char, dns_char_traits> DNSLabel;

class DNSLabel: public std::basic_string<char, dns_char_traits> {
	typedef std::basic_string<char, dns_char_traits> base;
public:
	DNSLabel() {}
	DNSLabel(const DNSLabel &) = default;
	DNSLabel(DNSLabel &&) = default;
	explicit DNSLabel(const std::string &);
	DNSLabel(const char *cstr)
		: base(cstr) {}
	DNSLabel(std::initializer_list<char> list)
		: base(list) {}
	template<typename T>
	DNSLabel(T begin, T end)
		: base(begin, end) {}
	DNSLabel &operator =(const DNSLabel &) = default;
	DNSLabel &operator =(DNSLabel &&) = default;
};
std::ostream& operator << (std::ostream&, const DNSLabel&);

class DNSName: public std::vector<DNSLabel> {
public:
	DNSName() {}
	DNSName(const std::initializer_list<DNSLabel> &other)
		: std::vector<DNSLabel>(other) {}
	DNSName(const std::vector<DNSLabel>&other)
		: std::vector<DNSLabel>(other) {}
	DNSName(const std::vector<DNSLabel>&& other)
		: std::vector<DNSLabel>(std::move(other)) {}
	DNSName(const std::string &);
	template<typename T>
	DNSName(T begin, T end)
		: std::vector<DNSLabel>(begin, end) {}
	static DNSName make_reverse(const struct in6_addr&, unsigned short halfbytes=32);
	static DNSName make_reverse(const IPv6Network &net)
	{
		return make_reverse(net, net.prefixlen/4);
	}
	bool endswith(const DNSName &) const;
};
std::ostream& operator << (std::ostream&, const DNSName&);

class DNSNameTree;

enum DNSType {
	A = 1,
	NS = 2,
	CNAME = 5,
	SOA = 6,
	PTR = 12,
	TXT = 16,
	AAAA = 28,
	ANY_TYPE = 255
};

enum DNSClass {
	IN = 1,
	CS = 2,
	CH = 3,
	HS = 4,
	ANY_CLASS = 255
};

struct DNSHeader {
	uint16_t dns_id;
	uint16_t dns_op_flags;
	uint16_t dns_qdcount;
	uint16_t dns_ancount;
	uint16_t dns_nscount;
	uint16_t dns_arcount;
	
	constexpr DNSHeader(): dns_id(0), dns_op_flags(0), dns_qdcount(0), dns_ancount(0), dns_nscount(0), dns_arcount(0)
	{}
	
	uint16_t id() const {
		return ntohs(dns_id);
	}
	void id(uint16_t id) {
		dns_id = htons(id);
	}
	bool qr() const {
		return ntohs(dns_op_flags) >> 15;
	}
	void qr(bool bit) {
		dns_op_flags = htons((ntohs(dns_op_flags) & 0x7F) | (bit << 15));
	}
	uint8_t opcode() const {
		return (ntohs(dns_op_flags) >> 11) & 0xF;
	}
	void opcode(uint8_t op) {
		if (op > 0xF)
			throw std::out_of_range("invalid opcode");
		dns_op_flags = htons((ntohs(dns_op_flags) & 0x87FF) | ((op & 0xF) << 11));
	}
	uint8_t flags() const {
		return (ntohs(dns_op_flags) >> 4) & 0x7F;
	}
	void flags(uint8_t flags) {
		if (flags > 0x7F)
			throw std::out_of_range("invalid flags");
		dns_op_flags = htons((ntohs(dns_op_flags) & 0xF80F) | ((flags & 0x7F) << 4));
	}
	uint8_t rcode() const {
		return ntohs(dns_op_flags) & 0xF;
	}
	void rcode(uint8_t op) {
		if (op > 0xF)
			throw std::out_of_range("invalid opcode");
		dns_op_flags = htons((ntohs(dns_op_flags) & 0xFFF0) | (op & 0xF));
	}
	uint16_t qdcount() const {
		return ntohs(dns_qdcount);
	}
	void qdcount(uint16_t count) {
		dns_qdcount = htons(count);
	}
	uint16_t ancount() const {
		return ntohs(dns_ancount);
	}
	void ancount(uint16_t count) {
		dns_ancount = htons(count);
	}
	uint16_t nscount() const {
		return ntohs(dns_nscount);
	}
	void nscount(uint16_t count) {
		dns_nscount = htons(count);
	}
	uint16_t arcount() const {
		return ntohs(dns_arcount);
	}
	void arcount(uint16_t count) {
		dns_arcount = htons(count);
	}
};

class DNSQuestion {
public:
	DNSName name;
	uint16_t type;
	uint16_t class_;

	DNSQuestion(const DNSName &name, uint16_t type, uint16_t class_)
		: name(name), type(type), class_(class_)
	{}
	DNSQuestion(DNSName &&name, uint16_t type, uint16_t class_)
		: name(name), type(type), class_(class_)
	{}
	void build_into(std::vector<uint8_t>&, DNSNameTree&) const;
};

class RData {
public:
	virtual std::unique_ptr<RData> clone() const = 0;
	virtual void build_into(std::vector<uint8_t>&, DNSNameTree&) const = 0;
	virtual ~RData() {}
};

class DNSRecord: public DNSQuestion {
private:
	int32_t ttl;
	std::unique_ptr<RData> rdata;
public:
	DNSRecord(const DNSName &name, uint16_t type, uint16_t class_, const RData &rdata)
		: DNSQuestion(name, type, class_), ttl(0), rdata(rdata.clone())
	{}
	DNSRecord(const DNSName &name, uint16_t type, uint16_t class_, int32_t ttl, const RData &rdata)
		: DNSQuestion(name, type, class_), ttl(ttl), rdata(rdata.clone())
	{}
	
	DNSRecord(const DNSRecord &other)
		: DNSQuestion(other.name, other.type, other.class_),
		  ttl(other.ttl), rdata(other.rdata->clone())
	{}
	DNSRecord(DNSRecord &&other)
		: DNSQuestion(std::move(other.name), other.type, other.class_),
		  ttl(other.ttl), rdata(std::move(other.rdata))
	{}
	DNSRecord &operator =(const DNSRecord &other)
	{
		name = other.name;
		type = other.type;
		class_ = other.class_;
		ttl = other.ttl;
		rdata = other.rdata->clone();
		return *this;
	}
	DNSRecord &operator =(DNSRecord &&other)
	{
		name = std::move(other.name);
		type = other.type;
		class_ = other.class_;
		ttl = other.ttl;
		rdata = std::move(other.rdata);
		return *this;
	}
	void build_into(std::vector<uint8_t>&, DNSNameTree&) const;
	template <typename T>
	T &get_rdata()
	{
		return dynamic_cast<T&>(*rdata);
	}
	template <typename T>
	const T &get_rdata() const
	{
		return dynamic_cast<const T&>(*rdata);
	}
};

class RDataRaw: public RData {
public:
	std::vector<uint8_t> rdata;
	RDataRaw() {}
	RDataRaw(const std::vector<uint8_t> &rdata): rdata(rdata) {}
	RDataRaw(std::vector<uint8_t> &&rdata): rdata(std::move(rdata)) {}
	~RDataRaw() override {}
	std::unique_ptr<RData> clone() const override;
	void build_into(std::vector<uint8_t>&, DNSNameTree&) const override;
};

class RDataA: public RData {
public:
	in_addr address;
	RDataA() { zero_init(address); }
	RDataA(const in_addr &addr): address(addr) {}
	~RDataA() override {}
	std::unique_ptr<RData> clone() const override;
	void build_into(std::vector<uint8_t>&, DNSNameTree&) const override;
};

class RDataAAAA: public RData {
public:
	IPv6Address address;
	RDataAAAA() {}
	RDataAAAA(const IPv6Address &addr): address(addr) {}
	~RDataAAAA() override {}
	std::unique_ptr<RData> clone() const override;
	void build_into(std::vector<uint8_t>&, DNSNameTree&) const override;
};

class RDataNAME: public RData {
public:
	DNSName name;
	RDataNAME() {}
	RDataNAME(const DNSName &name): name(name) {}
	RDataNAME(DNSName &&name): name(std::move(name)) {}
	~RDataNAME() override {}
	std::unique_ptr<RData> clone() const override;
	void build_into(std::vector<uint8_t>&, DNSNameTree&) const override;
};

class RDataTXT: public RData {
public:
	std::vector<std::string> txt_data;
	RDataTXT() {}
	template<typename T>
	RDataTXT(const T &data) {txt_data.emplace_back(data);}
	template<typename... Types>
	RDataTXT(const std::string &item, Types ...items)
	{
		add(item, items...);
	}
	template<typename... Types>
	RDataTXT(std::string &&item, Types ...items)
	{
		add(std::move(item), items...);
	}
	RDataTXT(std::string &&data) {txt_data.emplace_back(data);}
	RDataTXT(const std::string &data) {txt_data.emplace_back(data);}
	RDataTXT(const std::vector<std::string> &data): txt_data(data) {}
	RDataTXT(std::vector<std::string> &&data): txt_data(std::move(data)) {}
	~RDataTXT() override {}
	std::unique_ptr<RData> clone() const override;
	void build_into(std::vector<uint8_t>&, DNSNameTree&) const override;
	void add(const std::string &item)
	{
		txt_data.emplace_back(item);
	}
	void add(std::string &&item)
	{
		txt_data.emplace_back(std::move(item));
	}
	template<typename ...Types>
	void add(const std::string &item, Types ...items)
	{
		add(item);
		add(items...);
	}
	template<typename ...Types>
	void add(std::string &&item, Types ...items)
	{
		add(std::move(item));
		add(items...);
	}
};

class RDataSOA: public RData {
public:
	DNSName mname;
	DNSName rname;
	uint32_t serial, refresh, retry, expire, minimum;
	RDataSOA(): serial(0), refresh(0), retry(0), expire(0), minimum(0) {}
	RDataSOA(const DNSName &mname, const DNSName &rname, uint32_t serial = 0,
			 uint32_t refresh = 0, uint32_t retry = 0, uint32_t expire = 0,
		     uint32_t minimum = 0)
		: mname(mname), rname(rname), serial(serial), refresh(refresh),
		  retry(retry), expire(expire), minimum(minimum)
	{}
	~RDataSOA() override {}
	std::unique_ptr<RData> clone() const override;
	void build_into(std::vector<uint8_t>&, DNSNameTree&) const override;
};


class DNSPacket {
	mutable DNSHeader header;
public:
	template<typename T>
	class accessor final {
		std::vector<T> list;
		constexpr accessor() {}
		constexpr accessor(accessor &&other): list(std::move(other.list)) {}
	public:
		typedef typename std::vector<T>::iterator iterator;
		accessor& operator= (accessor&&) = delete;
		accessor& operator= (const accessor&) = delete;
		iterator begin() { return list.begin(); }
		iterator end() { return list.end(); }
		size_t size() const { return list.size(); }
		void add(const T &record) { list.emplace_back(record); }
		void add(T &&record) { list.emplace_back(std::move(record)); }
		void clear() { list.clear(); }
		friend class DNSPacket;
	};
	accessor<DNSQuestion> qd;
	accessor<DNSRecord> an, ns, ar;
	DNSPacket();
	DNSPacket(DNSPacket&&) noexcept;
	DNSPacket(const std::vector<uint8_t> &);
	uint16_t id() const { return header.id(); }
	void id(uint16_t id) { header.id(id); }
	bool qr() const { return header.qr(); }
	void qr(bool bit) { header.qr(bit); }
	uint8_t opcode() const { return header.opcode(); }
	void opcode(uint8_t op) { header.opcode(op); }
	uint8_t flags() const { return header.flags(); }
	void flags(uint8_t flags) { header.flags(flags); }
	uint8_t rcode() const { return header.rcode(); }
	void rcode(uint8_t code) { header.rcode(code); }
	std::vector<uint8_t> build();
	void build(std::vector<uint8_t>&);
};

#endif
