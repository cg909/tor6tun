[Unit]
Description=DNS server for .onion domains
Documentation=man:tor6dns(8)
Requires=tor6dns.socket
Wants=tor6tun.service

[Service]
Type=notify
User=nobody
ExecStart=/usr/local/sbin/tor6dns

[Install]
WantedBy=multi-user.target
Also=tor6dns.socket