/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************
 * Entry point and main program                                             *
 ****************************************************************************/

#include "dns.h"
#include "base32.h"
#include "getaddrinfo.h"
#include "logging.h"
#include "privdrop.h"
#include "signals.h"
#include "socket.h"
#include "util.h"

#include <argp.h>
#include <unistd.h>
#ifdef SYSTEMD
#  include <systemd/sd-daemon.h>
#endif

/* Argument handling */

const char *argp_program_version = "tor6dns 1.0";

static struct argp_option options[] = {
	{"onion-network", 'o', "SUBNET",  0, "IPv6 subnet for .onion domain mapping (default: fd87:7026:eb43::/48)" },
	{"interface", 'I', "NAME",  0, "bind to interface" },
	{"port", 'P', "PORT",  OPTION_HIDDEN, "UDP port" }, // for backwards compatibility
	{"bind", 'b', "ADDR",  0, "bind to address:port" },
	{"user", 'u', "USER",  0, "setuid to user after initialization" },
	{"legacy-daemonize", 'D', 0,  0, "detach and run in background" },
	{0, 'v', 0,  0, "increase verbosity" },
	{0, 'q', 0,  0, "decrease verbosity" },
	{ 0 }
};

struct arguments
{
	IPv6Network onion_network;
	std::string bind;
	std::string interface;
	std::string user;
	int verbosity;
	bool daemonize;
};


/* Parse a single option. */
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
	struct arguments *arguments = static_cast<struct arguments *>(state->input);

	switch (key) {
	case 'o': {
		std::string text(arg);
		if (text.find('/') == std::string::npos) {
			text.append("/48");
		}
		try {
			arguments->onion_network = text;
		} catch (std::invalid_argument &exc) {
			log::error("invalid argument for --onion-network: ", exc.what());
			return EINVAL;
		}
		if (arguments->onion_network.prefixlen != 48) {
			log::error("invalid argument for --onion-network: must be a /48 subnet");
			return EINVAL;
		}
		break;
	}
	case 'u':
		arguments->user = arg;
		break;
	case 'I':
		arguments->interface = arg;
		break;
	case 'P':
		arguments->bind = "[::]:";
		arguments->bind += arg;
		break;
	case 'b':
		arguments->bind = arg;
		break;
	case 'v':
		if (arguments->verbosity < log::DEBUG)
			arguments->verbosity++;
		break;
	case 'q':
		if (arguments->verbosity >= log::CRITICAL)
			arguments->verbosity--;
		break;
	case 'D':
		arguments->daemonize = true;
		break;
	case ARGP_KEY_ARG:
		/* Too many arguments. */
		argp_usage (state);
		return ARGP_ERR_UNKNOWN;
		break;

	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static struct argp argp = { options, parse_opt, "", "minimal DNS server for .onion lookups" };

/* Notifications */

static void notify_ready()
{
	log::info("ready");
	#ifdef SYSTEMD
	sd_notify(0, "READY=1");
	#endif
}

[[gnu::cold]]
static void notify_terminating()
{
	std::cout.flush();
	log::info("terminating...");
	#ifdef SYSTEMD
	sd_notify(0, "STOPPING=1");
	#endif
}

/* Signal handling */

static volatile sig_atomic_t exit_requested = 0;

[[gnu::cold]]
static void term_handler(int signal)
{
	// Check and set termination flag
	if (!exit_requested) {
		exit_requested = 1;
		// Notify user about pending termination
		notify_terminating();
		// Unset signal handler
		try { sigaction({signal}, SIG_DFL); }
		catch (std::exception &exc) { abort(); }
	}
}

/* Helper functions */
static char hex2byte(char hex) {
	if (hex >= '0' && hex <= '9')
		return hex - '0';
	else if (hex >= 'a' && hex <= 'f')
		return hex - 'a' + 10;
	else if (hex >= 'A' && hex <= 'f')
		return hex - 'A' + 10;
	else
		throw std::invalid_argument("hex2byte(): not a hexadecimal digit");

}

static DNSName reverse2onion(const DNSName &reverse)
{
	if (reverse.size() != 34)
		throw std::invalid_argument("reverse2onion(): invalid length");
	auto it = reverse.rbegin();
	it++; it++;
	in6_addr tmp;
	uint8_t *ptr = tmp.s6_addr;
	while (it != reverse.rend()) {
		if (it->size() != 1)
			throw std::invalid_argument("reverse2onion(): label with invalid length");
		*ptr = hex2byte(it->at(0)) << 4;
		++it;
		if (it->size() != 1)
			throw std::invalid_argument("reverse2onion(): label with invalid length");
		*ptr |= hex2byte(it->at(0));
		++it;
		++ptr;
	}

	DNSLabel l { b32encode(&tmp.s6_addr[6], 10) };
	return {l, "onion"};
}

static Socket get_server_socket(const std::string &bind = "", unsigned int interface_index=0) {
	if (bind.length()) {
		// Create socket
		Socket s(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
		// Resolve address and port
		struct sockaddr_in6 addr;
		addr.sin6_family = 0;
		try {
			for (auto &&item : getaddrinfo(bind, AI_PASSIVE|AI_V4MAPPED, AF_INET6, SOCK_DGRAM)) {
				addr = *reinterpret_cast<sockaddr_in6*>(item.ai_addr);
				break;
			}
		} catch (std::invalid_argument &) {
			// Default to standard DNS port
			for (auto &&item : getaddrinfo(bind, "domain", AI_PASSIVE|AI_V4MAPPED, AF_INET6, SOCK_DGRAM)) {
				addr = *reinterpret_cast<sockaddr_in6*>(item.ai_addr);
				break;
			}
		}
		if (!addr.sin6_family)
			throw std::invalid_argument("argument for --bind is invalid");
		if (interface_index)
			addr.sin6_scope_id = interface_index;
		log::debug("binding to ", bind);
		// Bind socket
		s.bind(addr);
		// Join multicast group if addr is multicast
		// This allows to use e.g. ff01::/16 like 127.0.0.0/8
		if (static_cast<IPv6Address>(addr.sin6_addr).is_multicast()) {
			s.join_group(addr.sin6_addr, addr.sin6_scope_id);
		}
		// If port was 0, inform user of used port
		if (addr.sin6_port == 0) {
			addr = s.getsockname<sockaddr_in6>();
			log::info("bound to port ", ntohs(addr.sin6_port));
		}
		return s;
	} else {
		#ifdef SYSTEMD
		int count = sd_listen_fds(true);
		if (count == 1) {
			if (sd_is_socket(SD_LISTEN_FDS_START, AF_UNSPEC, SOCK_DGRAM, -1) <= 0)
				throw std::invalid_argument("invalid socket passed by systemd");
			return Socket {SD_LISTEN_FDS_START};
		} else if (count > 1) {
			throw std::invalid_argument("too many sockets passed by systemd");
		}
		#endif
		throw std::invalid_argument("no bind address specified");
	}
}

/* Main function */

int main(int argc, char *argv[])
{
	try {
		struct arguments arguments;

		// Argument defaults
		arguments.onion_network = "fd87:7026:eb43::/48";
		arguments.bind = "";
		arguments.verbosity = log::INFO;
		arguments.daemonize = false;

		// Parse arguments
		if (argp_parse (&argp, argc, argv, 0, 0, &arguments)) {
			return 2;
		}

		// Set signal handlers
		sigaction(
			{SIGINT, SIGQUIT, SIGTERM},
			term_handler,
			signalset::full().remove({SIGILL, SIGSEGV, SIGABRT})
		);
		sigaction(
			{SIGHUP, SIGUSR1, SIGUSR2},
			SIG_IGN
		);

		// Set loglevel
		log::loglevel = static_cast<enum log::loglevels>(arguments.verbosity);

		// Initialization
		std::vector<uint8_t> buffer;
		buffer.reserve(65535);
		Socket s;
		try {
			unsigned int interface_index = 0;
			if (arguments.interface.length()) {
				interface_index = Socket::if_nametoindex(arguments.interface);
			}
			s = get_server_socket(arguments.bind, interface_index);
			if (arguments.interface.length()) {
				s.setsockopt(SOL_SOCKET, SO_BINDTODEVICE, arguments.interface);
			}
		} catch (std::system_error &exc) {
			std::cout.flush();
			log::critical(exc.what(), " [", exc.code().value(), ']');
			return 3;
		} catch (std::invalid_argument &exc) {
			std::cout.flush();
			log::critical(exc.what());
			return 2;
		}
		DNSName reverse_domain { DNSName::make_reverse(arguments.onion_network) };
		DNSRecord soa {{"onion"}, SOA, IN, 30, RDataSOA {{}, {}, 1337}};
		DNSRecord soa_r {reverse_domain, SOA, IN, 30, RDataSOA {{}, {}, 1337}};

		// Privilege dropping
		drop_privileges(arguments.user);

		// Chdir to root
		if (chdir("/"))
			throw_errno("chdir() failed");

		// Notify end of startup
		notify_ready();
		if (arguments.daemonize) {
			daemonize(true);
		}

		// Main loop
		while (!exit_requested) {
			try {
				sockaddr_in6 remote_addr;
				bool result;
				std::tie(result, remote_addr) = s.recvfrom<sockaddr_in6>(buffer);
				if (!result)
					continue;
				log::debug("received ", buffer.size(), " bytes from ", static_cast<IPv6Address>(remote_addr.sin6_addr));
				DNSPacket query(buffer);
				if (query.qr()) {
					log::info("received DNS response instead of a query");
					continue;
				}
				DNSPacket response;
				bool one_found = false;
				bool all_authorative = true;
				bool reverse_query = false;
				response.qr(1);
				response.id(query.id());
				response.opcode(query.opcode());
				response.flags(query.flags()&0x10);

				if (query.opcode() == 0) {
					if (query.qd.size() == 0) {
						log::debug("received empty DNS query");
						response.rcode(1);
					}
					for (auto &question: query.qd) {
						log::debug("received DNS query for ", question.name, " type: ", question.type, " class: ", question.class_);
						response.qd.add(question);

						if (question.class_ != IN and question.class_ != ANY_CLASS) {
							log::debug("ignoring query for unsupported class");
							all_authorative = false;
							continue;
						}
						if (!question.name.size()) {
							log::debug("ignoring query for root zone");
							all_authorative = false;
							continue;
						}

						if (question.name.at(question.name.size()-1) == "onion") {
							if (question.name.size() == 1) {
								if (question.type == SOA or question.type == ANY_TYPE) {
									response.an.add(soa);
								}
								if (question.type == TXT or question.type == ANY_TYPE) {
									response.an.add({question.name, TXT, IN, 30, RDataTXT {
										"special-use tld for anonymous hidden services reachable via the Tor network."
									}});
								}
								one_found = true;
							}
							else {
								const auto &sld = question.name.at(question.name.size()-2);
								if (sld.length() != 16)
									continue;
								try {
									b32decode(sld, &arguments.onion_network.s6_addr[6], 10);
								} catch (std::invalid_argument &exc) {
									continue;
								}
								one_found = true;
								if (question.name.size() > 2)
									response.an.add({question.name, CNAME, IN, 30, RDataNAME {{sld, "onion"}}});
								if (question.type == AAAA || question.type == ANY_TYPE) {
									response.an.add({{sld, "onion"}, AAAA, IN, 30, RDataAAAA { arguments.onion_network }});
								}
							}
						} else if (question.name.endswith(reverse_domain)) {
							reverse_query = true;
							if (question.name.size() == 34) {
								try {
									DNSName name = reverse2onion(question.name);
									if (question.type == PTR or question.type == ANY_TYPE) {
										response.an.add({question.name, PTR, IN, 30, RDataNAME {name}});
										one_found = true;
									}
								} catch (std::invalid_argument &exc) {
									continue;
								}
							}
						} else {
							log::debug("ignoring query for non .onion domain ", question.name);
							all_authorative = false;
						}
					}
					if (!one_found)
						response.rcode(3);
					if (all_authorative)
						response.flags(response.flags()|64);
					if (all_authorative and response.an.size() == 0)
						response.ns.add((reverse_query)?soa_r:soa);
				} else {
					log::debug("received unsupported DNS command ", query.opcode());
					response.rcode(4);
				}

				auto data = response.build();
				s.sendto(data, remote_addr);
				log::debug("sent response with ", response.an.size(),
						" answer(s) (code ", static_cast<int>(response.rcode()),
						(all_authorative ? ", aa" : ""), ")");
			} catch (std::invalid_argument &exc) {
				log::warn("received invalid DNS packet");
			}
		}
		return 0;
	} catch (std::system_error & exc) {
		std::cout.flush();
		log::critical(exc.what(), " [", exc.code().value(), ']');
		return 3;
	}
	catch (std::runtime_error & exc) {
		std::cout.flush();
		log::critical("runtime error: ", exc.what());
		abort();
	}
	catch (std::invalid_argument & exc) {
		std::cout.flush();
		log::critical("invalid argument: ", exc.what());
		return 2;
	}
	catch (std::exception & exc) {
		throw;
	}

}