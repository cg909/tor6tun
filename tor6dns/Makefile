#!/usr/bin/make -f
# (c) 2016 Christoph Grenz <christophg@grenz-bonn.de>
# License: GNU GPLv3 or later
# This file is part of tor6dns

SRCPATH := $(shell dirname $(lastword $(MAKEFILE_LIST)))
COMMONPATH := $(shell dirname $(lastword $(MAKEFILE_LIST)))/../common
VPATH := $(SRCPATH):$(SRCPATH)/../common

include $(COMMONPATH)/Makefile.common

ifdef DEBUG
CXXFLAGS ?= -O0 -g -fPIE -Wall -Wextra -Wpedantic
else
CXXFLAGS ?= -Os -flto -fPIE -Wall -Wextra -Wpedantic
endif
LDFLAGS ?= -pie
CXXINCL = -I$(SRCPATH) -I$(COMMONPATH)

SOURCES = main.cpp base32.cpp dns.cpp getaddrinfo.cpp ip_addr.cpp logging.cpp \
	  privdrop.cpp signals.cpp socket.cpp 

all: tor6dns tor6dns.service tor6dns.socket

tor6dns: $(SOURCES:.cpp=.o)
	$(CXX) -std=c++11 $(CXXFLAGS) $(LDFLAGS) -o "$@" $^ $(LIBS)

main.o: main.cpp Makefile
	$(CXX) -c -std=c++11 $(DEFINES) $(CXXFLAGS) -Wno-missing-field-initializers $(CXXINCL) -o "$@" "$<"

clean:
	rm $(SOURCES:.cpp=.o) || true
	rm $(SOURCES:%.cpp=%.dep) || true
	rm tor6dns.socket || true
	rm tor6dns.service || true
	rm tor6dns || true

install: all
	install -m 0755 -o root -g root -s tor6dns "$(PREFIX)"/sbin/
	install -m 0755 -d "$(PREFIX)"/lib/systemd/system
	install -m 0644 -o root -g root tor6dns.socket "$(PREFIX)"/lib/systemd/system/
	install -m 0644 -o root -g root tor6dns.service "$(PREFIX)"/lib/systemd/system/
	systemctl daemon-reload || true

uninstall:
	rm "$(PREFIX)"/sbin/tor6dns || true
	rm "$(PREFIX)"/lib/systemd/system/tor6dns.service || true
	rm "$(PREFIX)"/lib/systemd/system/tor6dns.socket || true
	rmdir --ignore-fail-on-non-empty "$(PREFIX)"/lib/systemd/system "$(PREFIX)"/lib/systemd

.PHONY: all clean install uninstall

-include $(SOURCES:%.cpp=%.dep)
