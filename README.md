tor6tun
=======

An IPv6 layer 3 proxy for TOR.

This project aims to provide transparent access to both hidden services and the IPv6 clearnet via TOR.

It consists of a daemon (tor6tun) that provides a TUN interface to the TOR network, a minimal non-recursive DNS server and a NSS plugin (libnss_onion)
which both resolve .onion addresses to site-scope IPv6 addresses.

## Prerequisites

`tor6tun`, `tor6dns` and `libnss_onion` are written in **C++11** and have no dependencies besides a recent C++ standard library.

The manpages are generated with **ronn**.

## Installation

Run `make` in the project root directory to compile all components.
The compilation results are stored in a subdirectory `build`.

Then use `make install` to install.

The following environment variables are used:

+ **DEBUG**: changes the default **CXXFLAGS** to disable optimizations and enable debug symbols if set.
+ **CXX**: C++ compiler to use
+ **CXXFLAGS**: compilation options
+ **LDFLAGS**: linker options
+ **PREFIX**: installation prefix (defaults to /usr/local)
+ **SYSTEMD**: set to 1 or 0 to enable/disable support for libsystemd-daemon (default: 1 if libsystemd-daemon respective libsystemd is found)
+ **LINUXCAPS**: set to 1 or 0 to enable/disable support for capability dropping (default: 1 if sys/capability.h exists)

## Usage

See [tor6tun(8)](man/tor6tun.8.md), [tor6dns(8)](man/tor6dns.8.md) and [libnss_onion(3)](man/libnss_onion.3.md).
