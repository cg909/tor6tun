/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This library is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU Lesser General Public               *
 * License as published by the Free Software Foundation; either             *
 * version 3 of the License, or (at your option) any later version.       *
 *                                                                          *
 * This library is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * Lesser General Public License for more details.                          *
 *                                                                          *
 * You should have received a copy of the GNU Lesser General Public License *
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************/

#include "nss_onion.h"
#include "ip_addr.h"
#include "base32.h"

#include <netdb.h>
#include <error.h>
#include <iostream>
#include <cstring>

static IPv6Network onion_network ("fd87:7026:eb43::/48");

struct buffer_view {
	struct in6_addr* addrs[2];
	struct in6_addr addr;
	char name[23];
};

static bool name_is_onion(const std::string &str) {
	if (str.length() < 22)
		return false;
	if (str.substr(str.length()-6) == ".onion")
		return true;
	if (str.substr(str.length()-7) == ".onion.")
		return true;
	return false;
}

static IPv6Address onion_gethostbyname(const std::string &str)
{
	size_t n, startpos = -1, endpos = -1;
	n = str.rfind(".onion");
	if (n == std::string::npos)
		throw std::domain_error("not an onion url");
	
	endpos = n;
	n = str.rfind(".", endpos-1);
	if (n == std::string::npos)
		startpos = 0;
	else
		startpos = n+1;

	std::string mainpart = str.substr(startpos, endpos-startpos);
	if (mainpart.length() != 16)
		throw std::invalid_argument("invalid address length");

	IPv6Address addr = onion_network;
	b32decode(mainpart, &addr.s6_addr[6], 10);

	return addr;
	
}

nss_status _nss_onion_sethostent(int) noexcept
{
	return NSS_STATUS_SUCCESS;
}

nss_status _nss_onion_endhostent() noexcept
{
	return NSS_STATUS_SUCCESS;
}

nss_status _nss_onion_gethostent_r(struct hostent *, char *, size_t,
                                   int *errnop, int *herrnop) noexcept
{
	*herrnop = HOST_NOT_FOUND;
	*errnop = ENOENT;
	return NSS_STATUS_NOTFOUND;
}

nss_status _nss_onion_gethostbyname_r(const char *n, struct hostent *result, char *buffer,
                                      size_t buflen, int *errnop, int *herrnop) noexcept
{
	const std::string name = n;
	if (!name_is_onion(name)) {
		*herrnop = NO_RECOVERY;
		*errnop = EINVAL;
		return NSS_STATUS_UNAVAIL;
	}
	
	IPv6Address addr;
	try {
		addr = onion_gethostbyname(name);
	} catch (std::invalid_argument &exc) {
		*herrnop = HOST_NOT_FOUND;
		*errnop = ENOENT;
		return NSS_STATUS_NOTFOUND;
	}
	
	// Check buffer size and adjust alignment
	size_t align_by = 0;
	size_t alignment_error = (reinterpret_cast<uintptr_t>(buffer) % alignof(struct buffer_view));
	if (!__builtin_expect(alignment_error, 0)) {
		align_by = 4-alignment_error;
	}
	if (!buffer || buflen < sizeof(struct buffer_view)+align_by+name.length()-22) {
		*herrnop = TRY_AGAIN;
		*errnop = ERANGE;
		return NSS_STATUS_TRYAGAIN;
	}
	
	// Save data to buffer
	struct buffer_view &buf = *reinterpret_cast<struct buffer_view *>(buffer+align_by);
	buf.addrs[0] = &buf.addr;
	buf.addrs[1] = nullptr;
	std::memcpy(&buf.addr, &addr, 16);
	std::memcpy(&buf.name, name.c_str(), name.length()+1);
	
	// Fill result struct
	result->h_name = buf.name;
	result->h_aliases = reinterpret_cast<char**>(&buf.addrs[1]);
	result->h_addrtype = AF_INET6;
	result->h_length = 16;
	result->h_addr_list = reinterpret_cast<char**>(buf.addrs);
	return NSS_STATUS_SUCCESS;
}

nss_status _nss_onion_gethostbyname2_r(const char *name, int af, struct hostent *result,
                                       char *buffer, size_t buflen, int *errnop,
                                       int *herrnop) noexcept
{
	if (af != AF_INET6) {
		*herrnop = NO_RECOVERY;
		*errnop = EINVAL;
		return NSS_STATUS_UNAVAIL;
	}
	return _nss_onion_gethostbyname_r(name, result, buffer, buflen, errnop, herrnop);
}

nss_status _nss_onion_gethostbyname3_r(const char *n, int af, struct hostent *result,
                                       char *buffer, size_t buflen, int *errnop, int *herrnop,
                                       int32_t *ttlp, char **canonp) noexcept
{
	if (af != AF_INET6) {
		*herrnop = NO_RECOVERY;
		*errnop = EINVAL;
		return NSS_STATUS_UNAVAIL;
	}
	
	const std::string name = n;
	if (!name_is_onion(name)) {
		*herrnop = NO_RECOVERY;
		*errnop = EINVAL;
		return NSS_STATUS_UNAVAIL;
	}
	
	IPv6Address addr;
	try {
		addr = onion_gethostbyname(name);
	} catch (std::invalid_argument &exc) {
		*herrnop = HOST_NOT_FOUND;
		*errnop = ENOENT;
		return NSS_STATUS_NOTFOUND;
	}
	
	// Check buffer size and adjust alignment
	size_t align_by = 0;
	size_t alignment_error = (reinterpret_cast<uintptr_t>(buffer) % alignof(struct buffer_view));
	if (!__builtin_expect(alignment_error, 0)) {
		align_by = 4-alignment_error;
	}
	if (!buffer || buflen < sizeof(struct buffer_view)+align_by+name.length()+1+23) {
		*herrnop = TRY_AGAIN;
		*errnop = ERANGE;
		return NSS_STATUS_TRYAGAIN;
	}
	
	// Get canonical name
	std::string onion = b32encode(&addr.s6_addr[6], 10)+".onion";
	
	// Save data to buffer
	struct buffer_view &buf = *reinterpret_cast<struct buffer_view *>(buffer+align_by);
	buf.addrs[0] = &buf.addr;
	buf.addrs[1] = nullptr;
	std::memcpy(&buf.addr, &addr, 16);
	std::memcpy(&buf.name, onion.c_str(), 23);
	char *nameptr = buffer+align_by+sizeof(struct buffer_view);
	std::memcpy(nameptr, name.c_str(), name.length()+1);
	
	// Fill result struct
	result->h_name = nameptr;
	result->h_aliases = reinterpret_cast<char**>(&buf.addrs[1]);
	result->h_addrtype = AF_INET6;
	result->h_length = 16;
	result->h_addr_list = reinterpret_cast<char**>(buf.addrs);
	
	if (canonp)
		*canonp = buf.name;
	if (ttlp)
		*ttlp = 0;
	
	return NSS_STATUS_SUCCESS;
}

nss_status _nss_onion_gethostbyname4_r(const char *n, struct gaih_addrtuple **pat,
                                       char *buffer, size_t buflen, int *errnop, int *herrnop,
                                       int32_t *ttlp) noexcept
{
	const std::string name = n;
	if (!name_is_onion(name)) {
		*herrnop = NO_RECOVERY;
		*errnop = EINVAL;
		return NSS_STATUS_UNAVAIL;
	}
	
	IPv6Address addr;
	try {
		addr = onion_gethostbyname(name);
	} catch (std::invalid_argument &exc) {
		*herrnop = HOST_NOT_FOUND;
		*errnop = ENOENT;
		return NSS_STATUS_NOTFOUND;
	}
	
	// Check buffer size and adjust alignment
	size_t align_by = 0;
	size_t alignment_error = (reinterpret_cast<uintptr_t>(buffer) % alignof(struct gaih_addrtuple));
	if (!__builtin_expect(alignment_error, 0)) {
		align_by = 4-alignment_error;
	}
	if (!buffer || buflen < sizeof(struct gaih_addrtuple)+align_by+name.length()+1) {
		*herrnop = TRY_AGAIN;
		*errnop = ERANGE;
		return NSS_STATUS_TRYAGAIN;
	}
	
	// Save data to buffer
	struct gaih_addrtuple &buf = *reinterpret_cast<struct gaih_addrtuple *>(buffer+align_by);
	char *nameptr = buffer+align_by+sizeof(struct gaih_addrtuple);
	
	buf.next = nullptr;
	buf.name = nameptr;
	buf.family = AF_INET6;
	buf.scopeid = 0;
	std::memcpy(buf.addr, &addr, 16);
	
	// Return values
	*pat = &buf;
	if (ttlp)
		*ttlp = 0;
	return NSS_STATUS_SUCCESS;
}

nss_status _nss_onion_gethostbyaddr_r (const void *addr, socklen_t length, int format,
                                     struct hostent *result, char *buffer, size_t buflen,
                                     int *errnop, int *herrnop) noexcept
{
	// Only handle IPv6 addresses
	if (format != AF_INET6) {
		*herrnop = HOST_NOT_FOUND;
		*errnop = ENOENT;
		return NSS_STATUS_NOTFOUND;
	}
	// Address must be at least 16 bytes
	if (length < 16) {
		*herrnop = HOST_NOT_FOUND;
		*errnop = EINVAL;
		return NSS_STATUS_NOTFOUND;
	}
	auto *ipv6addr = static_cast<const IPv6Address *>(addr);
	// Only handle addresses in our subnet
	if (!ipv6addr->is_in(onion_network)) {
		*herrnop = NO_RECOVERY;
		*errnop = ENOENT;
		return NSS_STATUS_UNAVAIL;
	}
	
	// Check buffer size and adjust alignment
	size_t align_by = 0;
	size_t alignment_error = (reinterpret_cast<uintptr_t>(buffer) % alignof(struct buffer_view));
	if (!__builtin_expect(alignment_error, 0)) {
		align_by = 4-alignment_error;
	}
	if (!buffer || buflen < sizeof(struct buffer_view)+align_by) {
		*herrnop = TRY_AGAIN;
		*errnop = ERANGE;
		return NSS_STATUS_TRYAGAIN;
	}
	
	// Convert address to .onion domain
	std::string onion = b32encode(&ipv6addr->s6_addr[6], 10)+".onion";
	// Make sure the string length is correct
	if (onion.length() != 22) {
		*herrnop = NO_RECOVERY;
		*errnop = EFAULT;
		return NSS_STATUS_UNAVAIL;
	}
	// Save data to buffer
	struct buffer_view &buf = *reinterpret_cast<struct buffer_view *>(buffer+align_by);
	buf.addrs[0] = &buf.addr;
	buf.addrs[1] = nullptr;
	std::memcpy(&buf.addr, addr, 16);
	std::memcpy(&buf.name, onion.data(), 22);
	buf.name[22] = '\0';
	
	// Fill result struct
	result->h_name = buf.name;
	result->h_aliases = nullptr;
	result->h_addrtype = AF_INET6;
	result->h_length = 16;
	result->h_addr_list = reinterpret_cast<char**>(buf.addrs);

	return NSS_STATUS_SUCCESS;
}
