/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This library is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU Lesser General Public               *
 * License as published by the Free Software Foundation; either             *
 * version 3 of the License, or (at your option) any later version.       *
 *                                                                          *
 * This library is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * Lesser General Public License for more details.                          *
 *                                                                          *
 * You should have received a copy of the GNU Lesser General Public License *
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************/

#ifndef _NSS_ONION_H
#define _NSS_ONION_H
extern "C" {

#include <nss.h>
#include <sys/types.h>
#include <sys/socket.h>

#define EXPORT [[gnu::externally_visible]][[gnu::visibility("default")]]

struct hostent;
struct gaih_addrtuple;

EXPORT
enum nss_status _nss_onion_sethostent(int) noexcept;
EXPORT
enum nss_status _nss_onion_endhostent() noexcept;
EXPORT
enum nss_status _nss_onion_gethostent_r(struct hostent *result, char *buffer, size_t buflen,
                                        int *errnop, int *herrnop) noexcept;

EXPORT
enum nss_status _nss_onion_gethostbyname_r(const char *name, struct hostent *result, char *buffer,
                                           size_t buflen, int *errnop, int *herrnop) noexcept;

EXPORT
enum nss_status _nss_onion_gethostbyname2_r(const char *name, int af, struct hostent *result,
                                            char *buffer, size_t buflen, int *errnop,
                                            int *herrnop) noexcept;

EXPORT
enum nss_status _nss_onion_gethostbyname3_r(const char *name, int af, struct hostent *result,
                                            char *buffer, size_t buflen, int *errnop, int *herrnop,
                                            int32_t *ttlp, char **canonp) noexcept;

EXPORT
enum nss_status _nss_onion_gethostbyname4_r(const char *name, struct gaih_addrtuple **pat,
                                            char *buffer, size_t buflen, int *errnop, int *herrnop,
                                            int32_t *ttlp) noexcept;

EXPORT
enum nss_status _nss_onion_gethostbyaddr_r (const void *addr, socklen_t length, int format,
                                          struct hostent *result, char *buffer, size_t buflen,
                                          int *errnop, int *herrnop) noexcept;

}
#endif