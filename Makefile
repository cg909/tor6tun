#!/usr/bin/make -f
# (c) 2016 Christoph Grenz <christophg@grenz-bonn.de>
# License: GNU GPLv3 or later
# This file is part of tor6tun

all: tor6tun tor6dns nss_onion man

tor6tun: build
	@$(MAKE) -C build/tor6tun -f ../../tor6tun/Makefile

tor6dns: build
	@$(MAKE) -C build/tor6dns -f ../../tor6dns/Makefile

nss_onion: build
	@$(MAKE) -C build/nss_onion -f ../../nss_onion/Makefile

man: build
	@$(MAKE) -C build/man -f ../../man/Makefile

build:
	mkdir build build/tor6tun build/tor6dns build/nss_onion build/man

clean: build
	@$(MAKE) -C build/tor6tun -f ../../tor6tun/Makefile $@
	@$(MAKE) -C build/tor6dns -f ../../tor6dns/Makefile $@
	@$(MAKE) -C build/nss_onion -f ../../nss_onion/Makefile $@
	@$(MAKE) -C build/man -f ../../man/Makefile $@
	rmdir build/tor6tun || true
	rmdir build/tor6dns || true
	rmdir build/nss_onion || true
	rmdir build/man || true
	rmdir build

install: build
	@$(MAKE) -C build/tor6tun -f ../../tor6tun/Makefile $@
	@$(MAKE) -C build/tor6dns -f ../../tor6dns/Makefile $@
	@$(MAKE) -C build/nss_onion -f ../../nss_onion/Makefile $@
	@$(MAKE) -C build/man -f ../../man/Makefile $@

uninstall: build
	@$(MAKE) -C build/tor6tun -f ../../tor6tun/Makefile $@
	@$(MAKE) -C build/tor6dns -f ../../tor6dns/Makefile $@
	@$(MAKE) -C build/nss_onion -f ../../nss_onion/Makefile $@
	@$(MAKE) -C build/man -f ../../man/Makefile $@

.PHONY: all tor6tun tor6dns nss_onion man clean install uninstall