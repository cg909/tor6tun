/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************
 * Entry point:                                                             *
 * Argument parsing, signal handling and main loop                          *
 ****************************************************************************/

#include "tor6tun.h"
#include "call.h"
#include "ip_addr.h"
#include "logging.h"
#include "privdrop.h"
#include "select.h"
#include "signals.h"
#include "util.h"

#include <atomic>
#include <system_error>
#include <argp.h>
#include <unistd.h>
#include <net/if.h>
#ifdef SYSTEMD
#  include <systemd/sd-daemon.h>
#endif

/* Argument handling */

const char *argp_program_version = "tor6tun 1.0";

static struct argp_option options[] = {
	{"proxy",   'p', "HOST", 0, "SOCKS5 proxy to use (default: 127.0.0.1:9050)" },
	{"tun",     't', "NAME", 0, "interface name for TUN device (default: onion?)" },
	{"onion-network", 'o', "SUBNET",  0, "IPv6 subnet for .onion domain mapping (default: fd87:7026:eb43::/48)" },
	{"user", 'u', "USER",  0, "setuid to user after initialization" },
	{"port", 'P', "PORT",  0, "internally used TCP port (default: random)" },
	{"max-requests", 'm', "NUMBER",  0, "maximum number of concurrent TCP connection requests (default: 65535)" },
	{"max-connections", 'M', "NUMBER",  0, "maximum number of concurrent TCP connections (default: 65535)" },
	{"legacy-daemonize", 'D', 0,  0, "detach and run in background" },
	{0, 'v', 0,  0, "increase verbosity" },
	{0, 'q', 0,  0, "decrease verbosity" },
	{ 0 }
};

struct arguments
{
	std::string proxy;
	std::string tun;
	std::string user;
	IPv6Network onion_network;
	std::string port;
	int verbosity;
	unsigned long max_connections, max_connreqs;
	bool daemonize;
};

/* Parse a single option. */
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
	struct arguments *arguments = static_cast<struct arguments *>(state->input);
	size_t tmp;

	switch (key) {
	case 'p': {
		arguments->proxy = arg;
		auto i = arguments->proxy.rfind(':');
		auto j = arguments->proxy.find(']');
		if (i == std::string::npos or (j != std::string::npos and i <= j)) {
			log::error("argument for --proxy must contain both a host and a port");
			return ARGP_ERR_UNKNOWN;
		}
		if (arguments->proxy.find_first_of("\0\t \n_@/", 0, 7) != std::string::npos) {
			log::error("argument for --proxy contains invalid characters");
			return EINVAL;
		}
		break;
	}
	case 't': {
		arguments->tun = arg;
		size_t i = arguments->tun.find('?');
		if (i != std::string::npos)
			arguments->tun.replace(i, 1, "%d");
		if (arguments->tun.length() >= IFNAMSIZ) {
			log::error("argument for --tun is too long");
			return EINVAL;
		}
		break;
	}
	case 'o': {
		std::string text(arg);
		if (text.find('/') == std::string::npos) {
			text.append("/48");
		}
		try {
			arguments->onion_network = text;
		} catch (std::invalid_argument &exc) {
			log::error("invalid argument for --onion-network: ", exc.what());
			return EINVAL;
		}
		if (arguments->onion_network.prefixlen != 48) {
			log::error("invalid argument for --onion-network: must be a /48 subnet");
			return EINVAL;
		}
		break;
	}
	case 'u':
		arguments->user = arg;
		break;
	case 'P':
		arguments->port = arg;
		break;
	case 'v':
		if (arguments->verbosity < log::DEBUG)
			arguments->verbosity++;
		break;
	case 'q':
		if (arguments->verbosity >= log::CRITICAL)
			arguments->verbosity--;
		break;
	case 'm':
		try {
			std::string text(arg);
			arguments->max_connreqs = std::stoul(text, &tmp, 0);
			if (tmp != text.length()) {
				log::error("invalid argument for --max-requests: must be an integer");
				return EINVAL;
			}
		} catch (std::invalid_argument &exc) {
			log::error("invalid argument for --max-requests: must be an integer");
			return EINVAL;
		} catch (std::out_of_range &exc) {
			log::error("invalid argument for --max-requests: number too big");
			return EINVAL;
		}
		break;
	case 'M':
		try {
			std::string text(arg);
			arguments->max_connections = std::stoul(text, &tmp, 0);
			if (tmp != text.length()) {
				log::error("invalid argument for --max-connections: must be an integer");
				return EINVAL;
			}
		} catch (std::invalid_argument &exc) {
			log::error("invalid argument for --max-connections: must be an integer");
			return EINVAL;
		} catch (std::out_of_range &exc) {
			log::error("invalid argument for --max-connections: number too big");
			return EINVAL;
		}
		break;
	case 'D':
		arguments->daemonize = true;
		break;
	case ARGP_KEY_ARG:
		/* Too many arguments. */
		argp_usage (state);
		return ARGP_ERR_UNKNOWN;
		break;

	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static struct argp argp = { options, parse_opt, "", "IPv6 layer 3 proxy for TOR" };

/* Signal handling */

static volatile sig_atomic_t exit_requested = 0;
static siginfo_t signal_info {0, 0, 0};

[[gnu::cold]]
static void term_handler(int signal, siginfo_t *siginfo, void *) noexcept
{
	static const signalset exit_sigs {SIGINT, SIGTERM};

	// Check and set termination flag
	if (!exit_requested) {
		signal_info = *siginfo;
		if (exit_sigs.contains(signal)) {
			exit_requested = 1;
		} else {
			exit_requested = 2;
		}

		signal_info = *siginfo;
	}
}

/* Notifications */

static void notify_ready()
{
	log::info("ready");
	#ifdef SYSTEMD
	sd_notify(0, "READY=1");
	#endif
}

[[gnu::cold]]
static void notify_terminating()
{
	std::cout.flush();
	if (signal_info.si_signo == SIGXCPU
	  and signal_info.si_code == SI_KERNEL) {
		log::warn("CPU time limit exceeded");
	} else if (signal_info.si_code == SI_USER
	  or signal_info.si_code == SI_QUEUE) {
		log::debug("signal ", signal_info.si_signo, " received from process ", signal_info.si_pid);
	}
	log::info("terminating...");
	#ifdef SYSTEMD
	sd_notify(0, "STOPPING=1");
	#endif
}

/* Main function */

int main(int argc, char* argv[]) {
	struct arguments arguments;
	bool had_connections;

	// Argument defaults
	arguments.tun = "onion%d";
	arguments.proxy = "127.0.0.1:9050";
	arguments.onion_network = "fd87:7026:eb43::/48";
	arguments.port = "0";
	arguments.verbosity = log::INFO;
	arguments.max_connections = 65535;
	arguments.max_connreqs = 65535;
	arguments.daemonize = false;

	// Parse arguments
	if (argp_parse (&argp, argc, argv, 0, 0, &arguments)) {
		return 2;
	}

	// Set signal handlers
	signalset sigmask {signalset::full()};
	sigmask.remove({SIGILL, SIGSEGV, SIGABRT});
	sigaction(
		{SIGTERM, SIGXCPU, SIGXFSZ},
		term_handler,
		sigmask,
		SA_RESETHAND
	);
	sigaction(
		{SIGHUP, SIGUSR1, SIGUSR2, SIGPIPE},
		SIG_IGN
	);

	if (sigaction(SIGINT).sa_handler != SIG_IGN) {
		sigaction(
			{SIGINT},
			term_handler,
			sigmask,
			SA_RESETHAND
		);
	}
	if (sigaction(SIGQUIT).sa_handler != SIG_IGN) {
		sigaction(
			{SIGQUIT},
			term_handler,
			sigmask,
			SA_RESETHAND
		);
	}

	// Set loglevel
	log::loglevel = static_cast<enum log::loglevels>(arguments.verbosity);

	// Run main program
	try {
		Tor6TunApplication mainprog(arguments.tun, arguments.proxy, arguments.onion_network, arguments.port);
		drop_privileges(arguments.user);
		mainprog.set_max_connections(arguments.max_connections);
		mainprog.set_max_connreqs(arguments.max_connreqs);
		if (chdir("/"))
			throw_errno("chdir() failed");

		notify_ready();
		if (arguments.daemonize) {
			daemonize(true);
		}

		while (!exit_requested) {
			mainprog.process();
		}
		// Notify user about pending termination
		notify_terminating();
		// Shutdown connections
		had_connections = mainprog.shutdown();
	}
	catch (std::system_error & exc) {
		const std::error_category &cat = exc.code().category();
		int code = exc.code().value();

		std::cout.flush();
		log::critical(exc.what(), " [", exc.code().value(), ']');
		if (cat == std::generic_category()) {
			return (code == EPERM)
				? 77 // EX_NOPERM
				: 71; // EX_OSERR
		} else {
			return 71; // EX_OSERR
		}
	}
	catch (CalledProcessError & exc) {
		std::cout.flush();
		log::critical(exc.what());
		return 69; // EX_UNAVAIL
	}
	catch (std::runtime_error & exc) {
		std::cout.flush();
		log::critical("runtime error: ", exc.what());
		return 70; // EX_SOFTWARE
	}
	catch (std::invalid_argument & exc) {
		std::cout.flush();
		log::critical("invalid argument: ", exc.what());
		return 64; // EX_USAGE
	}
	catch (std::exception & exc) {
		throw;
	}
	if (had_connections) {
		usleep(500);
	}
	// Reraise signal if not a normal termination request
	if (exit_requested == 2) {
		::kill(::getpid(), signal_info.si_signo);
	}
}
