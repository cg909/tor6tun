/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************/

#ifndef _TORV6PROXY_H
#define _TORV6PROXY_H

#include "asyncsocket.h"
#include "ip_addr.h"
#include "select.h"
#include "socket.h"
#include "tuntap.h"

#include <string>
#include <map>

typedef std::vector<uint8_t> bytevector;

struct ConnKey final {
	IPv6Address addr;
	uint16_t port;

	bool operator == (const ConnKey &other) const {
		return addr == other.addr and port == other.port;
	}
	bool operator < (const ConnKey &other) const {
		if (addr < other.addr)
			return true;
		if (addr == other.addr)
			return port < other.port;
		return false;
	}
	bool operator <= (const ConnKey &other) const {
		if (addr < other.addr)
			return true;
		if (addr == other.addr)
			return port <= other.port;
		return true;
	}
	bool operator > (const ConnKey &other) const {
		return !operator <= (other);
	}
	bool operator >= (const ConnKey &other) const {
		return !operator < (other);
	}
};

std::ostream& operator << (std::ostream&, const ConnKey&);

struct Connection final {
	IPv6Address source;
	IPv6Address destination;
	uint16_t sport;
	uint16_t dport;
	AsyncSocket client_socket;
	AsyncSocket socks_socket;
	
	bool operator == (const Connection &other) const {
		return source == other.source
			and destination == other.destination
			and sport == other.sport
			and dport == other.dport;
	}
};

class Tor6TunApplication final {
	typedef std::map<ConnKey, Connection> connections_t;
	typedef std::map<ConnKey, bytevector> connreqs_t;
	typedef std::vector< std::pair<time_t, ConnKey> > closing_t;
	
	int tunnel_proxy_port;
	std::string socks_server;
	struct sockaddr_in6 socks_addr;
	IPv6Network transfer_network;
	IPv6Network onion_network;
	Socket tcp_sock;
	TunDevice tun;
	connections_t connections;
	connreqs_t connreqs;
	closing_t closing;
	Selector selector;
	unsigned long max_connections, max_connreqs;
	time_t last_router_adv;
	bool in_shutdown;

	Tor6TunApplication(const Tor6TunApplication&) = delete;
	Tor6TunApplication& operator= (const Tor6TunApplication&) = delete;
	void cleanup_closing_connections();
	std::string setup_tun();
	[[gnu::hot]]
	void on_tun_packet();
	void on_new_connection();
	void on_socks_auth(const ConnKey&, AsyncSocket&);
	void on_socks_assoc(const ConnKey&, AsyncSocket&);
	[[gnu::hot]]
	void on_conn_recv(const ConnKey&, AsyncSocket&, AsyncSocket&);
	void on_conn_send(const ConnKey&, AsyncSocket&, AsyncSocket&);
	void handle_tun_icmpv6_packet(PacketBufferView packet);
	[[gnu::hot]]
	void handle_tun_tcp_packet(PacketBufferView packet, bytevector &buffer);
	[[gnu::hot]]
	void handle_tun_tcp_reply(PacketBufferView packet, bytevector &buffer);
	void send_icmpv6_error(const PacketBufferView old_packet, uint8_t type, uint8_t code=0, uint32_t data=0);
	void send_tcp_rst(const PacketBufferView old_packet);
	void send_router_adv(bool active=true);
	
public:
	Tor6TunApplication(const std::string &tun, const std::string &proxy, const IPv6Network &onion, const std::string &port);
	unsigned long get_max_connreqs();
	unsigned long get_max_connections();
	void set_max_connreqs(unsigned long);
	void set_max_connections(unsigned long);
	void process();
	bool shutdown();
};

#endif