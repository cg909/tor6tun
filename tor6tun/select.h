/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************/

#ifndef _SELECT_H
#define _SELECT_H

#include "packetbuffer.h"

#include <stdexcept>
#include <map>
#include <functional>
#include <sys/types.h>
#include <sys/select.h>

class Selector final {
	typedef std::function<void()> callback_t;
	class accessor final {
		accessor() noexcept;
		std::map<int, callback_t> callbacks;
		fd_set fds;
		int nfds;
	public:
		accessor(const accessor &) = delete;
		accessor& operator= (accessor&&) = delete;
		accessor& operator= (const accessor&) = delete;
		template <typename T>
		void add(T &, std::function<void(T&)>);
		template <typename T>
		void add(T &, void(*)(T&));
		template <typename T>
		void add(T &, std::function<void()>);
		template <typename T>
		void replace(T &, std::function<void(T&)>);
		template <typename T>
		void replace(T &, void(*)(T&));
		template <typename T>
		void replace(T &, std::function<void()>);
		template <typename T>
		bool remove(const T &);
		template <typename T>
		bool has(const T &) const noexcept;
		void clear();
		friend class Selector;
	};
	int get_nfds() const noexcept;
	int select(struct timeval *) const;
	void do_callbacks(int&, const accessor&, fd_set&) const;
public:
	Selector();
	int select() const;
 	int select(double timeout) const;
	accessor read, write, except;
};

template <typename T>
void Selector::accessor::add(T &f, std::function<void()> callback)
{
	int fd = f.fileno();
	auto result = callbacks.insert(std::make_pair(fd, callback));
	if (!result.second)
		throw std::invalid_argument("fd already in list");
	nfds = std::max(nfds, fd+1);
	FD_SET(fd, &fds);
}

template <typename T>
void Selector::accessor::add(T &f, std::function<void(T&)> callback)
{
	std::function<void()> cb = std::bind(callback, std::ref(f));
	add(f, cb);
}

template <typename T>
void Selector::accessor::add(T &f, void(*cb)(T&))
{
	add(f, std::function<void(T&)>(cb));
}

template <typename T>
void Selector::accessor::replace(T &f, std::function<void()> callback)
{
	int fd = f.fileno();
	auto it = callbacks.find(fd);
	if (it == callbacks.end())
		throw std::out_of_range("fd not in list");
	it->second = callback;
}

template <typename T>
void Selector::accessor::replace(T &f, std::function<void(T&)> callback)
{
	std::function<void()> cb = std::bind(callback, &f);
	replace(f, cb);
}

template <typename T>
void Selector::accessor::replace(T &f, void(*cb)(T&))
{
	replace(f, std::function<void(T&)>(cb));
}

template <typename T>
bool Selector::accessor::remove(const T &f)
{
	int fd = f.fileno();
	if (fd == -1)
		return false;
	FD_CLR(fd, &fds);
	bool result = callbacks.erase(fd) != 0;

	if (result) {
		auto it = callbacks.crbegin();
		if (it != callbacks.crend())
			nfds = it->first+1;
	}
	return result;
}

template <typename T>
bool Selector::accessor::has(const T &f) const noexcept
{
	int fd = f.fileno();
	auto it = callbacks.find(fd);
	return (it != callbacks.end());
}

inline void Selector::accessor::clear()
{
	FD_ZERO(&fds);
	nfds = 0;
	callbacks.clear();
}

#endif
