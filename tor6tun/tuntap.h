/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************/

#ifndef _TUNTAP_H
#define _TUNTAP_H

#include <sys/types.h>
#include <linux/if_tun.h>
#include <string>
#include <vector>

struct PacketBufferView;

class TunTapDevice {
	std::string name;
	unsigned short flags;
	int fd;
protected:
	TunTapDevice(const std::string &name, unsigned short flags);
	size_t recv(void* buffer, size_t maxlen=1500);
	size_t send(const void* buffer, size_t len);
public:
	size_t send(const std::string& buffer);
	size_t send(const PacketBufferView& buffer);
	template<typename T>
	size_t send(const std::vector<T>& buffer) {
		return send(&buffer[0], sizeof(T)*buffer.size());
	}
	bool recv(std::string& buffer, size_t maxlen=1500);
	template<typename T>
	bool recv(std::vector<T>& buffer) {
		buffer.resize(buffer.capacity());
		size_t result = recv(&buffer[0], sizeof(T)*buffer.capacity());
		buffer.resize(result / sizeof(T));
		return result;
	}
	int fileno() const {
		return fd;
	}
	void attach_queue();
	void detach_queue();
	void close() noexcept;
	
	bool has_pi_headers() const {
		return !(flags & IFF_NO_PI);
	}
	bool has_multiqueue() const {
		return flags & IFF_MULTI_QUEUE;
	}
	void set_blocking(bool value);
	void set_cloexec(bool value);
	const std::string &get_name() const {
		return name;
	}
	virtual ~TunTapDevice() noexcept;
	TunTapDevice(const TunTapDevice&) = delete;
	TunTapDevice(TunTapDevice&&) noexcept;
};

class TunDevice: public TunTapDevice {
public:
	explicit TunDevice(const std::string &name, unsigned short flags=0)
		: TunTapDevice(name, flags|IFF_TUN)
	{}
};

class TapDevice: public TunTapDevice {
public:
	explicit TapDevice(const std::string &name, unsigned short flags=0)
		: TunTapDevice(name, flags|IFF_TAP)
	{}
};

struct tun_frame_header {
	uint16_t flags;
	uint16_t proto;
};

#endif