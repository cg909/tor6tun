[Unit]
Description=IPv6 layer 3 proxy for TOR
Documentation=man:tor6tun(8)
Requires=tor.service
After=tor.service
Wants=tor6dns.service

[Service]
Type=notify
ExecStart=/usr/local/sbin/tor6tun -u nobody

[Install]
WantedBy=multi-user.target