/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************
 * Buffered socket wrapper class for asynchronous handling.                 *
 ****************************************************************************/

#include "asyncsocket.h"
#include "util.h"

#include <stdexcept>
#include <unistd.h>

AsyncSocket::AsyncSocket() noexcept
{
	recvbuf.reserve(2048);
	sendbuf.reserve(2048);
}

AsyncSocket::AsyncSocket(Socket&& sock) noexcept
	: sock(std::move(sock))
{
	recvbuf.reserve(2048);
	sendbuf.reserve(2048);
}

AsyncSocket::AsyncSocket(AsyncSocket &&o) noexcept
	: sock(std::move(o.sock)), sendbuf(std::move(o.sendbuf)), recvbuf(std::move(o.recvbuf))
{}

AsyncSocket& AsyncSocket::operator = (AsyncSocket &&other) noexcept
{
	sock = std::move(other.sock);
	recvbuf = std::move(other.recvbuf);
	sendbuf = std::move(other.sendbuf);
	return *this;
}

AsyncSocket& AsyncSocket::operator = (Socket &&other) noexcept
{
	sock = std::move(other);
	return *this;
}

AsyncSocket::~AsyncSocket() noexcept
{}

void AsyncSocket::close() noexcept
{
	sock.close();
}

void AsyncSocket::shutdown(int how)
{
	if (how == SHUT_WR || how == SHUT_RDWR)
		flush();
	sock.shutdown(how);
}

bool AsyncSocket::recv(std::vector<uint8_t>& buffer, size_t size)
{
	// Get more data if not enough in recv buffer
	if (recvbuf.size() < size or !recvbuf.size()) {
		size_t rsize = size;
		if (rsize != 0)
			rsize -= recvbuf.size();
		else
			recvbuf.reserve(1024);
		sock.recv(recvbuf, rsize, recvbuf.size());
	}

	// If enough data, copy to user buffer
	if (recvbuf.size() >= size && recvbuf.size()) {
		if (size == 0)
			size = std::min(recvbuf.size(), buffer.capacity());
		buffer.resize(size);
		std::memcpy(buffer.data(), recvbuf.data(), size);
		// Move remaining recv buffer contents
		size_t rest = recvbuf.size()-size;
		if (rest)
			std::memmove(&recvbuf[0], &recvbuf[size], rest);
		recvbuf.resize(rest);
		return true;
	}

	return false;
}

bool AsyncSocket::send(const void* buffer, size_t len)
{
	size_t result;
	if (sendbuf.size() == 0) {
		// Simple case: sendbuf is empty, send directly
		result = sock.send(buffer, len, SOCKET_DEFAULT_SENDFLAGS|MSG_MORE);
		// Copy remaining data to buffer
		if (result < len) {
			sendbuf.resize(len - result);
			std::memcpy(sendbuf.data(), static_cast<const uint8_t*>(buffer)+result, len-result);
			return false;
		}
		return true;
	} else {
		// There is data in the sendbuf.
		// Append new data to buffer and send it.
		size_t buffered = sendbuf.size();
		sendbuf.resize(buffered+len);
		std::memcpy(&sendbuf[buffered], buffer, len);
		result = sock.send(sendbuf, SOCKET_DEFAULT_SENDFLAGS|MSG_MORE);
		// Move remaining buffer contents
		if (result < sendbuf.size()) {
			size_t rest = sendbuf.size()-result;
			if (rest)
				std::memmove(&recvbuf[0], &recvbuf[result], rest);
			sendbuf.resize(rest);
			return false;
		}
		return true;
	}
}

bool AsyncSocket::send(const PacketBufferView& buffer)
{
	return send(buffer.pointer(), buffer.size());
}

bool AsyncSocket::send(const ConstPacketBufferView& buffer)
{
	return send(buffer.pointer(), buffer.size());
}

bool AsyncSocket::send(const std::string& buffer)
{
	return send(buffer.data(), buffer.length());
}

bool AsyncSocket::flush()
{
	size_t result;
	if (sendbuf.size() == 0) {
		sock.send(sendbuf.data(), 0);
		return true;
	} else {
		result = sock.send(sendbuf);
		// Move remaining buffer contents
		if (result < sendbuf.size()) {
			size_t rest = sendbuf.size()-result;
			if (rest)
				memmove(&sendbuf[0], &sendbuf[result], rest);
			sendbuf.resize(rest);
			return false;
		}
		return true;
	}
}

unsigned int AsyncSocket::domain() const
{
	return sock.domain();
}

unsigned int AsyncSocket::type() const
{
	return sock.type();
}

unsigned int AsyncSocket::protocol() const
{
	return sock.protocol();
}
