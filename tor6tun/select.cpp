/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************
 * C++ style select() wrapper using callback functions.                     *
 ****************************************************************************/

#include "select.h"
#include "util.h"

#ifndef FD_COPY
#define FD_COPY(fd_orig, fd_dest) *fd_dest = *fd_orig;
#endif

Selector::accessor::accessor() noexcept
{
	FD_ZERO(&fds);
}

Selector::Selector()
	: read(), write(), except()
{}

int Selector::get_nfds() const noexcept
{
	int result = 0;
	result = std::max(read.nfds, result);
	result = std::max(write.nfds, result);
	result = std::max(except.nfds, result);
	return result;
}

void Selector::do_callbacks(int &counter, const accessor &ac, fd_set &fds) const
{
	auto it = ac.callbacks.cbegin();
	do {
		it = ac.callbacks.cbegin();
		while (it != ac.callbacks.cend()) {
			auto pair = *(it++);
			if (!counter)
				break;
			if (FD_ISSET(pair.first, &fds)) {
				FD_CLR(pair.first, &fds);
				pair.second();
				--counter;
				break;
			}
		}
	} while (counter && it != ac.callbacks.cend());
}

inline int Selector::select(struct timeval *timeval) const
{
	int result, counter, nfds;
	fd_set rd, wr, ex;

	FD_COPY(&read.fds, &rd);
	FD_COPY(&write.fds, &wr);
	FD_COPY(&except.fds, &ex);
	nfds = get_nfds();
	result = ::select(nfds, &rd, &wr, &ex, timeval);
	if (result == -1) {
		if (errno != EINTR)
			throw_errno("select() failed");
		else
			return 0;
	}
	counter = result;
	do_callbacks(counter, read, rd);
	do_callbacks(counter, write, wr);
	if (__builtin_expect(counter, 0))
		do_callbacks(counter, except, ex);
	return result;
}

int Selector::select() const
{
	return select(nullptr);
}

int Selector::select(double timeout) const
{
	struct timeval timeval;

	if (timeout < 0) {
		throw std::invalid_argument("invalid timeout");
	}

	timeval.tv_sec = static_cast<unsigned long>(timeout);
	timeval.tv_usec = static_cast<unsigned long>(timeout * 1000000) % 1000000;

	return select(&timeval);
}
