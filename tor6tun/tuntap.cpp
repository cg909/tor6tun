/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************
 * TUN/TAP device wrapper classes.                                          *
 ****************************************************************************/

#include "tuntap.h"
#include "packetbuffer.h"
#include "util.h"

#include <exception>
#include <system_error>
#include <sys/ioctl.h>
#include <net/if.h>
#include <cstring>
#include <unistd.h>
#include <fcntl.h>

#define TUN_KO_PATH "/dev/net/tun"

namespace {
	struct ifrequest final: public ::ifreq {
		ifrequest(const std::string &name, unsigned short flags) {
			zero_init(*this);
			std::strncpy(this->ifr_name, name.c_str(), IFNAMSIZ);
			this->ifr_flags = flags;
		}
	};
}

TunTapDevice::TunTapDevice(const std::string &name, unsigned short flags)
	: name(name), flags(flags), fd(-1)
{
	// open tun control device
	fd = open(TUN_KO_PATH, O_RDWR);
	if (!fd)
		throw_errno("opening " TUN_KO_PATH " failed");
	// allocate a TUN/TAP device
	try {
		ifrequest request(name, flags);
		if (ioctl(fd, TUNSETIFF, &request) < 0)
			throw_errno("allocating TUN/TAP device failed");
		this->name.assign(request.ifr_name, strnlen(request.ifr_name, IFNAMSIZ));
	}
	catch (std::exception &exc) {
		close();
		throw;
	}
}

TunTapDevice::TunTapDevice(TunTapDevice &&o) noexcept
	: name(o.name), flags(o.flags), fd(o.fd)
{
	o.fd = -1;
}

void TunTapDevice::close() noexcept
{
	if (fd != -1) {
		::close(fd);
		fd = -1;
	}
}

TunTapDevice::~TunTapDevice() noexcept
{
	close();
}

size_t TunTapDevice::send(const void *buffer, size_t len)
{
	ssize_t result;
	do {
		result = write(fd, buffer, len);
	} while (result == -1 and errno == EINTR);
	
	if (result == -1) {
		if (errno == EAGAIN)
			return 0;
		throw_errno("writing to TUN/TAP device failed");
	}
	return result;
}

size_t TunTapDevice::send(const PacketBufferView& buffer)
{
	return send(buffer.pointer(), buffer.size());
}

size_t TunTapDevice::send(const std::string& buffer) {
	return send(buffer.data(), buffer.length());
}

size_t TunTapDevice::recv(void *buffer, size_t maxlen)
{
	ssize_t result;
	do {
		result = read(fd, buffer, maxlen);
	} while (result == -1 and errno == EINTR);
	
	if (result == -1) {
		if (errno == EAGAIN) {
			return 0;
		}
		else
			throw_errno("reading from TUN/TAP device failed");
	}
	return result;
}

bool TunTapDevice::recv(std::string& buffer, size_t maxlen) {
	size_t result;
	char * const buf = new char[maxlen];
	try {
		result = recv(buf, maxlen);
	}
	catch (std::exception &exc) {
		delete[] buf;
		throw;
	}
	if (!result)
		buffer.clear();
	else
		buffer.assign(buf, result);
	delete[] buf;
	return result != 0;
}

void TunTapDevice::attach_queue() {
	ifrequest request("", IFF_ATTACH_QUEUE);
	if (ioctl(fd, TUNSETQUEUE, &request) < 0)
		throw_errno("IFF_ATTACH_QUEUE failed");
}

void TunTapDevice::detach_queue() {
	ifrequest request("", IFF_DETACH_QUEUE);
	if (ioctl(fd, TUNSETQUEUE, &request) < 0)
		throw_errno("IFF_DETACH_QUEUE failed");
}

void TunTapDevice::set_blocking(bool value) {
	int flags = fcntl(fd, F_GETFL);
	if (flags == -1)
		throw_errno("F_GETFL failed");
	if (value)
		flags |= O_NONBLOCK;
	else
		flags &= ~O_NONBLOCK;
	if (fcntl(fd, F_SETFL, flags) == -1)
		throw_errno("F_SETFL failed");
}

void TunTapDevice::set_cloexec(bool value)
{
	int flags = fcntl(fd, F_GETFD);
	if (flags == -1)	
		throw_errno("F_GETFD failed");
	if (value)
		flags |= FD_CLOEXEC;
	else
		flags &= ~FD_CLOEXEC;
	if (fcntl(fd, F_SETFD, flags) == -1)
		throw_errno("F_SETFD failed");
}
