#!/usr/bin/make -f
# (c) 2016 Christoph Grenz <christophg@grenz-bonn.de>
# License: GNU GPLv3 or later
# This file is part of tor6tun

SRCPATH := $(shell dirname $(lastword $(MAKEFILE_LIST)))
COMMONPATH := $(shell dirname $(lastword $(MAKEFILE_LIST)))/../common
VPATH := $(SRCPATH):$(SRCPATH)/../common

include $(COMMONPATH)/Makefile.common

ifdef DEBUG
CXXFLAGS ?= -O0 -g -fPIE -fno-rtti -Wall -Wextra -Wpedantic
else
CXXFLAGS ?= -Os -flto -fPIE -fno-rtti -Wall -Wextra -Wpedantic
endif
LDFLAGS ?= -pie
CXXINCL = -I$(SRCPATH) -I$(COMMONPATH)

SOURCES = main.cpp tor6tun.cpp asyncsocket.cpp base32.cpp call.cpp \
	getaddrinfo.cpp ip_addr.cpp ip_packets.cpp logging.cpp \
	privdrop.cpp select.cpp signals.cpp socket.cpp tuntap.cpp \
	if_addrs.cpp

all: tor6tun tor6tun.service

tor6tun: $(SOURCES:.cpp=.o)
	$(CXX) -std=c++11 $(CXXFLAGS) $(LDFLAGS) -o "$@" $^ $(LIBS)

main.o: main.cpp Makefile
	$(CXX) -c -std=c++11 $(DEFINES) $(CXXFLAGS) -Wno-missing-field-initializers $(CXXINCL) -o "$@" "$<"

clean:
	rm $(SOURCES:.cpp=.o) || true
	rm $(SOURCES:%.cpp=%.dep) || true
	rm tor6tun.service || true
	rm tor6tun || true

install: all
	install -m 0755 -o root -g root -s tor6tun "$(PREFIX)"/sbin/
	install -m 0755 -d "$(PREFIX)"/lib/systemd/system
	install -m 0644 -o root -g root tor6tun.service "$(PREFIX)"/lib/systemd/system/

uninstall:
	rm "$(PREFIX)"/sbin/tor6tun || true
	rm "$(PREFIX)"/lib/systemd/system/tor6tun.service || true
	rmdir --ignore-fail-on-non-empty "$(PREFIX)"/lib/systemd/system "$(PREFIX)"/lib/systemd

.PHONY: all clean install uninstall

-include $(SOURCES:%.cpp=%.dep)
