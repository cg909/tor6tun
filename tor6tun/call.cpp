/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************
 * posix_spawn/execvp() wrapper functions for safe subprocess instatiation. *
 ****************************************************************************/

#include "call.h"

#include "util.h"
#include "logging.h"

#include <unistd.h>
#include <spawn.h>
#include <sys/wait.h>
#include <cstring>
#include <string>
#include <sstream>
#include <system_error>
#include <vector>

extern char **environ;

/**
 * @internal
 * @brief OpenBSD style exit codes (range 64 - 78)
 */
static const char * const sysexits[] = {
	"incorrect usage",
	"input data incorrect",
	"no input",
	"user doesn't exist",
	"host doesn't exist",
	"service unavailable",
	"internal software error",
	"OS error",
	"system file error",
	"cannot create output file",
	"I/O error",
	"temporary failure",
	"protocol error",
	"no permission",
	"misconfiguration"
};

/**
 * @internal
 * @brief builds the what() message for CalledProcessError
 */
static std::string makeCalledProcessErrorWhat(const std::string &what, int code)
{
	std::string name;
	if (code < 0) {
		name = strsignal(-code);
	} else if (code == 0) {
		name = "success";
	} else if (code >= 64 and code <= 78) {
		name = sysexits[code-64];
	} else if (code == 127) {
		name = "command not found";
	}
	if (name.empty()) {
		name = "failure";
	}

	std::ostringstream ss;
	ss << what << ": " << name << " [" << code << "]";
	return ss.str();
}

CalledProcessError::CalledProcessError(int exitStatus, const std::string& cmdline)
	: std::runtime_error(makeCalledProcessErrorWhat(cmdline, exitStatus)),
	  exit_status(exitStatus)
{}

static inline std::string debug_cmdline(std::initializer_list<std::string> args)
{
	std::ostringstream ss;
	for (auto &arg : args)
	{
		if (arg.find(' ') == std::string::npos) {
			ss << arg << ' ';
		} else {
			ss << '"' << arg << "\" ";
		}
	}
	auto result = ss.str();
	result.pop_back();
	return result;
}

pid_t spawn(std::initializer_list<std::string> args)
{
	const char *filename = args.begin()->c_str();
	std::vector<const char*> argv;
	argv.reserve(args.size()+1);

	for (auto &arg : args)
	{
		argv.push_back(arg.c_str());
	}
	argv.push_back(nullptr);

	pid_t pid;
	int status = posix_spawnp(&pid, filename, nullptr, nullptr, const_cast<char* const*>(&argv[0]), environ);
	if (status != 0) {
		throw_errno("posix_spawn() failed", status);
	}

	return pid;
}

int call(std::initializer_list<std::string> args)
{
	pid_t pid = spawn(args);

	int result;
	siginfo_t status;
	zero_init(status);
	do {
		result = waitid(P_PID, pid, &status, WEXITED);
	} while (result == -1 and errno == EINTR);

	if (result == -1)
		throw_errno("waitid() failed");

	switch (status.si_code) {
		case CLD_EXITED:
			return status.si_status;
		case CLD_DUMPED:
			log::error(debug_cmdline(args), " [" ,pid, "] dumped core on signal ", status.si_status, " (", strsignal(status.si_status), ')');
		// fall through
		case CLD_KILLED:
			return -status.si_status;
		default:
			throw std::runtime_error("waitid() returned unexpected si_code");
	}
}

void check_call(std::initializer_list<std::string> args)
{
	int code = call(args);

	if (code != 0) {
		throw CalledProcessError(code, debug_cmdline(args));
	}
}
