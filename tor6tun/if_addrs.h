/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************/

#ifndef _IF_ADDRS_H
#define _IF_ADDRS_H

#include <sys/types.h>
#include <net/if.h>
#include <ifaddrs.h>

class IfAddrsResultIterator final {
	struct ifaddrs *ptr;
public:
	constexpr IfAddrsResultIterator(): ptr(nullptr) {}
	constexpr IfAddrsResultIterator(struct ifaddrs *ptr): ptr(ptr) {}
	constexpr const struct ifaddrs &operator *() const { return *ptr; }
	struct ifaddrs &operator *() { return *ptr; }
	explicit constexpr operator bool() const { return ptr != nullptr; }
	IfAddrsResultIterator& operator ++();
	IfAddrsResultIterator operator ++(int);
	constexpr bool operator ==(const IfAddrsResultIterator &other) const
	{ return other.ptr == ptr; }
	constexpr bool operator !=(const IfAddrsResultIterator &other) const
	{ return other.ptr != ptr; }
};

class IfAddrsResult final
{
	struct ifaddrs *addrs;
public:
	typedef IfAddrsResultIterator iterator;
	constexpr IfAddrsResult(struct ifaddrs *addrs)
		: addrs(addrs) {}
	IfAddrsResult(IfAddrsResult &&other);
	IfAddrsResult(const IfAddrsResult &) = delete;
	~IfAddrsResult();
	iterator begin() { return addrs; }
	const iterator& end() { return null_it; }
private:
	static constexpr const iterator null_it {};
};

IfAddrsResult getifaddrs();

#endif