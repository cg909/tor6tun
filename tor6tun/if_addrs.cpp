/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************
 * getifaddrs() wrapper.                                                    *
 ****************************************************************************/

#include "if_addrs.h"
#include "util.h"

#include <stdexcept>

IfAddrsResult::iterator& IfAddrsResult::iterator::operator ++()
{
	if (ptr == nullptr)
		throw std::out_of_range("IfAddrsResult::iterator out of bounds");
	ptr = ptr->ifa_next;
	return *this;
}

IfAddrsResult::iterator IfAddrsResult::iterator::operator ++(int)
{
	IfAddrsResult::iterator copy = *this;
	if (ptr == nullptr)
		throw std::out_of_range("IfAddrsResult::iterator out of bounds");
	ptr = ptr->ifa_next;
	return copy;
}

IfAddrsResult::IfAddrsResult(IfAddrsResult &&other)
{
	addrs = other.addrs;
	other.addrs = nullptr;
}

IfAddrsResult::~IfAddrsResult()
{
	if (addrs)
		freeifaddrs(addrs);
}

IfAddrsResult getifaddrs()
{
	struct ifaddrs *addrs;
	if (::getifaddrs(&addrs) != 0)
		throw_errno("getifaddrs() failed");
	return IfAddrsResult(addrs);
}
