/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************/

#ifndef _CALL_H
#define _CALL_H

#include <string>
#include <initializer_list>
#include <stdexcept>

/**
 * Thrown when a process run by check_call() returns a non-zero exit status
 *
 * The exception message returned by what() will be of the form
 * "commandline: description [exitstatus]"
 */
class CalledProcessError: public std::runtime_error
{
	int exit_status;
public:
	/**
	 * @param exitStatus the exit status / exit code
	 * @param cmdline the command line causing the exception
	 */
	explicit CalledProcessError(int exitStatus, const std::string& cmdline);

	/**
	 * Get the exit status of the executed process
	 *
	 * @return exit status (negative value when killed by a signal)
	 */
	int exitStatus() const { return exit_status; }
};

/**
 * Spawn a child process with a command line
 *
 * @param args command line
 * @return PID of child process
 * @throws std::system_exception if spawning the process failed early (e.g. because a system limit is hit)
 */
pid_t spawn(std::initializer_list<std::string> args);

/**
 * Run a child process and wait for it to complete
 *
 * @param args command line
 * @return exit status (negative if killed by a signal)
 * @throws std::system_exception if spawning the process failed early (e.g. because a system limit is hit)
 */
int call(std::initializer_list<std::string> args);

/**
 * Run a child process, wait for it to complete and check if the exit status was zero
 *
 * @param args command line
 * @throws std::system_exception if spawning the process failed early (e.g. because a system limit is hit)
 * @throws CalledProcessError if the process exited with a non-zero exit status or was killed by a signal
 */
void check_call(std::initializer_list<std::string> args);

#endif