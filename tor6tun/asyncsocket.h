/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************/

#ifndef _ASYNCSOCKET_H
#define _ASYNCSOCKET_H

#include "socket.h"

#include <sys/types.h>
#include <vector>
#include <string>
#include <sys/socket.h>
#include <stdexcept>

class AsyncSocket {
	Socket sock;
	std::vector<uint8_t> sendbuf;
	std::vector<uint8_t> recvbuf;
	bool send(const void* buffer, size_t len);
public:
	AsyncSocket() noexcept;
	AsyncSocket(Socket&&) noexcept;
	AsyncSocket(const AsyncSocket&) = delete;
	AsyncSocket(AsyncSocket&&) noexcept;
	virtual ~AsyncSocket() noexcept;
	AsyncSocket& operator = (AsyncSocket&&) noexcept;
	AsyncSocket& operator = (Socket&&) noexcept;
	bool send(const PacketBufferView& buffer);
	bool send(const ConstPacketBufferView& buffer);
	bool send(const std::string& buffer);
	bool recv(std::vector<uint8_t> &buffer, size_t size = 0);
	bool flush();
	void shutdown(int how);
	void close() noexcept;
	[[gnu::pure]]
	unsigned int domain() const;
	[[gnu::pure]]
	unsigned int type() const;
	[[gnu::pure]]
	unsigned int protocol() const;
	size_t pending_writes() noexcept {
		return sendbuf.size();
	}
	[[gnu::pure]]
	int fileno() const {
		return sock.fileno();
	}
	void set_blocking(bool value) {
		sock.set_blocking(value);
	}
};

#endif
