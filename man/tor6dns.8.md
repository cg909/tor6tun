tor6dns(8) -- Small DNS server for .onion domains
=================================================

## SYNOPSIS

`tor6dns` [`-v`|`-q`]*
 [ `-I` <NAME> ]
 [ `-o` <SUBNET> ]
 [ `-P` <PORT> ]
 [ `-u` <USER> ]

## DESCRIPTION

`tor6dns` is a minimal DNS server implementation for resolving .onion domains
to IPv6 addresses.

It is designed for use in tandem with tor6tun(8).

## OPTIONS

* `-t`, `--tun`=<NAME>:
	Bind socket to the interface NAME.
	If omitted, the server will be reachable from any interface.

* `-o`, `--onion-network`=<SUBNET>:
	Use SUBNET for .onion domain mapping.
	It must be a valid IPv6 /48 network.
	(Default: *fd87:7026:eb43::/48*)

* `-b`, `--bind`=<ADDR>[:<PORT>]:
	Serve DNS requests on ADDR:PORT.
	The port must be available to bind to.
	It defaults to port 53.
	This option is only optional when systemd style
	socket passing is used.

* `-u`, `--user`=<USER>:
	Drop privileges to USER after initialization. The process will get the UID,
	GID and default group list of the specified user.
	If the effective UID differs from the real UID, USER defaults to the real
	UID; else no privileges will be dropped by default.

* `-v`:
	Increase verbosity. This option can be specified multiple times.

* `-q`:
	Decrease verbosity. This option can be specified multiple times.

## EXIT STATUS

The exit status is 0 if the daemon is terminated normally via
`SIGINT`, `SIGQUIT` or `SIGTERM`.
If the value of an option is invalid the exit status is 2.
If an error occured the exit status is 3.
If unknown options or superfluous parameters are specified the exit status is 64.

## BUGS AND LIMITATIONS

Currently no TCP support.

The server is non-recursive. Only .onion domains and their reverses are resolved.
If you need a recursive DNS server, use dnsmasq(8) or a similar service and configure
it to forward requests for .onion domains to `tor6dns`
(e.g. dnsmasq --server=/onion/::1#5300).

## AUTHOR
Written by Christoph Grenz [christophg at grenz-bonn.de].

## COPYRIGHT
Copyright 2016 Christoph Grenz.

License: GNU GPL version 3 or later
<<http://gnu.org/licenses/gpl.html>>.

This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

## SEE ALSO

tor6tun(8)

tor(1)
