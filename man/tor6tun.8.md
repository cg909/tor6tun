tor6tun(8) -- IPv6 layer 3 proxy for TOR
========================================

## SYNOPSIS

`tor6tun` [`-v`|`-q`]*
 [ `-m` <NUMBER> ]
 [ `-M` <NUMBER> ]
 [ `-o` <SUBNET> ]
 [ `-p` <HOST> ]
 [ `-P` <PORT> ]
 [ `-t` <NAME> ]
 [ `-u` <USER> ]

## DESCRIPTION

`tor6tun` is a IPv6 SOCKS proxy for tor(1) that operates on layer 3.

It creates a virtual network interface, which forwards IPv6 TCP streams to a TOR proxy.

Onion domains are mapped to a special site-scope subnet, so in tandem with
libnss_onion(3) or tor6dns(8) it allows to access .onion URLs from any application.


## OPTIONS

* `-m`, `--max-requests`=<NUMBER>:
	Maximum number of concurrent TCP connection requests.
	If this limit is reached SYN packets are dropped until
	at least one pending connection is established or aborted.
	(Default: 65535)

* `-M`, `--max-connections`=<NUMBER>:
	Maximum number of concurrent TCP connections.
	If this limit is reached SYN packets are dropped until
	at least one established connection is terminated.
	(Default: 65535)

* `-o`, `--onion-network`=<SUBNET>:
	Use SUBNET for .onion domain mapping.
	It must be a valid IPv6 /48 network.
	(Default: *fd87:7026:eb43::/48*)

* `-p`, `--proxy`=<HOST>:
	Use the TOR SOCKS proxy addressed by HOST.
	(Default: *localhost:9050*)

* `-P`, `--port`=<PORT>:
	Use the TCP port PORT for internal purposes.
	The port must be available to bind to.
	Setting this option is only useful for debugging purposes.
	(Default: random)

* `-t`, `--tun`=<NAME>:
	Use NAME as interface name for the TUN device.
	If it contains a question mark, NAME is interpreted as an interface
	name template and the question mark will automatically be replaced by
	an index number.
	(Default: *onion?*)

* `-u`, `--user`=<USER>:
	Drop privileges to USER after initialization. The process will get the UID,
	GID and default group list of the specified user.
	If the effective UID differs from the real UID, USER defaults to the real UID;
	else no privileges will be dropped by default.

* `-v`:
	Increase verbosity. This option can be specified multiple times.

* `-q`:
	Decrease verbosity. This option can be specified multiple times.

## EXIT STATUS

The exit status is 0 if the daemon is terminated normally via
`SIGINT`, `SIGQUIT` or `SIGTERM`.
If the value of an option is invalid the exit status is 2.
If an error occured the exit status is 3.
If unknown options or superfluous parameters are specified the exit status is 64.

## BUGS AND LIMITATIONS

Only TCP connections can be forwarded, all other protocols cause ICMPv6 error responses.

connect(2) calls to the SOCKS5 proxy are performed in blocking mode.
Thus all connections may hang when TCP handshakes are performed.

Source addresses are mapped into a /12 transfer network.
If two clients whose IPv6 addresses only differ in the first 12 bits connect
using the same source port to the same destination
the second connection is silenty dropped.

Currently no stream isolation via SOCKS AUTH is performed.

`tor6tun` provides no way to resolve domain names through TOR.

## AUTHOR
Written by Christoph Grenz [christophg at grenz-bonn.de].

## COPYRIGHT
Copyright 2016 Christoph Grenz.

License: GNU GPL version 3 or later
<<http://gnu.org/licenses/gpl.html>>.

This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

## SEE ALSO

libnss_onion(3), tor6dns(8)

tor(1), torsocks(1)
