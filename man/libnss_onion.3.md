libnss_onion(3) -- Name Service Switch module for .onion domains
================================================================

## SYNOPSIS
hosts: &#x09; `onion` [ *[NOTFOUND=return]* ] ...

## DESCRIPTION

`libnss_onion` is a Name Service Switch module that resolves .onion domains
to IPv6 addresses to allow routing through tor6tun(8).

Onion SLDs are opaque, 16-character alpha-numeric hashes that represent an 80 bit number in Base32.
This module resolves valid .onion domains into IPv6 addresses in the subnet *fd87:7026:eb43::/48*.

Invalid domains in the .onion TLD cause a `NOTFOUND` result. All other name lookups result in `UNAVAIL`.

Inverse lookups are also supported for IPv6 addresses in the given subnet.

## USAGE

Insert *onion* into the `hosts` database source list in */etc/nsswitch.conf*.

## PROVIDED API

The module provides the hosts interface in version 2 of the NSS interface specification:

`_nss_onion_gethostbyname_r`(...)  
`_nss_onion_gethostbyname2_r`(...)  
`_nss_onion_gethostbyname3_r`(...)  
`_nss_onion_gethostbyname4_r`(...)  

Used by gethostbyname(), gethostbyname2(), gethostbyname_r(), gethostbyname2_r(), getaddrinfo(), etc.

`_nss_onion_gethostbyaddr_r`(...)

Used by gethostbyaddr(), gethostbyaddr_r(), getnameinfo(), etc.

## ENVIRONMENT VARIABLES

None supported.

## AUTHOR

Written by Christoph Grenz [christophg at grenz-bonn.de].

## COPYRIGHT

Copyright 2016 Christoph Grenz.

License: GNU LGPL version 3 or later <<http://gnu.org/licenses/lgpl.html>>.

This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

## SEE ALSO

tor6tun(8)

nss(5),
nsswitch.conf(5),
gethostbyname(3),
getaddrinfo(3),
getnameinfo(3)
