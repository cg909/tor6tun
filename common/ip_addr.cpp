#include "ip_addr.h"
#include "util.h"

#include <cstring>
#include <arpa/inet.h>

IPv6Address::IPv6Address(const std::string &string)
{
	*this = string;
}

IPv6Address::IPv6Address(const IPv6Network &network, const in_addr &addr)
{
	if (network.prefixlen > 96)
		throw std::invalid_argument("network too small for IPv4 mapping");
	std::memcpy(s6_addr, network.s6_addr, 12);
	std::memcpy(&s6_addr[12], &addr.s_addr, 4);
}

IPv6Address &IPv6Address::operator =(const std::string &string)
{
	int result = inet_pton(AF_INET6, string.c_str(), this);
	if (result == 0) {
		throw std::invalid_argument("not a valid IPv6 address");
	} else if (result == -1) {
		throw_errno("inet_pton() failed");
	}
	return *this;
}

IPv6Address::operator bool() const
{
	for (int i = 0; i < 16; ++i)
		if (s6_addr[i])
			return true;
	return false;
}

bool IPv6Address::operator ==(const IPv6Address &other) const
{
	return std::memcmp(this, &other, sizeof(IPv6Address)) == 0;
}

bool IPv6Address::operator < (const IPv6Address &other) const
{
	return std::memcmp(this, &other, sizeof(IPv6Address)) < 0;
}

bool IPv6Address::operator <= (const IPv6Address &other) const
{
	return std::memcmp(this, &other, sizeof(IPv6Address)) <= 0;
}

std::string IPv6Address::to_string() const
{
	const char *result;
	char buffer[INET6_ADDRSTRLEN];
	buffer[INET6_ADDRSTRLEN-1] = 0;
	result = inet_ntop(AF_INET6, this, buffer, sizeof(buffer));
	if (!result) {
		throw_errno("inet_ntop() failed");
	}
	return buffer;
}

void IPv6Address::renumber(const IPv6Network &network)
{
	uint8_t *addrbyte = &s6_addr[0];
	const uint8_t *netbyte = &network.s6_addr[0];
	uint8_t mask;
	int i = network.prefixlen;
	if (i > 128) i = 128;
	for (; i >= 0; i -= 8) {
		if (i >= 8) {
			*addrbyte = *netbyte;
		} else {
			mask = 0xFF << (8-i);
			*addrbyte = (*netbyte&mask)|(*addrbyte&~mask);
		}
		addrbyte++; netbyte++;
	}
}

std::ostream& operator<< (std::ostream &stream, const IPv6Address &addr)
{
	return stream << addr.to_string();
}

IPv6Network::IPv6Network(const std::string &string)
{
	*this = string;
}

IPv6Network &IPv6Network::operator =(const std::string &string)
{
	auto i = string.find('/');
	if (i == std::string::npos) {
		prefixlen = 128;
	} else {
		char * err_ptr = nullptr;
		auto tmp = std::strtoul(&string.c_str()[i+1], &err_ptr, 10);
		if ((err_ptr != &*string.end()) or (tmp > 128))
			throw std::invalid_argument("not a valid prefix length");
		prefixlen = tmp;
	}
	std::string tmp = string.substr(0, i);
	int result = inet_pton(AF_INET6, tmp.c_str(), this);
	if (result == 0) {
		throw std::invalid_argument("not a valid IPv6 address");
	} else if (result == -1) {
		throw_errno("inet_pton() failed");
	}
	return *this;
}

std::string IPv6Network::to_string() const
{
	const char *result;
	char buffer[INET6_ADDRSTRLEN+4];
	zero_init(buffer);
	result = inet_ntop(AF_INET6, this, buffer, sizeof(buffer));
	if (!result) {
		throw_errno("inet_ntop() failed");
	}
	size_t i = std::strlen(buffer);
	snprintf(&buffer[i], 5, "/%hhu", prefixlen);
	return result;
}

std::ostream& operator<< (std::ostream &stream, const IPv6Network &net)
{
	return stream << net.to_string();
}

bool IPv6Address::is_in (const IPv6Network &net) const
{
	const uint8_t *addrbyte = &s6_addr[0];
	const uint8_t *netbyte = &net.s6_addr[0];
	uint8_t mask;
	int i = net.prefixlen;
	if (i > 128) i = 128;
	for (; i >= 0; i -= 8) {
		if (i >= 8) {
			if (*addrbyte != *netbyte)
				return false;
		} else {
			mask = 0xFF << (8-i);
			if ((*addrbyte & mask) != (*netbyte & mask))
				return false;
		}
		addrbyte++; netbyte++;
	}
	return true;
}

IPv4Address::IPv4Address(const std::string &string)
{
	*this = string;
}

IPv4Address::IPv4Address(const struct in6_addr &addr)
{
	std::memcpy(&s_addr, &addr.s6_addr[12], 4);
}

IPv4Address &IPv4Address::operator =(const std::string &string)
{
	int result = inet_pton(AF_INET, string.c_str(), this);
	if (result == 0) {
		throw std::invalid_argument("not a valid IPv4 address");
	} else if (result == -1) {
		throw_errno("inet_pton() failed");
	}
	return *this;
}

IPv4Address::operator bool() const
{
	return (s_addr != INADDR_ANY);
}

bool IPv4Address::operator == (const IPv4Address &other) const
{
	return s_addr == other.s_addr;
}

bool IPv4Address::operator < (const IPv4Address &other) const
{
	return s_addr < other.s_addr;
}

bool IPv4Address::operator <= (const IPv4Address &other) const
{
	return s_addr <= other.s_addr;
}

std::string IPv4Address::to_string() const
{
	const char *result;
	char buffer[INET_ADDRSTRLEN];
	buffer[INET_ADDRSTRLEN-1] = 0;
	result = inet_ntop(AF_INET, this, buffer, sizeof(buffer));
	if (!result) {
		throw_errno("inet_ntop() failed");
	}
	return buffer;
}

std::ostream& operator<< (std::ostream &stream, const IPv4Address &addr)
{
	return stream << addr.to_string();
}
