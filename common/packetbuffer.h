/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************/

/**
 * @addtogroup common Common helper functions and classes
 * @{
 * @file
 * Packet buffers
 */

#ifndef _PACKETBUFFER_H
#define _PACKETBUFFER_H

#include <stdexcept>
#include <cstring>
#include <string>
#include <vector>
#include <arpa/inet.h>

/**
 * Mutable packet buffer view
 *
 * Allowes to sequentially overlay a buffer with structs to efficiently
 * parse a received network packet and to build a response.
 *
 * Exceeding the buffer length results in std::out_of_range exceptions.
 *
 * @attention You must be very careful to only use aliasable standard-layout
 * structs to not produce undefined behaviour.
 */
class PacketBufferView final {
	size_t view_size; /**< remaining view size in bytes */
	uint8_t *ptr; /**< pointer to the current position of the view */

public:
	/**
	 * Constructs a packet buffer view from a vector
	 *
	 * @attention
	 * reallocation of the vector storage will cause undefined behaviour
	 * so refrain from appending data to the vector while a view is in scope.
	 *
	 * @param vector std::vector whose storage the view will use
	 */
	template<typename T>
	explicit constexpr PacketBufferView(std::vector<T> &vector);

	/**
	 * Constructs a packet buffer view from a vector with a custom offset and size
	 *
	 * The resulting view is capped at the vector size
	 *
	 * @attention
	 * reallocation of the vector storage will cause undefined behaviour
	 * so refrain from appending data to the vector while a view is in scope.
	 *
	 * @param vector std::vector whose storage the view will use
	 * @param offset offset into the vector storage
	 * @param size size of the view relative to the offset
	 */
	template<typename T>
	constexpr PacketBufferView(std::vector<T> &vector, size_t, size_t=-1UL);

	/**
	 * Get the remaining size of the view
	 *
	 * @return remaining size in bytes
	 */
	constexpr size_t size() const noexcept
	{
		return view_size;
	}

	/**
	 * Skips bytes in the view
	 *
	 * @param count how many bytes should be skipped
	 * @throws std::out_of_range if the byte count exceeds the view size
	 */
	void skip_bytes(size_t count);

	/**
	 * Overlays a struct at the beginning of the current view position and
	 * move the view position after it
	 *
	 * @tparam T struct to overlay
	 * @return the overlaid storage memory casted to T&
	 * @throws std::out_of_range if the byte count exceeds the view size
	 */
	template<typename T>
	T& next();

	/**
	 * Overlays a struct at the beginning of the current view position
	 *
	 * @tparam T struct to overlay
	 * @return the overlaid storage memory casted to const T&
	 * @throws std::out_of_range if the byte count exceeds the view size
	 */
	template<typename T>
	const T& peek() const;

	/**
	 * Overlays a struct at the beginning of the current view position
	 *
	 * @tparam T struct to overlay
	 * @return the overlaid storage memory casted to T&
	 * @throws std::out_of_range if the struct size exceeds the view size
	 */
	template<typename T>
	T& peek();

	/**
	 * Caps the view size
	 *
	 * @note The view size can only be reduced by this method and not be enlarged.
	 *
	 * @param size new view size in bytes
	 */
	void cap(size_t size);

	/**
	 * Construct a child view with capped size
	 *
	 * @param size new view size in bytes
	 * @return a view to the same storage with capped size
	 */
	PacketBufferView capped(size_t size) const;

	/**
	 * Copies the memory representation of a structure into the storage at the
	 * current view position and move the view position after it
	 *
	 * If the data size exceeds the view size, the data is capped.
	 *
	 * @param data structure/object to read from
	 * @return written byte count
	 */
	template<typename T>
	size_t fill_from(const T &data);

	/**
	 * Gets a pointer to the current view position
	 *
	 * @return void pointer
	 */
	void *pointer() { return ptr; }

	/**
	 * @details @overload
	 */
	const void *pointer() const { return ptr; }

	/**
	 * Gets a pointer to the current view position
	 *
	 * @return uint8_t pointer
	 */
	uint8_t *begin() { return ptr; }

	/**
	 * @details @overload
	 */
	const uint8_t *begin() const { return ptr; }

	/**
	 * Gets a pointer to the end of the view
	 *
	 * @return uint8_t pointer
	 */
	uint8_t *end() { return static_cast<uint8_t*>(ptr)+view_size; }

	/**
	 *  @details @overload
	 */
	const uint8_t *end() const { return static_cast<uint8_t*>(ptr)+view_size; }
};

/**
 * Immutable packet buffer view
 *
 * Allowes to sequentially overlay a buffer with structs to efficiently
 * parse a received network packet.
 *
 * Exceeding the buffer length results in std::out_of_range exceptions.
 *
 * @attention You must be very careful to only use aliasable standard-layout
 * structs to not produce undefined behaviour.
 */
class ConstPacketBufferView final {
	size_t view_size; /**< remaining view size in bytes */
	const uint8_t *ptr; /**< pointer to the current position of the view */

public:
	/**
	 * Constructs an immutable packet buffer view from a mutable packet buffer view
	 */
	template<typename T>
	constexpr ConstPacketBufferView(const PacketBufferView &view)
		: view_size(view.size()), ptr(static_cast<const uint8_t*>(view.pointer()))
	{}

	/** @copydoc PacketBufferView::PacketBufferView(std::vector &) */
	template<typename T>
	constexpr ConstPacketBufferView(const std::vector<T> &vector);

	/** @copydoc PacketBufferView::PacketBufferView(std::vector &,size_t,size_t) */
	template<typename T>
	constexpr ConstPacketBufferView(const std::vector<T> &vector, size_t, size_t=-1);

	/** @copydoc PacketBufferView::size() */
	size_t size() const
	{
		return view_size;
	}

	/** @copydoc PacketBufferView::skip_bytes() */
	void skip_bytes(size_t count);

	/**
	 * Overlays a struct at the beginning of the current view position and
	 * move the view position after it
	 *
	 * @tparam T struct to overlay
	 * @return the overlaid storage memory casted to const T&
	 * @throws std::out_of_range if the byte count exceeds the view size
	 */
	template<typename T>
	const T& next();

	/** @copydoc PacketBufferView::peek() const */
	template<typename T>
	const T& peek() const;

	/** @copydoc PacketBufferView::capped() */
	ConstPacketBufferView capped(size_t) const;

	/** @copydoc PacketBufferView::pointer() */
	const void *pointer() const { return ptr; }
	/** @copydoc PacketBufferView::begin() */
	const uint8_t *begin() const { return ptr; }
	/** @copydoc PacketBufferView::end() */
	const uint8_t *end() const { return static_cast<const uint8_t*>(ptr)+view_size; }

	friend PacketBufferView;
};

template<typename T>
constexpr PacketBufferView::PacketBufferView(std::vector<T> &vector)
	: view_size(vector.size()*sizeof(T)),
	  ptr(reinterpret_cast<uint8_t*>(&vector[0]))
{
}

template<typename T>
constexpr PacketBufferView::PacketBufferView(std::vector<T> &vector, size_t offset, size_t size)
	: view_size(std::min(vector.size()*sizeof(T)-offset, size)),
	  ptr(reinterpret_cast<uint8_t*>(&vector[0])+offset)
{
}

template<typename T>
T& PacketBufferView::next()
{
	uint8_t *curptr = ptr;
	if (sizeof(T) > view_size) {
		throw std::out_of_range("struct size exceeds buffer length");
	}
	view_size -= sizeof(T);
	ptr += sizeof(T);
	return *reinterpret_cast<T*>(curptr);
}

template<typename T>
const T& PacketBufferView::peek() const
{
	if (sizeof(T) > view_size) {
		throw std::out_of_range("struct size exceeds buffer length");
	}
	return *reinterpret_cast<T*>(ptr);
}

template<typename T>
T& PacketBufferView::peek()
{
	if (sizeof(T) > view_size) {
		throw std::out_of_range("struct size exceeds buffer length");
	}
	return *reinterpret_cast<T*>(ptr);
}

inline void PacketBufferView::cap(size_t size)
{
	view_size = std::max(view_size, size);
}

inline PacketBufferView PacketBufferView::capped(size_t size) const
{
	PacketBufferView result(*this);
	result.view_size = std::max(view_size, size);
	return result;
}

template<typename T>
size_t PacketBufferView::fill_from(const T &data)
{
	size_t size = std::min(data.size(), view_size);
	std::memcpy(ptr, &data[0], size);
	view_size -= size;
	ptr += size;
	return size;
}

/**
 * @details @overload
 *
 * @param data packet buffer view to read from
 * @return written byte count
 */
template<>
inline size_t PacketBufferView::fill_from(const PacketBufferView &data)
{
	size_t size = std::min(data.size(), view_size);
	std::memmove(ptr, data.ptr, size);
	view_size -= size;
	ptr += size;
	return size;
}

/**
 * @details @overload
 *
 * @param data packet buffer view to read from
 * @return written byte count
 */
template<>
inline size_t PacketBufferView::fill_from(const ConstPacketBufferView &data)
{
	size_t size = std::min(data.size(), view_size);
	std::memmove(ptr, data.ptr, size);
	view_size -= size;
	ptr += size;
	return size;
}

inline void PacketBufferView::skip_bytes(size_t count)
{
	if (count > view_size) {
		throw std::out_of_range("byte count exceeds buffer length");
	}
	view_size -= count;
	ptr += count;
}

template<typename T>
constexpr ConstPacketBufferView::ConstPacketBufferView(const std::vector<T> &vector)
	: view_size(vector.size()*sizeof(T)),
	  ptr(reinterpret_cast<const uint8_t*>(&vector[0]))
{
}

template<typename T>
constexpr ConstPacketBufferView::ConstPacketBufferView(const std::vector<T> &vector, size_t offset, size_t size)
	: view_size(std::min(vector.size()*sizeof(T)-offset, size)),
	  ptr(reinterpret_cast<const uint8_t*>(&vector[0])+offset)
{
}

template<typename T>
const T& ConstPacketBufferView::next()
{
	const uint8_t *curptr = ptr;
	if (sizeof(T) > view_size) {
		throw std::out_of_range("struct size exceeds buffer length");
	}
	view_size -= sizeof(T);
	ptr += sizeof(T);
	return *reinterpret_cast<const T*>(curptr);
}

template<typename T>
const T& ConstPacketBufferView::peek() const
{
	if (sizeof(T) > view_size) {
		throw std::out_of_range("struct size exceeds buffer length");
	}
	return *reinterpret_cast<const T*>(ptr);
}

inline ConstPacketBufferView ConstPacketBufferView::capped(size_t size) const
{
	ConstPacketBufferView result(*this);
	result.view_size = std::max(view_size, size);
	return result;
}

inline void ConstPacketBufferView::skip_bytes(size_t count)
{
	if (count > view_size) {
		throw std::out_of_range("byte count exceeds buffer length");
	}
	view_size -= count;
	ptr += count;
}

#endif
/**@}*/
