/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************/

/**
 * @addtogroup common Common helper functions and classes
 * @{
 * @file
 * Signal handling
 */

#ifndef _SIGNALS_H
#define _SIGNALS_H

#include "util.h"

#include <initializer_list>
#include <csignal>

/**
 * Signal set class
 *
 * @see sigaction(std::initializer_list<int>, void (*)(int), const signalset &, int)
 * @see sigaction(std::initializer_list<int>, void (*)(int, siginfo_t *, void *), const signalset &, int)
 */
struct signalset final {
	sigset_t sigs; /**< POSIX signal set that is manipulated by the methods of this instance */

	/**
	 * Constructs an empty signal set
	 */
	signalset() noexcept
	{
		sigemptyset(&sigs);
	}

	/**
	 * Constructs an signal set from a sigset_t
	 * @param sigset POSIX signal set
	 */
	signalset(const sigset_t &sigset) noexcept;

	/**
	 * Constructs an signal set from a single signal
	 * @param signum signal number
	 */
	explicit signalset(int signum) noexcept
	{
		sigemptyset(&sigs);
		sigaddset(&sigs, signum);
	}

	signalset(float) = delete;
	signalset(double) = delete;

	/**
	 * Constructs an signal set from a list of signals
	 * @param signums signal numbers
	 */
	signalset(std::initializer_list<int> signums) noexcept
	{
		sigemptyset(&sigs);
		for (int signum: signums) {
			sigaddset(&sigs, signum);
		}
	}

	/**
	 * Constructs a full signal set
	 * @return signalset instance containing all signals
	 */
	static signalset full() noexcept
	{
		signalset result = signalset();
		sigfillset(&result.sigs);
		return result;
	}

	/**
	 * Adds a signal to the signal set
	 *
	 * If the signal is already in the set,
	 * no change will occur.
	 *
	 * @param signum signal number
	 * @return itself, for chaining
	 */
	signalset &add(int signum) noexcept
	{
		sigaddset(&sigs, signum);
		return *this;
	}

	/**
	 * Removes a signal from the signal set
	 *
	 * If the signal is not in the set,
	 * no change will occur.
	 *
	 * @param signum signal number
	 * @return itself, for chaining
	 */
	signalset &remove(int signum) noexcept
	{
		sigdelset(&sigs, signum);
		return *this;
	}

	/**
	 * Adds multiple signals to the signal set
	 *
	 * If a signal is already in the set, it is skipped.
	 *
	 * @param signums signal numbers
	 * @return itself, for chaining
	 */
	signalset &add(std::initializer_list<int> signums) noexcept
	{
		for (int signum: signums)
			sigaddset(&sigs, signum);
		return *this;
	}

	/**
	 * Removes multiple signals from the signal set
	 *
	 * If a signal is not in the set, it is skipped.
	 *
	 * @param signums signal numbers
	 * @return itself, for chaining
	 */
	signalset &remove(std::initializer_list<int> signums) noexcept
	{
		for (int signum: signums)
			sigdelset(&sigs, signum);
		return *this;
	}

	/**
	 * Checks if a signal is in the signal set
	 *
	 * @param signum signal number
	 * @return true, if the signal is in the set
	 */
	bool contains(int signum) const noexcept
	{
		return sigismember(&sigs, signum);
	}
};

/**
 * Change a signal action
 *
 * @param signums signal numbers
 * @param action signal action
 */
void sigaction(std::initializer_list<int> signums, const struct sigaction &action);

/**
 * @details @overload
 * @param signums signal numbers
 * @param handler sa_handler-style callback or SIG_IGN or SIG_DFL
 * @param sig_mask signals to block while the handler is executed
 * @param flags set  of flags
 */
void sigaction(std::initializer_list<int> signums, void (*handler)(int), const signalset &sig_mask = {}, int flags = 0);

/**
 * @details @overload
 * @param signums signal numbers
 * @param handler sa_sigaction-style callback
 * @param sig_mask signals to block while the handler is executed
 * @param flags set of flags
 */
void sigaction(std::initializer_list<int> signums, void (*handler)(int, siginfo_t *, void *), const signalset &sig_mask = {}, int flags = 0);

/**
 * Examine a signal action
 *
 * @overload
 * @param signum signal number
 * @return sigaction describing the action taken on signal receipt
 */
struct sigaction sigaction(int signum);

#endif
/**@}*/
