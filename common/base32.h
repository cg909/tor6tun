/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************/

/**
 * @addtogroup common Common helper functions and classes
 * @{
 * @file
 * Tor-style Base32 implementation
 */

#ifndef _BASE32_H
#define _BASE32_H

#include <string>
#include <vector>

/**
 * Encodes a block of data with Tor-style base32
 *
 * @param ptr pointer to source data
 * @param size data size in bytes
 * @return base32 string
 */
[[gnu::nonnull(1)]]
std::string b32encode (const void *ptr, size_t size);

/**
 * @details @overload
 *
 * @param str data string
 * @return base32 string
 */
inline std::string b32encode(const std::string &str)
{
	return b32encode(str.data(), str.length());
}

/**
 * @details @overload
 *
 * @param obj object with fixed size
 * @return base32 string
 */
template<typename T>
std::string b32encode(const T &obj)
{
	return b32encode(static_cast<const void*>(&obj), sizeof(T));
}

/**
 * Decodes a Tor-style base32 string
 *
 * @param input input string
 * @param[out] dest destination buffer
 * @param destsize destination buffer size
 * @return count of written bytes
 */
size_t b32decode(const std::string &input, void *dest, size_t destsize);

/**
 * @details @overload
 */
template<typename T>
size_t b32decode(const std::basic_string<char, T> &input, void *dest, size_t destsize)
{
	return b32decode(reinterpret_cast<const std::string &>(input), dest, destsize);
}

#endif
/**@}*/
