/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************
 * Logging infrastructure.                                                  *
 ****************************************************************************/

#include "logging.h"
#include "unistd.h"
#ifdef SYSTEMD
# include <systemd/sd-daemon.h>
#endif

extern char **environ;

namespace log {

	enum loglevels loglevel = INFO;

}

const char *log::_debug = "debug: ";
const char *log::_info = "info: ";
const char *log::_warning = "warning: ";
const char *log::_err = "error: ";
const char *log::_crit = "fatal error: ";

#ifdef SYSTEMD
[[gnu::constructor]]
static void init()
{
	bool use_systemd_style = false;
	for (const char * const * it = environ; *it != nullptr; ++it) {
		const char * item = *it;
		if (strlen(item) <= 14)
			continue;
		if (strncmp("NOTIFY_SOCKET=", item, 14) == 0) {
			use_systemd_style = true;
			break;
		}
	}
	if (use_systemd_style) {
		log::_debug = SD_DEBUG;
		log::_info = SD_INFO;
		log::_warning = SD_WARNING;
		log::_err = SD_ERR;
		log::_crit = SD_CRIT;
	}
}
#endif
