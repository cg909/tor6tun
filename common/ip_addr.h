/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************/

/**
 * @addtogroup common Common helper functions and classes
 * @{
 * @file
 * IP address classes
 */

#ifndef _IP_ADDR_H
#define _IP_ADDR_H

#include <stdexcept>
#include <string>
#include <vector>
#include <arpa/inet.h>

/**
 * IPv6 network (or interface) consisting of an IPv6 address and a prefix length (CIDR style)
 */
struct IPv6Network: public in6_addr {
	unsigned char prefixlen; /**< network prefix length in bits */

	/** Constructs the IPv6 network ::/0 */
	constexpr IPv6Network()
		: in6_addr(IN6ADDR_ANY_INIT), prefixlen(0)
	{}
	/**
	 * Constructs an IPv6 network from a string in the form "address/length"
	 * @throws std::invalid_argument if the string cannot be parsed
	 */
	explicit IPv6Network(const std::string &);

	/**
	 * Constructs an IPv6 network from an address and a prefix length
	 * @param addr in6_addr or IPv6Address
	 * @param prefixlen network prefix length
	 */
	constexpr IPv6Network(const struct in6_addr &addr, unsigned char prefixlen)
		: in6_addr(addr), prefixlen(prefixlen)
	{}

	/**
	 * Replaces the network from a string representation
	 * @param string string in the form "address/length"
	 * @throws std::invalid_argument if the string cannot be parsed
	 */
	IPv6Network &operator =(const std::string &);

	/**
	 * Converts to a string representation
	 * @returns a string in the form "address/length"
	 */
	std::string to_string() const;
};

/**
 * IPv6 address
 */
struct IPv6Address: public in6_addr {
	/**
	 * Constructs the unspecified address (::)
	 */
	constexpr IPv6Address()
		: in6_addr(IN6ADDR_ANY_INIT)
	{}

	/**
	 * Copy constructor
	 * @param rhs in6_addr struct or other IPv6Address instance
	 */
	constexpr IPv6Address(const struct in6_addr &rhs)
		: in6_addr(rhs)
	{}

	/**
	 * Constructs an IPv6 address from a string representation
	 */
	explicit IPv6Address(const std::string &);

	/**
	 * Constructs an IPv4-mapped IPv6 address
	 *
	 * @param network IPv6 network with a prefix of at most 96 bits
	 * @param addr IPv4 address
	 * @throws std::invalid_argument if the network is too small to map IPv4 addresses
	 */
	explicit IPv6Address(const IPv6Network &, const in_addr &);

	/**
	 * Replaces the address from a string representation
	 * @param string string representation of an address
	 * @throws std::invalid_argument if the string cannot be parsed
	 */
	IPv6Address &operator =(const std::string &);

	/**
	 * Compares this address to another
	 * @return true if both addresses are equal
	 */
	[[gnu::pure]]
	bool operator ==(const IPv6Address&) const;

	/**
	 * Check if the address is non-zero
	 * @return true if the address is not all zeroes
	 */
	[[gnu::pure]]
	explicit operator bool() const;

	/**
	 * Constructs the loopback IPv6 address (::1)
	 */
	static constexpr IPv6Address loopback();

	/**
	 * Checks if the address is contained in a network
	 */
	[[gnu::pure]]
	bool is_in (const IPv6Network&) const;

	/**
	 * Converts to a string representation
	 * @returns string representing this address
	 */
	std::string to_string() const;

	/**
	 * Renumbers this address to another network.
	 *
	 * This replaces all network bits of the address with these of the new
	 * network.
	 *
	 * @param network IPv6 network to renumber to
	 */
	void renumber(const IPv6Network &);

	/**
	 * Checks if this instance is the loopback address
	 */
	inline bool is_loopback() const;

	/**
	 * Checks if this instance is a multicast address
	 */
	inline bool is_multicast() const;

	bool operator < (const IPv6Address&) const;
	bool operator <= (const IPv6Address&) const;
	bool operator > (const IPv6Address &other) const {
		return !operator <=(other);
	}
	bool operator >= (const IPv6Address &other) const {
		return !operator <(other);
	}
};

/**
 * IPv4 address
 */
struct IPv4Address: public in_addr {
	/**
	 * Constructs the unspecified address (0.0.0.0)
	 */
	constexpr IPv4Address()
		: in_addr({INADDR_ANY})
	{}

	/**
	 * Copy constructor
	 * @param rhs in_addr struct or other IPv4Address instance
	 */
	constexpr IPv4Address(const struct in_addr &rhs)
		: in_addr(rhs)
	{}

	/**
	 * Constructs an IPv4 address from a string representation
	 */
	explicit IPv4Address(const std::string &);

	/**
	 * Constructs an IPv4 address from a IPv4-mapped IPv6 address
	 *
	 * Takes the last 4 bytes of the IPv6 address and interprets them
	 * as an IPv4 address.
	 */
	explicit IPv4Address(const in6_addr &);

	/**
	 * Constructs an IPv4 address from a IPv4-mapped IPv6 address
	 *
	 * Takes the last 4 bytes of the IPv6 address and interprets them
	 * as an IPv4 address.
	 */
	IPv4Address &operator =(const std::string &);

	/**
	 * Compares this address to another
	 * @return true if both addresses are equal
	 */
	[[gnu::pure]]
	bool operator ==(const IPv4Address&) const;

	/**
	 * Check if the address is non-zero
	 * @return true if the address is not all zeroes
	 */
	[[gnu::pure]]
	explicit operator bool() const;

	/**
	 * Constructs the canonical loopback IPv4 address (127.0.0.1)
	 */
	static constexpr IPv4Address loopback();

	/**
	 * Converts to a string representation
	 * @returns string representing this address
	 */
	std::string to_string() const;

	/**
	 * Checks if this instance is a loopback address
	 */
	inline bool is_loopback() const;

	/**
	 * Checks if this instance is a multicast address
	 */
	inline bool is_multicast() const;

	bool operator < (const IPv4Address&) const;
	bool operator <= (const IPv4Address&) const;
	bool operator > (const IPv4Address &other) const {
		return !operator <=(other);
	}
	bool operator >= (const IPv4Address &other) const {
		return !operator <(other);
	}
};

/**
 * Stream insertion operator
 *
 * Inserts the string representation of the address into the output stream.
 *
 * @related IPv6Address
 */
std::ostream& operator << (std::ostream&, const IPv6Address&);

/**
 * Stream insertion operator
 *
 * Inserts the string representation of the network into the output stream.
 *
 * @related IPv6Network
 */
std::ostream& operator << (std::ostream&, const IPv6Network&);

/**
 * Stream insertion operator
 *
 * Inserts the string representation of the address into the output stream.
 *
 * @related IPv4Address
 */
std::ostream& operator << (std::ostream&, const IPv4Address&);

inline constexpr IPv6Address IPv6Address::loopback()
{
	return in6_addr(IN6ADDR_LOOPBACK_INIT);
}

inline constexpr IPv4Address IPv4Address::loopback()
{
	return in_addr({INADDR_LOOPBACK});
}

inline bool IPv6Address::is_loopback() const
{
	IPv6Address loopback;
	loopback.s6_addr[15] = 1;
	return *this == loopback;
}

inline bool IPv4Address::is_loopback() const
{
	return (s_addr & 0xFF000000) == 0x7f000000;
}

inline bool IPv6Address::is_multicast() const
{
	return this->s6_addr[0] == 0xFF;
}

inline bool IPv4Address::is_multicast() const
{
	return IN_MULTICAST(s_addr);
}

static_assert(sizeof(IPv6Address)==16, "Wrong alignment in IPv6Address struct");
static_assert(sizeof(IPv4Address)==sizeof(struct in_addr), "Wrong alignment in IPv4Address struct");

/**
 * @internal
 * @brief Helper functions for constexpr IPv4 address parsing
 */
namespace _ipaddr_helpers {

	/**
	 * constexpr implementation of std::pow()
	 */
	template<typename T>
	static constexpr T const_pow(T base, uint8_t exponent, T _r=1)
	{
		return (exponent == 0)
			? _r
			: const_pow(base, exponent-1, base * _r);
	}

	/**
	 * convert a digit character to an integer
	 * @throws std::invalid_argument if the character is no ASCII digit
	 */
	static constexpr uint8_t digit2int(char digit)
	{
		return (digit >= '0' and digit <= '9') ? digit & 0xF : throw std::invalid_argument("not a number");
	}

	/**
	 * constexpr implementation of an IPv4 address parser
	 */
	static constexpr uint32_t parseIPv4(const char s[], size_t l, size_t p = 0, uint8_t digit=0, uint8_t byte=0)
	{
		return (p == l) ? 0 :
			(s[l-p-1] == '.')
			? parseIPv4(s, l, p+1, 0, byte+1)
			: (
				(digit2int(s[l-p-1]) * const_pow(10, digit) << (8*byte))
				+ parseIPv4(s, l, p+1, digit+1, byte)
			);
	}

	/// Assert that the parser parses at least the loopback address correctly
	static_assert(parseIPv4("127.0.0.1", 9) == INADDR_LOOPBACK, "IPv4 address parser assertion failed");
}

/**
 * User-defined literal for IPv4 addresses
 *
 * @relatedalso IPv4Address
 */
constexpr IPv4Address operator "" _ip4(const char *string, size_t length)
{
	return in_addr({_ipaddr_helpers::parseIPv4(string, length)});
}

#endif
/**@}*/
