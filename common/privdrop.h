#ifndef _PRIVDROP_H
#define _PRIVDROP_H

#include <string>

/**
 * @addtogroup common Common helper functions and classes
 * @{
 * @file
 * Privilege dropping helper functions
 */

/**
 * Switches to an unprivileged user
 *
 * Drops privileges by resetting the supplementary group list, the real, effective and saved GID
 * and the real, effective and saved UID of the process to that of the given user.
 *
 * Additionally (if supported by the OS) all Linux capabilities that survived setting the
 * UIDs are dropped.
 *
 * If the user is 'root' (uid 0) then after dropping as many privileges as possible, a warning
 * is logged.
 *
 * @param user username (or uid)
 */
[[gnu::cold]]
void drop_privileges(const std::string &user);

/**
 * Legacy daemonization
 *
 * Daemonizes the process the traditional way, detaching from the controlling terminal
 * and forking into the background.
 *
 * @param nochdir if false, changes the calling process's working directory to the root directory
 * @param noclose if false, redirects stdin, stdout and stderr to /dev/null
 */
[[gnu::cold]]
void daemonize(bool nochdir=false, bool noclose=false);

#endif
/**@}*/
