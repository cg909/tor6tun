#ifndef _IPV6_PACKETS_H
#define _IPV6_PACKETS_H

#include "ip_addr.h"

#include <arpa/inet.h>

/**
 * @addtogroup common Common helper functions and classes
 * @{
 * @file
 * IP packet structs
 */

/* IPv6 */

/**
 * IPv6 header structure (RFC 2460)
 *
 * This structure is guaranteed to have a memory layout compatible with
 * a captured IPv6 packet header and thus can be used with
 * e.g. PacketBufferView::next<>().
 */
struct [[gnu::may_alias]] IPv6Header {
	uint32_t	iph_v_tc_flow;
	uint16_t	iph_payload_length;
	uint8_t		iph_next_header;
	uint8_t		iph_hop_limit;
	IPv6Address iph_source;
	IPv6Address iph_destination;

	/**
	 * Get the IP version number
	 *
	 * Must be 6 for a valid IPv6 packet
	 */
	uint8_t version() const {
		return ntohl(iph_v_tc_flow) >> 28;
	}

	/**
	 * Set the IP version number field
	 *
	 * Must be set to 6 to create a valid IPv6 packet
	 *
	 * @param version a 4-bit integer
	 */
	void version(uint8_t version) {
		iph_v_tc_flow = htonl((ntohl(iph_v_tc_flow) & 0x0FFFFFFF) | ((version & 0xF) << 28));
	}

	/**
	 * Get the traffic class
	 *
	 * The Traffic Class field in the can be used to to identify and
	 * distinguish between different classes or priorities of packets.
	 */
	uint8_t traffic_class() const {
		return (ntohl(iph_v_tc_flow) >> 20) & 0xFF;
	}

	/**
	 * Set the traffic class field
	 * @param cls a 8-bit integer
	 */
	void traffic_class(uint8_t cls) {
		iph_v_tc_flow = htonl((ntohl(iph_v_tc_flow) & 0xF00FFFFF) | (cls << 20));
	}

	/**
	 * Get the flow label
	 *
	 * The Flow Label field can be used by the source to label sequences of
	 * packets for which it requests special handling by the IPv6 routers.
	 */
	uint32_t flow_label() const {
		return ntohl(iph_v_tc_flow) & 0xFFFFF;
	}

	/**
	 * Set the flow label field
	 * @param label a 20-bit integer
	 */
	void flow_label(uint32_t label) {
		iph_v_tc_flow = htonl((ntohl(iph_v_tc_flow) & 0xFFF00000) | (label & 0xFFFFF));
	}

	/**
	 * Get the payload length
	 *
	 * The length of the IPv6 payload, i.e., the rest of the packet following
	 * this IPv6 header, in octets, including all extension headers.
	 */
	uint16_t payload_length() const {
		return htons(iph_payload_length);
	}

	/**
	 * Set the payload length field
	 * @param length a 16-bit integer
	 */
	void payload_length(uint16_t length) {
		iph_payload_length = htons(length);
	}

	/**
	 * Get the next header type
	 *
	 * Identifies the type of header immediately following the IPv6 header.
	 * Uses the same values as IPv4Header::protocol() (RFC 1700, etc.)
	 */
	uint8_t next_header() {
		return iph_next_header;
	}

	/**
	 * Set the next header type
	 *
	 * @param code a 8-bit integer
	 */
	void next_header(uint8_t code) {
		iph_next_header = code;
	}

	/**
	 * Get the hop limit
	 *
	 * Decremented by 1 by each node that forwards the packet. The packet
	 * is discarded if the Hop Limit is decremented to zero.
	 */
	uint8_t hop_limit() {
		return iph_hop_limit;
	}

	/**
	 * Set the hop limit field
	 *
	 * @param limit a 8-bit integer
	 */
	void hop_limit(uint8_t limit) {
		iph_hop_limit = limit;
	}

	/**
	 * Get the source address
	 */
	const IPv6Address& source() const {
		return iph_source;
	}

	/**
	 * @details @overload
	 */
	IPv6Address& source() {
		return iph_source;
	}

	/**
	 * Get the destination address
	 */
	const IPv6Address& destination() const {
		return iph_destination;
	}

	/**
	 * @details @overload
	 */
	IPv6Address& destination() {
		return iph_destination;
	}
};

/**
 * ICMPv6 header structure (RFC 4443)
 *
 * This structure is guaranteed to have a memory layout compatible with
 * a captured ICMPv6 packet header and thus can be used with
 * PacketBufferView::next<>().
 */
struct [[gnu::may_alias]] ICMPv6Header {
	uint8_t		ic6_type;
	uint8_t		ic6_code;
	uint16_t	ic6_checksum;

	/**
	 * Get the message type
	 *
	 * The type field indicates the type of the message. Its value determines
	 * the format of the remaining data.
	 */
	uint8_t type() const {
		return ic6_type;
	}

	/**
	 * Set the message type field
	 *
	 * @param type a 8-bit integer
	 */
	void type(uint8_t type) {
		ic6_type = type;
	}

	/**
	 * Get the message code
	 *
	 * The meaning depends on the message type. Allows for an additional level
	 * of message granularity.
	 */
	uint8_t code() const {
		return ic6_code;
	}

	/**
	 * Set the message code field
	 *
	 * @param code a 8-bit integer
	 */
	void code(uint8_t code) {
		ic6_code = code;
	}

	/**
	 * Get the message checksum
	 *
	 * The checksum field is used to detect data corruption in the ICMPv6
	 * message and parts of the IPv6 header.
	 */
	uint16_t checksum() const {
		return ntohs(ic6_checksum);
	}

	/**
	 * Set the message checksum field
	 *
	 * @param checksum a 16 bit integer (in host byte order)
	 */
	void checksum(uint16_t checksum) {
		ic6_checksum = htons(checksum);
	}

	/**
	 * @internal
	 * @brief Calculates the message checksum
	 *
	 * @param header the IPv6 header preceeding this ICMPv6 message
	 * @return calculated checksum
	 */
	uint16_t _calc_checksum(const IPv6Header&);

	/**
	 * @brief Calculates and sets the message checksum
	 *
	 * @attention The message must be laid out in memory so that the payload
	 * directly succeeds the header without any padding. The IPv6 payload
	 * length also needs to be set correctly or else an out-of-bounds read will
	 * happen!
	 *
	 * @param header the IPv6 header preceeding this ICMPv6 message
	 * @return calculated checksum
	 */
	void set_checksum(const IPv6Header&);

	/**
	 * @brief Verifies and sets the message checksum
	 *
	 * @attention The message must be laid out in memory so that the payload
	 * directly succeeds the header without any padding. The IPv6 payload
	 * length also needs to be verified before verifying the checksum or else
	 * an out-of-bounds read will happen!
	 *
	 * @param header the IPv6 header preceeding this ICMPv6 message
	 * @return true, if the checksum was correct.
	 */
	bool verify_checksum(const IPv6Header&);
};

/**
 * ICMPv6 Router Advertisement header structure (RFC 4861)
 *
 * This structure is guaranteed to have a memory layout compatible with
 * a captured Router Advertisement packet header and thus can be used with
 * PacketBufferView::next<>().
 */
struct [[gnu::may_alias]] RouterAdvertisementHeader {
	uint8_t ra_hop_limit;
	uint8_t ra_flags;
	uint16_t ra_router_lifetime;
	uint32_t ra_reachable_time;
	uint32_t ra_retrans_timer;

	uint8_t hop_limit() {
		return ra_hop_limit;
	}
	void hop_limit(uint8_t limit) {
		ra_hop_limit = limit;
	}
	uint8_t flags() {
		return ra_flags;
	}
	void flags(uint8_t flags) {
		ra_flags = flags;
	}
	uint16_t router_lifetime() {
		return ntohs(ra_router_lifetime);
	}
	void router_lifetime(uint16_t seconds) {
		ra_router_lifetime = htons(seconds);
	}
	uint32_t reachable_time() {
		return ntohl(ra_reachable_time);
	}
	void reachable_time(uint32_t ms) {
		ra_reachable_time = htonl(ms);
	}
	uint32_t retrans_timer() {
		return ntohl(ra_retrans_timer);
	}
	void retrans_timer(uint32_t ms) {
		ra_retrans_timer = htonl(ms);
	}
};

/**
 * ICMPv6 Neighbor Discovery Option header structure (RFC 4861)
 *
 * This structure is guaranteed to have a memory layout compatible with
 * the header fields of a captured ND Option and thus can be used with
 * e.g. PacketBufferView::peek<>() and PacketBufferView::next<>().
 */
struct [[gnu::may_alias]] NDOption {
	uint8_t opt_type;
	uint8_t opt_length;

	uint8_t code() const {
		return opt_type;
	}
	void type(uint8_t code) {
		opt_type = code;
	}
	uint16_t length() const {
		return opt_length*8;
	}
	void length(uint16_t bytes) {
		if ((bytes < 8) || (bytes > 2040) || (bytes % 8))
			throw std::invalid_argument("invalid option length");
		opt_length = bytes/8;
	}
};

/**
 * Neighbor Discovery Option "Link-layer Address" structure (RFC 4861)
 *
 * This structure is guaranteed to have a memory layout compatible with
 * a captured ND Option of type 1 or 2 and thus can be used with
 * e.g. PacketBufferView::next<>().
 */
struct [[gnu::may_alias]] NDLinkLayerAddress: NDOption {
	uint8_t lla_addr[6];
};

/**
 * Neighbor Discovery Option "Prefix Information" structure (RFC 4861)
 *
 * This structure is guaranteed to have a memory layout compatible with
 * a captured ND Option of type 3 and thus can be used with
 * e.g. PacketBufferView::next<>().
 */
struct [[gnu::may_alias]] NDPrefixInformation: NDOption {
	uint8_t pi_prefix_length;
	uint8_t pi_flags;
	uint32_t pi_valid_lifetime;
	uint32_t pi_preferred_lifetime;
	uint32_t pi_reserved;
	IPv6Address pi_prefix;

	uint8_t prefix_length() const {
		return pi_prefix_length;
	}
	void prefix_length(uint8_t bits) {
		pi_prefix_length = bits;
	}
	uint8_t flags() const {
		return pi_flags;
	}
	void flags(uint8_t flags) {
		pi_flags = flags;
	}
	uint32_t valid_lifetime() const {
		return ntohl(pi_valid_lifetime);
	}
	void valid_lifetime(uint32_t sec) {
		pi_valid_lifetime = htonl(sec);
	}
	uint32_t preferred_lifetime() const {
		return ntohl(pi_preferred_lifetime);
	}
	void preferred_lifetime(uint32_t sec) {
		pi_preferred_lifetime = htonl(sec);
	}
	const IPv6Address &prefix() const {
		return pi_prefix;
	}
	IPv6Address &prefix() {
		return pi_prefix;
	}
};

/**
 * Neighbor Discovery Option "Route Information" structure (RFC 4191)
 *
 * This structure is guaranteed to have a memory layout compatible with
 * a captured ND Option of type 24 and thus can be used with
 * e.g. PacketBufferView::next<>().
 */
struct [[gnu::may_alias]] NDRouteInformation: NDOption {
	uint8_t pi_prefix_length;
	uint8_t pi_flags;
	uint32_t pi_route_lifetime;
	IPv6Address pi_prefix;

	uint8_t prefix_length() const {
		return pi_prefix_length;
	}
	void prefix_length(uint8_t bits) {
		pi_prefix_length = bits;
	}
	uint8_t flags() const {
		return pi_flags;
	}
	void flags(uint8_t flags) {
		pi_flags = flags;
	}
	uint32_t route_lifetime() const {
		return ntohl(pi_route_lifetime);
	}
	void route_lifetime(uint32_t sec) {
		pi_route_lifetime = htonl(sec);
	}
	const IPv6Address &prefix() const {
		return pi_prefix;
	}
	IPv6Address &prefix() {
		return pi_prefix;
	}
};

/* IPv4 */

/**
 * IPv4 packet header (RFC 791)
 *
 * This structure is guaranteed to have a memory layout compatible with
 * a captured IPv4 header and thus can be used with
 * e.g. PacketBufferView::next<>().
 */
struct [[gnu::may_alias]] IPv4Header {
	uint8_t		iph_v_ihl;
	uint8_t		iph_tos;
	uint16_t	iph_length;
	uint16_t	iph_id;
	uint16_t	iph_flags_fragment;
	uint8_t		iph_ttl;
	uint8_t		iph_protocol;
	uint16_t	iph_checksum;
	in_addr iph_source;
	in_addr iph_destination;

	uint8_t version() const {
		return iph_v_ihl >> 4;
	}
	void version(uint8_t version) {
		iph_v_ihl = (iph_v_ihl & 0x0F) | ((version & 0xF) << 4);
	}
	uint8_t header_length() const {
		return (iph_v_ihl & 0xF)*4;
	}
	void header_length(uint8_t ihl) {
		if ((ihl % 4) || (ihl < 20) || (ihl > 60))
			throw std::invalid_argument("invalid ipv4 header length");
		iph_v_ihl = ((iph_v_ihl & 0xF0) | (ihl & 0xF))/4;
	}
	uint8_t tos() const {
		return iph_tos;
	}
	void tos(uint8_t tos) {
		iph_tos = tos;
	}
	uint16_t total_length() const {
		return htons(iph_length);
	}
	void total_length(uint16_t length) {
		iph_length = htons(length);
	}
	uint16_t identification() const {
		return htons(iph_id);
	}
	void identification(uint16_t id) {
		iph_id = htons(id);
	}
	uint8_t flags() const {
		return ntohs(iph_flags_fragment) >> 13;
	}
	void flags(uint8_t flags) {
		iph_flags_fragment = htons((ntohs(iph_flags_fragment) & 0x1FFF) | (flags<<13));
	}
	uint16_t fragment_offset() const {
		return (ntohs(iph_flags_fragment) & 0x1FFF) * 8;
	}
	void fragment_offset(uint16_t offset) {
		if ((offset % 8) || (offset > 65528))
			throw std::invalid_argument("invalid ipv4 fragment offset");
		iph_flags_fragment = htons((ntohs(iph_flags_fragment) & 0xE000) | (offset/8));
	}
	uint8_t ttl() {
		return iph_ttl;
	}
	void ttl(uint8_t ttl) {
		iph_ttl = ttl;
	}
	uint8_t protocol() {
		return iph_protocol;
	}
	void protocol(uint8_t code) {
		iph_protocol = code;
	}
	uint16_t checksum() const {
		return ntohs(iph_checksum);
	}
	void checksum(uint16_t csum) {
		iph_checksum = htons(csum);
	}
	uint16_t _calc_checksum();
	void set_checksum();
	bool verify_checksum();
	const in_addr& source() const {
		return iph_source;
	}
	in_addr& source() {
		return iph_source;
	}
	const in_addr& destination() const {
		return iph_destination;
	}
	in_addr& destination() {
		return iph_destination;
	}
};

/* TCP */

/**
 * TCP packet header (RFC 793)
 *
 * This structure is guaranteed to have a memory layout compatible with
 * a captured TCP header and thus can be used with
 * e.g. PacketBufferView::next<>().
 */
struct [[gnu::may_alias]] TCPHeader {
	uint16_t	tcp_sport;
	uint16_t	tcp_dport;
	uint32_t	tcp_seq;
	uint32_t	tcp_ack;
	uint8_t		tcp_doff;
	uint8_t		tcp_flags;
	uint16_t	tcp_window;
	uint16_t	tcp_checksum;
	uint16_t	tcp_urgptr;

	uint16_t source_port() const {
		return ntohs(tcp_sport);
	}
	void source_port(uint16_t port) {
		tcp_sport = htons(port);
	}
	uint16_t destination_port() const {
		return ntohs(tcp_dport);
	}
	void destination_port(uint16_t port) {
		tcp_dport = htons(port);
	}
	uint32_t seq() const {
		return ntohl(tcp_seq);
	}
	void seq(uint32_t n) {
		tcp_seq = htonl(n);
	}
	uint32_t ack() const {
		return ntohl(tcp_ack);
	}
	void ack(uint32_t n) {
		tcp_ack = htonl(n);
	}
	uint8_t data_offset() const {
		return tcp_doff>>4;
	}
	void data_offset(uint8_t n) {
		tcp_doff = n<<4;
	}
	uint8_t flags() const {
		return tcp_flags;
	}
	void flags(uint8_t flags) {
		tcp_flags = flags;
	}
	uint16_t window() const {
		return ntohs(tcp_window);
	}
	void window(uint16_t w) {
		tcp_window = htons(w);
	}
	uint16_t checksum() const {
		return ntohs(tcp_checksum);
	}
	void checksum(uint16_t csum) {
		tcp_checksum = htons(csum);
	}
	uint16_t urgptr() const {
		return ntohs(tcp_urgptr);
	}
	void urgptr(uint16_t n) {
		tcp_urgptr = htons(n);
	}
	uint16_t _calc_checksum(const IPv6Header&);
	uint16_t _calc_checksum(const IPv4Header&);
	void set_checksum(const IPv6Header&);
	void set_checksum(const IPv4Header&);
	bool verify_checksum(const IPv6Header&);
	bool verify_checksum(const IPv4Header&);
};

/* Assert struct sizes */

static_assert(sizeof(IPv6Header)==40, "Wrong alignment in IPv6Header struct");
static_assert(sizeof(ICMPv6Header)==4, "Wrong alignment in ICMPv6Header struct");
static_assert(sizeof(RouterAdvertisementHeader)==12, "Wrong alignment in RouterAdvertisementHeader struct");
static_assert(sizeof(NDLinkLayerAddress)==8, "Wrong alignment in NDLinkLayerAddress struct");
static_assert(sizeof(NDPrefixInformation)==32, "Wrong alignment in NDPrefixInformation struct");
static_assert(sizeof(NDRouteInformation)==24, "Wrong alignment in NDRouteInformation struct");
static_assert(sizeof(IPv4Header)==20, "Wrong alignment in IPv4Header struct");
static_assert(sizeof(TCPHeader)==20, "Wrong alignment in TCPHeader struct");

#endif
/**@}*/
