/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************/

/**
 * @addtogroup common Common helper functions and classes
 * @{
 * @file
 * Socket wrappers
 */

#ifndef _SOCKET_H
#define _SOCKET_H

#include "packetbuffer.h"

#include <sys/types.h>
#include <vector>
#include <string>
#include <sys/socket.h>
#include <stdexcept>

/**
 * Default socket send flags
 *
 * Includes MSG_NOSIGNAL if supported by the OS, else zero.
 */
#ifdef MSG_NOSIGNAL
#define SOCKET_DEFAULT_SENDFLAGS MSG_NOSIGNAL
#else
#define SOCKET_DEFAULT_SENDFLAGS 0
#endif

/**
 * Exception thrown when the connection was closed while receiving/sending.
 */
class connection_closed: public std::runtime_error
{
public:
	connection_closed(const char *what)
		: std::runtime_error(what) {}
};

/**
 * Exception thrown when the connection was reset while receiving/sending.
 */
class connection_reset: public connection_closed
{
public:
	connection_reset(const char *what)
		: connection_closed(what) {}
};

/**
 * Socket wrapper class
 */
class Socket {
	int fd;
protected:
	size_t recv(void* buffer, size_t maxlen, int flags=0);
	size_t recvfrom(void* buffer, size_t len, int flags, struct sockaddr &addr, socklen_t addrlen);
	size_t sendto(const void* buffer, size_t len, int flags, const struct sockaddr &addr, socklen_t addrlen);
	void bind(const struct sockaddr &, int len);
	void connect(const struct sockaddr &, int len);
	Socket accept(struct sockaddr &, socklen_t &);
	void getsockname(struct sockaddr &, socklen_t &) const;
	void getsockopt(int level, int optname, void *optval, socklen_t *optlen) const;
	void setsockopt(int level, int optname, const void *optval, socklen_t optlen);
public:
	/**
	 * Constructs an uninitialized socket wrapper
	 */
	Socket() noexcept;

	/**
	 * Constructs and initialize a socket wrapper
	 *
	 * @param domain communication domain/address family (e.g. AF_INET6)
	 * @param type socket type specifying the communication semantics (e.g. SOCK_STREAM)
	 * @param protocol particular protocol to be used (e.g. IPPROTO_TCP or 0 for autoselection)
	 */
	Socket(int domain, int type, int protocol);

	/**
	 * Constructs an socket wrapper from an open file descriptor
	 *
	 * @param fd file descriptor number
	 * @throws std::system_error if the fd doesn't refer to a socket
	 */
	explicit Socket(int fd);

	/**
	 * *Deleted constructor template*
	 *
	 * Prevents implicit type conversion, e.g. from float to int, so that
	 * Socket::Socket(int fd) is only acceptable for int arguments.
	 */
	template <typename T>
	Socket(T) = delete;

	/**
	 * *Deleted copy constructor*
	 *
	 * Prevents the copying of instances to couple the existence of
	 * the underlying socket to the lifetime of the instance.
	 */
	Socket(const Socket&) = delete;

	/**
	 * Move constructor
	 *
	 * Allows moving an instance with std::move() or through a function return value.
	 */
	Socket(Socket&&) noexcept;

	/**
	 * Destructor
	 *
	 * Closes the underlying socket if it isn't already closed.
	 */
	virtual ~Socket() noexcept;

	/**
	 * Move operator
	 *
	 * Replaces this socket with another. May only be used if this
	 * socket is uninitialized or closed.
	 *
	 * @throws std::logic_error if the socket to move to is open
	 */
	Socket& operator = (Socket&&);

	/**
	 * Opens an uninitialized socket
	 *
	 * @param domain communication domain/address family (e.g. AF_INET6)
	 * @param type socket type specifying the communication semantics (e.g. SOCK_STREAM)
	 * @param protocol particular protocol to be used (e.g. IPPROTO_TCP or 0 for autoselection)
	 * @throws std::logic_error if the socket is already open
	 */
	void open(int domain, int type, int protocol);

	/**
	 * Closes the underlying socket if it isn't already closed.
	 */
	void close() noexcept;

	/**
	 * Shuts down all or part of a full-duplex connection
	 *
	 * @param how SHUT_RDWR, SHUT_RD or SHUT_WR
	 * @throws std::system_error
	 */
	void shutdown(int how = SHUT_RDWR);

	/**
	 * Initiates a connection
	 *
	 * @param addr a sockaddr-like struct describing the destination address
	 * @throws std::system_error
	 */
	template<typename T>
	void connect(const T &addr);

	/**
	 * Binds a name (address)
	 *
	 * @param addr a sockaddr-like struct describing the source address
	 * @throws std::system_error
	 */
	template<typename T>
	void bind(const T &addr);

	/**
	 * Sends a message
	 *
	 * @param buffer pointer to the data to send
	 * @param len length of data
	 * @param flags flags modifying the send process
	 * @return count of bytes sent
	 * @throws std::system_error
	 */
	size_t send(const void* buffer, size_t len, int flags=SOCKET_DEFAULT_SENDFLAGS);

	/**
	 * @details @overload
	 *
	 * @param buffer view to the data to send
	 * @param more sets the MSG_MORE flag
	 * @return count of bytes sent
	 * @throws std::system_error
	 */
	size_t send(const PacketBufferView& buffer, bool more=false);

	/**
	 * @details @overload
	 *
	 * @param buffer view to the data to send
	 * @param more sets the MSG_MORE flag
	 * @return count of bytes sent
	 * @throws std::system_error
	 */
	size_t send(const ConstPacketBufferView& buffer, bool more=false);

	/**
	 * @details @overload
	 *
	 * @param buffer string to send
	 * @param more sets the MSG_MORE flag
	 * @return count of bytes sent
	 * @throws std::system_error
	 */
	size_t send(const std::string& buffer, bool more=false);

	/**
	 * Sends a message to a specific recipient
	 *
	 * @param buffer view to the data to send
	 * @param addr a sockaddr-like struct describing the destination address
	 * @return count of bytes sent
	 * @throws std::system_error
	 */
	template<typename T>
	size_t sendto(const PacketBufferView& buffer, const T &addr);

	/**
	 * @details @overload
	 */
	template<typename T>
	size_t sendto(const ConstPacketBufferView& buffer, const T &addr);

	/**
	 * Receives a message into a buffer
	 *
	 * If size is zero, the buffer is resized before receiving
	 * to its capacity or `offset`, whichever is greater.
	 *
	 * If a size is given, the buffer is resized to offset+size before receiving.
	 *
	 * After receiving the buffer is resized to accommodate the received data.
	 * If the received octet count was not a multiple of sizeof(T) the buffer size
	 * is rounded down, so surplus data is lost.
	 *
	 * @param[out] buffer buffer to write into
	 * @param size expected size in sizeof(T)-increments
	 * @param offset offset into buffer in sizeof(T)-increments
	 * @return true if data was received
	 * @throws std::system_error
	 */
	template<typename T>
	bool recv(std::vector<T> &buffer, size_t size = 0, size_t offset = 0);

	/**
	 * Receives a message into a buffer and records the sender address
	 *
	 * If size is zero, the buffer is resized before receiving
	 * to its capacity or `offset`, whichever is greater.
	 *
	 * If a size is given, the buffer is resized to offset+size before receiving.
	 *
	 * After receiving the buffer is resized to accommodate the received data.
	 * If the received octet count was not a multiple of sizeof(T) the buffer size
	 * is rounded down, so surplus data is lost.
	 *
	 * @tparam T1 sockaddr-like struct type for the expected sender address
	 * @param[out] buffer buffer to write into
	 * @param size expected size in sizeof(T)-increments
	 * @param offset offset into buffer in sizeof(T)-increments
	 * @return {true if data was received, sender address}
	 * @throws std::system_error
	 */
	template<typename T1, typename T2>
	std::pair<bool, T1> recvfrom(std::vector<T2> &buffer, size_t size = 0, size_t offset = 0);

	/**
	 * Listens for connections
	 *
	 * Marks the socket as a passive socket. Only useful for SOCK_STREAM or
	 * SOCK_SEQPACKET sockets.
	 *
	 * @param backlog maximum length to which the queue of pending connections may grow
	 * @throws std::system_error
	 */
	void listen(int backlog);

	/**
	 * Accepts a connection
	 *
	 * @tparam T sockaddr-like struct type for the expected sender address
	 * @return {connection socket, sender address}
	 * @throws std::system_error
	 */
	template<typename T>
	std::pair<Socket, T> accept();

	/**
	 * Gets the domain/address family of the socket
	 *
	 * @return domain code
	 * @throws std::system_error
	 */
	[[gnu::pure]]
	unsigned int domain() const;

	/**
	 * Gets the socket type of the socket
	 *
	 * @return type code
	 * @throws std::system_error
	 */
	[[gnu::pure]]
	unsigned int type() const;

	/**
	 * Gets the protocol of the socket
	 *
	 * @return protocol code
	 * @throws std::system_error
	 */
	[[gnu::pure]]
	unsigned int protocol() const;

	/**
	 * Gets the file descriptor of the socket
	 *
	 * @return file descriptor number or -1 if uninitialized/closed
	 */
	[[gnu::pure]]
	int fileno() const {
		return fd;
	}

	/**
	 * Makes the socket blocking/non-blocking
	 *
	 * @throws std::system_error
	 */
	void set_blocking(bool value);

	/**
	 * Sets/unsets the close-on-exec flag
	 *
	 * @throws std::system_error
	 */
	void set_cloexec(bool value);

	/**
	 * Join a multicast group
	 *
	 * @param addr IPv6 multicast address
	 * @param iface_index interface index for link-local multicast groups
	 * @throws std::system_error
	 */
	void join_group(const in6_addr &addr, unsigned int iface_index=0);

	/**
	 * Leave a multicast group
	 *
	 * @param addr IPv6 multicast address
	 * @param iface_index interface index for link-local multicast groups
	 * @throws std::system_error
	 */
	void leave_group(const in6_addr &addr, unsigned int iface_index=0);

	/**
	 * Gets the socket name (the address the socket is bound to)
	 *
	 * @tparam T sockaddr-like struct type for the expected address
	 * @return socket name
	 * @throws std::system_error
	 */
	template<typename T>
	T getsockname() const;

	/**
	 * Gets a socket option
	 *
	 * @param level the level the option resides at (SOL_SOCKET, IPPROTO_IPV6, IPPROTO_TCP, etc.)
	 * @param optname the option code
	 * @param[out] optval buffer of appropriate size to write the option value to
	 * @throws std::system_error
	 */
	template<typename T>
	void getsockopt(int level, int optname, T &optval) const;

	/**
	 * @details @overload
	 *
	 * @param level the level the option resides at (SOL_SOCKET, IPPROTO_IPV6, IPPROTO_TCP, etc.)
	 * @param optname the option code
	 * @param[out] optval buffer of appropriate size to write the option value to
	 * @param[in,out] optlen as input: maximum size to write; as output: the actual data size
	 * @throws std::system_error
	 * @throws std::invalid_argument if `optlen` is bigger than `optval` size
	 */
	template<typename T>
	void getsockopt(int level, int optname, T &optval, socklen_t &optlen) const;

	/**
	 * @details @overload
	 *
	 * Convenience method for unsigned int socket options.
	 *
	 * @param level the level the option resides at (SOL_SOCKET, IPPROTO_IPV6, IPPROTO_TCP, etc.)
	 * @param optname the option code
	 * @return the option value
	 * @throws std::system_error
	 */
	unsigned int getsockopt(int level, int optname) const;

	/**
	 * Sets a socket option
	 *
	 * @param level the level the option resides at (SOL_SOCKET, IPPROTO_IPV6, IPPROTO_TCP, etc.)
	 * @param optname the option code
	 * @param optval[out] the option value
	 * @throws std::system_error
	 */
	template<typename T>
	void setsockopt(int level, int optname, const T &optval);

	/**
	 * Converts an interface name to an interface index
	 *
	 * @param ifname the interface name (e.g. "eth0")
	 * @return interface index as an unsigned integer
	 * @throws std::system_error
	 */
	static unsigned int if_nametoindex(const std::string &ifname);

	/**
	 * Converts an interface index to an interface name
	 *
	 * @param ifname the interface index
	 * @return interface name as a string
	 * @throws std::system_error
	 */
	static std::string if_indextoname(unsigned int index);
};

template<typename T>
bool Socket::recv(std::vector<T>& buffer, size_t size, size_t offset)
{
	if (size == 0) {
		size = std::max(buffer.capacity(), offset);
		buffer.resize(size);
		size -= offset;
	} else
		buffer.resize(offset+size);

	size_t result = recv(&buffer.at(offset), sizeof(T)*size);
	buffer.resize(offset + (result / sizeof(T)));
	return result;
}

template<typename T1, typename T2>
std::pair<bool, T1> Socket::recvfrom(std::vector<T2> &buffer, size_t size, size_t offset)
{
	if (size == 0) {
		size = std::max(buffer.capacity(), offset);
		buffer.resize(size);
		size -= offset;
	} else
		buffer.resize(offset+size);

	T1 sockaddr;
	zero_init(sockaddr);
	size_t result = recvfrom(&buffer.at(offset), sizeof(T2)*size, 0, reinterpret_cast<struct sockaddr &>(sockaddr), sizeof(T1));
	buffer.resize(offset + (result / sizeof(T2)));
	return std::make_pair(result, std::move(sockaddr));
}

template<typename T>
size_t Socket::sendto(const PacketBufferView& buffer, const T &addr)
{
	return sendto(buffer.pointer(), buffer.size(), 0, reinterpret_cast<const struct sockaddr &>(addr), sizeof(T));
}

template<typename T>
size_t Socket::sendto(const ConstPacketBufferView& buffer, const T &addr)
{
	return sendto(buffer.pointer(), buffer.size(), 0, reinterpret_cast<const struct sockaddr &>(addr), sizeof(T));
}

template<typename T>
void Socket::bind(const T &addr)
{
	bind(reinterpret_cast<const struct sockaddr &>(addr), sizeof(T));
}

template<typename T>
void Socket::connect(const T &addr)
{
	connect(reinterpret_cast<const struct sockaddr &>(addr), sizeof(T));
}

template<typename T>
std::pair<Socket, T> Socket::accept()
{
	T addr;
	socklen_t addrlen = sizeof(T);
	zero_init(addr);
	Socket socket = accept(reinterpret_cast<struct sockaddr &>(addr), addrlen);
	if (addrlen > sizeof(T))
		throw std::invalid_argument("address type was too small");
	return std::make_pair(std::move(socket), std::move(addr));
}

template<typename T>
inline T Socket::getsockname() const
{
	T result;
	zero_init(result);
	socklen_t len = sizeof(T);
	getsockname(reinterpret_cast<struct sockaddr &>(result), len);
	if (len > sizeof(T))
		throw std::invalid_argument("address type was too small");
	return result;
}

template<typename T>
void Socket::setsockopt(int level, int optname, const T& optval)
{
	setsockopt(level, optname, &optval, sizeof(T));
}

template<typename T>
void Socket::getsockopt(int level, int optname, T& optval, socklen_t &optlen) const
{
	if (optlen > sizeof(T))
		throw std::invalid_argument("optlen argument is bigger than optval size");
	getsockopt(level, optname, &optval, &optlen);
}

template<typename T>
void Socket::getsockopt(int level, int optname, T& optval) const
{
	socklen_t len = sizeof(T);
	getsockopt(level, optname, &optval, &len);
}

inline unsigned int Socket::getsockopt(int level, int optname) const
{
	int i = 0;
	socklen_t len = sizeof(i);
	getsockopt(level, optname, &i, &len);
	return i;
}

/**
 * @details @overload
 *
 * Specialization for strings.
 *
 * @param level the level the option resides at (SOL_SOCKET, IPPROTO_IPV6, IPPROTO_TCP, etc.)
 * @param optname the option code
 * @param optval[out] string to assign the read value to
 * @throws std::system_error
 */
template<>
void Socket::getsockopt(int level, int optname, std::string& optval) const;

/**
 * @details @overload
 *
 * Specialization for boolean option values. They are internally handled
 * as ints.
 *
 * @param level the level the option resides at (SOL_SOCKET, IPPROTO_IPV6, IPPROTO_TCP, etc.)
 * @param optname the option code
 * @param optval boolean option value
 * @throws std::system_error
 */
template<>
inline void Socket::setsockopt(int level, int optname, const bool &optval)
{
	int val = optval;
	setsockopt(level, optname, &val, sizeof(int));
}

/**
 * @details @overload
 *
 * Specialization for strings.
 *
 * @param level the level the option resides at (SOL_SOCKET, IPPROTO_IPV6, IPPROTO_TCP, etc.)
 * @param optname the option code
 * @param optval string option value
 * @throws std::system_error
 */
template<>
void Socket::setsockopt(int level, int optname, const std::string &optval);

#endif
/**@}*/
