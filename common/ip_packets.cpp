/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************
 * IP packet structs and helper methods.                                  *
 ****************************************************************************/

#include "ip_packets.h"
#include "util.h"

#include <cstring>
#include <arpa/inet.h>

static void _ipv6_calc_checksum_pseudo(uint32_t &total, const IPv6Header &header, uint16_t protocol)
{
	int i;
	const uint16_t* ptr;
	// Pseudo-header: source
	ptr = reinterpret_cast<const uint16_t*>(&header.source());
	for (i = 0; i < 8; ++i) {
		total += ntohs(*ptr++);
	}
	// Pseudo-header: destination
	ptr = reinterpret_cast<const uint16_t*>(&header.destination());
	for (i = 0; i < 8; ++i) {
		total += ntohs(*ptr++);
	}
	// Pseudo-header: size
	total += header.payload_length();
	// Pseudo-header: protocol
	total += protocol;
}

static void _ipv4_calc_checksum_pseudo(uint32_t &total, const IPv4Header &header, uint16_t protocol)
{
	int i;
	const uint16_t* ptr;
	// Pseudo-header: source
	ptr = reinterpret_cast<const uint16_t*>(&header.source());
	for (i = 0; i < 2; ++i) {
		total += ntohs(*ptr++);
	}
	// Pseudo-header: destination
	ptr = reinterpret_cast<const uint16_t*>(&header.destination());
	for (i = 0; i < 2; ++i) {
		total += ntohs(*ptr++);
	}
	// Pseudo-header: protocol
	total += protocol;
	// Pseudo-header: size
	total += header.total_length()-header.header_length();
}

uint16_t ICMPv6Header::_calc_checksum(const IPv6Header &header)
{
	uint32_t total = 0;
	uint32_t words = header.payload_length()/2;
	uint16_t tmp;
	bool odd = header.payload_length()%2;
	
	// Pseudo-header
	_ipv6_calc_checksum_pseudo(total, header, IPPROTO_ICMPV6);
	
	// Packet
	const uint16_t* ptr;
	ptr = reinterpret_cast<const uint16_t*>(this);
	while (words--) {
		total += ntohs(*ptr++);
	}
	if (odd) {
		tmp = *reinterpret_cast<const uint8_t*>(ptr);
		total += ntohs(tmp);
	}
	// Fold carry bits
	total = (total & 0xFFFF) + (total >> 16);
	total = (total & 0xFFFF) + (total >> 16);
	return ~total;
}

void ICMPv6Header::set_checksum(const IPv6Header &header) {
	ic6_checksum = 0;
	checksum(_calc_checksum(header));
}

bool ICMPv6Header::verify_checksum(const IPv6Header &header) {
	return _calc_checksum(header) == 0;
}

uint16_t IPv4Header::_calc_checksum()
{
	uint32_t total = 0;
	uint32_t words = header_length()/2;
	
	// Packet
	const uint16_t* ptr;
	ptr = reinterpret_cast<const uint16_t*>(this);
	while (words--) {
		total += ntohs(*ptr++);
	}
	// Fold carry bits
	total = (total & 0xFFFF) + (total >> 16);
	total = (total & 0xFFFF) + (total >> 16);
	return ~total;
}

void IPv4Header::set_checksum() {
	iph_checksum = 0;
	checksum(_calc_checksum());
}

bool IPv4Header::verify_checksum() {
	return _calc_checksum() == 0;
}

uint16_t TCPHeader::_calc_checksum(const IPv6Header &header)
{
	uint32_t total = 0;
	uint32_t words = header.payload_length()/2;
	uint16_t tmp;
	bool odd = header.payload_length()%2;
	
	// Pseudo-header
	_ipv6_calc_checksum_pseudo(total, header, IPPROTO_TCP);
	
	// Packet
	const uint16_t* ptr;
	ptr = reinterpret_cast<const uint16_t*>(this);
	while (words--) {
		total += ntohs(*ptr++);
	}
	if (odd) {
		tmp = *reinterpret_cast<const uint8_t*>(ptr);
		total += ntohs(tmp);
	}
	// Fold carry bits
	total = (total & 0xFFFF) + (total >> 16);
	total = (total & 0xFFFF) + (total >> 16);
	return ~total;
}

uint16_t TCPHeader::_calc_checksum(const IPv4Header &header)
{
	uint32_t total = 0;
	uint32_t words = (header.total_length()-header.header_length())/2;
	uint16_t tmp;
	bool odd = (header.total_length()-header.header_length())%2;
	
	// Pseudo-header
	_ipv4_calc_checksum_pseudo(total, header, IPPROTO_TCP);
	
	// Packet
	const uint16_t* ptr;
	ptr = reinterpret_cast<const uint16_t*>(this);
	while (words--) {
		total += ntohs(*ptr++);
	}
	if (odd) {
		tmp = *reinterpret_cast<const uint8_t*>(ptr);
		total += ntohs(tmp);
	}
	// Fold carry bits
	total = (total & 0xFFFF) + (total >> 16);
	total = (total & 0xFFFF) + (total >> 16);
	return ~total;
}

void TCPHeader::set_checksum(const IPv6Header &header) {
	tcp_checksum = 0;
	checksum(_calc_checksum(header));
}

void TCPHeader::set_checksum(const IPv4Header &header) {
	tcp_checksum = 0;
	checksum(_calc_checksum(header));
}

bool TCPHeader::verify_checksum(const IPv6Header &header) {
	return _calc_checksum(header) == 0;
}

bool TCPHeader::verify_checksum(const IPv4Header &header) {
	return _calc_checksum(header) == 0;
}
