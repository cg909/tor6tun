#include "privdrop.h"
#include "logging.h"
#include "util.h"

#include <grp.h>
#include <pwd.h>
#include <unistd.h>
#ifdef LINUXCAPS
#  include <sys/capability.h>
#endif

static void getpwnam(std::string &user, uid_t &uid, gid_t &gid)
{
	// get by username
	errno = 0;
	struct passwd *data = getpwnam(user.c_str());
	if (!data) {
		// try get by uid
		size_t i = 0;
		try {
			uid = std::stoul(user, &i);
			if (i != user.length())
				throw std::invalid_argument("not a uid");
			data = getpwuid(uid);
		} catch (std::invalid_argument &exc) {}
	}
	if (!data) {
		if (errno == 0)
			errno = ENOENT;
		throw_errno("could not resolve username/uid");
	}
	uid = data->pw_uid;
	gid = data->pw_gid;
	user = data->pw_name;
}

void drop_privileges(const std::string &u)
{
	std::string user = u;
	uid_t uid = 0;
	gid_t gid = 0;
	if (user.length()) {
		getpwnam(user, uid, gid);
	} else {
		uid = getuid();
		gid = getgid();
	}
	if (gid != 0) {
		// reset supplementary groups if effective GID differs from target GID
		if (getegid() != gid) {
			log::debug("resetting supplementary group list because EGID (", getegid(), ") differs from GID (", gid, ")");
			if (initgroups(user.c_str(), gid))
				throw_errno("initgroups() failed");
		}
		// Set GID
		log::debug("setting GID");
		if (setresgid(gid, gid, gid))
			throw_errno("setresgid() failed");
	}
	if (uid != 0) {
		// Set UID
		log::debug("setting UID");
		if (setresuid(uid, uid, uid))
			throw_errno("setresuid() failed");
	}

	#ifdef LINUXCAPS
	cap_t caps = cap_init();
	if (!caps)
		throw std::runtime_error("cap_init() failed");
	cap_t current_caps = cap_get_proc();
	if (!current_caps)
		throw_errno("cap_get_proc() failed");
	if (cap_compare(current_caps, caps) != 0) {
		log::debug("dropping linux capabilities");
		int result = cap_set_proc(caps);
		cap_free(caps);
		if (result)
			throw_errno("cap_set_proc() failed");
	}
	if (getuid() == 0) {
		log::warn("running as root - capablities were dropped, but this may still be insecure");
	}
	#else
	if (getuid() == 0) {
		log::warn("running as root - this is insecure");
	}
	#endif
	
}

/* Legacy daemonization */
void daemonize(bool nochdir, bool noclose)
{
	log::debug("detaching");
	if (::daemon(nochdir, noclose)) {
		throw_errno("daemon() failed");
	}
}
