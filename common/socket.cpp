/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************
 * Socket wrapper class.                                                    *
 ****************************************************************************/

#include "socket.h"
#include "util.h"

#include <stdexcept>
#include <unistd.h>
#include <fcntl.h>
#include <net/if.h>

Socket::Socket() noexcept
	: fd(-1)
{}

Socket::Socket(int domain, int type, int protocol=0)
	: fd(-1)
{
	open(domain, type, protocol);
}

void Socket::open(int domain, int type, int protocol=0)
{
	if (fd != -1)
		throw std::logic_error("socket already open");
	fd = socket(domain, type, protocol);
	if (fd == -1)
		throw_errno("socket() failed");
}

Socket::Socket(int fd)
	: fd(fd)
{
	int value = 0;
	socklen_t value_size = sizeof(value);
	if (::getsockopt(fd, SOL_SOCKET, SO_DOMAIN, &value, &value_size) == -1)
	{
		if (errno == ENOTSOCK or errno == EBADF) {
			throw_errno("cannot construct Socket instance from non-socket fd", errno);
		} else {
			throw_errno("getsockopt() failed when constructing Socket instance from fd", errno);
		}
	}
}

Socket::Socket(Socket &&o) noexcept
	: fd(o.fd)
{
	o.fd = -1;
}

Socket::~Socket() noexcept
{
	if (fd != -1) {
		::close(fd);
		fd = -1;
	}
}

void Socket::close() noexcept
{
	if (fd != -1) {
		::close(fd);
		fd = -1;
	}
}

Socket& Socket::operator = (Socket &&other)
{
	if (fd != -1)
		throw std::logic_error("socket already open");
	fd = other.fd;
	other.fd = -1;
	return *this;
}

void Socket::shutdown(int how)
{
	if (::shutdown(fd, how) < 0)
		throw_errno("shutdown() failed");
}

size_t Socket::recv(void* buffer, size_t maxlen, int flags)
{
	ssize_t result;
	do {
		result = ::recv(fd, buffer, maxlen, flags);
	} while (result == -1 and errno == EINTR);
	
	if (result == -1) {
		if (errno == EAGAIN || errno == EWOULDBLOCK) {
			return 0;
		} else if (errno == ECONNRESET) {
			throw connection_reset("ECONNRESET on recv()");
		} else
			throw_errno("recv() failed");
	}
	
	if (result == 0)
		throw connection_closed("EOF on recv()");
	
	return result;
}

size_t Socket::recvfrom(void* buffer, size_t maxlen, int flags, struct sockaddr &addr, socklen_t addrlen)
{
	ssize_t result;
	socklen_t real_addrlen = addrlen;
	
	result = ::recvfrom(fd, buffer, maxlen, flags, &addr, &real_addrlen);
	
	if (real_addrlen > addrlen)
		throw std::invalid_argument("address type too small");
	
	if (result == -1) {
		if (errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR) {
			return 0;
		} else if (errno == ECONNRESET) {
			throw connection_reset("ECONNRESET on recvfrom()");
		} else
			throw_errno("recvfrom() failed");
	}
	
	if (result == 0)
		throw connection_closed("EOF on recvfrom()");
	
	return result;
}


size_t Socket::send(const void* buffer, size_t len, int flags)
{
	ssize_t result;
	do {
		result = ::send(fd, buffer, len, flags);
	} while (result == -1 and errno == EINTR);
	
	if (result == -1) {
		if (errno == EAGAIN) {
			return 0;
		} else if (errno == EPIPE) {
			throw connection_closed("EPIPE on send()");
		} else if (errno == ECONNRESET) {
			throw connection_reset("ECONNRESET on send()");
		} else
			throw_errno("send() failed");
	}
	return result;
}

size_t Socket::send(const PacketBufferView& buffer, bool more)
{
	return send(buffer.pointer(), buffer.size(), SOCKET_DEFAULT_SENDFLAGS|(more?MSG_MORE:0));
}

size_t Socket::send(const ConstPacketBufferView& buffer, bool more)
{
	return send(buffer.pointer(), buffer.size(), SOCKET_DEFAULT_SENDFLAGS|(more?MSG_MORE:0));
}

size_t Socket::send(const std::string& buffer, bool more)
{
	return send(buffer.data(), buffer.length(), SOCKET_DEFAULT_SENDFLAGS|(more?MSG_MORE:0));
}

size_t Socket::sendto(const void* buffer, size_t len, int flags, const struct sockaddr &addr, socklen_t addrlen)
{
	return ::sendto(fd, buffer, len, flags, &addr, addrlen);
}

unsigned int Socket::domain() const
{
	return getsockopt(SOL_SOCKET, SO_DOMAIN);
}

unsigned int Socket::type() const
{
	return getsockopt(SOL_SOCKET, SO_TYPE);
}

unsigned int Socket::protocol() const
{
	return getsockopt(SOL_SOCKET, SO_PROTOCOL);
}

void Socket::bind(const struct sockaddr &addr, int len)
{
	if (::bind(fd, &addr, len) == -1)
		throw_errno("bind() failed");
}

void Socket::connect(const struct sockaddr &addr, int len)
{
	if (::connect(fd, &addr, len) == -1)
		throw_errno("connect() failed");
}

Socket Socket::accept(struct sockaddr &addr, socklen_t &addrlen)
{
	int result = ::accept(fd, &addr, &addrlen);
	if (result == -1)
		throw_errno("accept() failed");
	return Socket(result);
}

void Socket::set_blocking(bool value)
{
	int flags = fcntl(fd, F_GETFL);
	if (flags == -1)
		throw_errno("F_GETFL failed");
	if (!value)
		flags |= O_NONBLOCK;
	else
		flags &= ~O_NONBLOCK;
	if (fcntl(fd, F_SETFL, flags) == -1)
		throw_errno("F_SETFL failed");
}

void Socket::set_cloexec(bool value)
{
	int flags = fcntl(fd, F_GETFD);
	if (flags == -1)	
		throw_errno("F_GETFD failed");
	if (value)
		flags |= FD_CLOEXEC;
	else
		flags &= ~FD_CLOEXEC;
	if (fcntl(fd, F_SETFD, flags) == -1)
		throw_errno("F_SETFD failed");
}

void Socket::getsockname(struct sockaddr &addr, socklen_t &addrlen) const
{
	if (::getsockname(fd, &addr, &addrlen) == -1)
		throw_errno("getsockname() failed");
}

void Socket::getsockopt(int level, int optname, void *optval, socklen_t *optlen) const
{
	if (::getsockopt(fd, level, optname, optval, optlen) == -1)
		throw_errno("getsockopt() failed");
}

void Socket::setsockopt(int level, int optname, const void *optval, socklen_t optlen)
{
	if (::setsockopt(fd, level, optname, optval, optlen) == -1)
		throw_errno("setsockopt() failed");
}

template<>
void Socket::setsockopt(int level, int optname, const std::string &optval)
{
	setsockopt(level, optname, optval.data(), optval.length());
}

template<>
void Socket::getsockopt(int level, int optname, std::string& optval) const
{
	socklen_t len = optval.length();
	std::vector<char> buffer;
	buffer.resize(len);
	getsockopt(level, optname, buffer.data(), &len);
	if (len > buffer.size())
		len = buffer.size();
	optval = std::string(buffer.data(), len);
}

void Socket::listen(int backlog)
{
	if (::listen(fd, backlog) == -1)
		throw_errno("listen() failed");
}

void Socket::join_group(const in6_addr &addr, unsigned int iface_index)
{
	struct ipv6_mreq mreq {
		addr,
		iface_index
	};
	setsockopt(IPPROTO_IPV6, IPV6_JOIN_GROUP, mreq);
}

void Socket::leave_group(const in6_addr &addr, unsigned int iface_index)
{
	struct ipv6_mreq mreq {
		addr,
		iface_index
	};
	setsockopt(IPPROTO_IPV6, IPV6_LEAVE_GROUP, mreq);
}

unsigned int Socket::if_nametoindex(const std::string &ifname)
{
	unsigned int result = ::if_nametoindex(ifname.c_str());
	if (!result)
		throw_errno("if_nametoindex() failed");
	return result;
}

std::string Socket::if_indextoname(unsigned int ifindex)
{
	char buffer[IF_NAMESIZE+1];
	buffer[IF_NAMESIZE] = '\0';
	if (!::if_indextoname(ifindex, buffer))
		throw_errno("if_indextoname() failed");
	return buffer;
}