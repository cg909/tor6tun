/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************
 * C++ style getaddrinfo wrappers.                                          *
 ****************************************************************************/

#include "getaddrinfo.h"
#include "util.h"

#include <stdexcept>
#include <sys/socket.h>

static class gai_error_category_class final: public std::error_category
{
public:
	gai_error_category_class() = default;
	gai_error_category_class(const gai_error_category_class&) = delete;
	gai_error_category_class& operator = (const gai_error_category_class&) = delete;

	[[gnu::pure]]
	virtual const char *name() const noexcept
	{
		return "gai";
	}

	[[gnu::pure]]
	virtual std::string message(int condition) const noexcept
	{
		return gai_strerror(condition);
	}
} gai_error_category;

const std::error_category& gai_category()
{
	return gai_error_category;
}

AddrInfoResultIterator& AddrInfoResultIterator::operator ++()
{
	ptr = ptr->ai_next;
	return *this;
}
AddrInfoResultIterator AddrInfoResultIterator::operator ++(int)
{
	AddrInfoResultIterator copy = *this;
	ptr = ptr->ai_next;
	return copy;
}

AddrInfoResult::~AddrInfoResult()
{
	if (data) {
		freeaddrinfo(data);
		data = nullptr;
	}
}

AddrInfoResult::AddrInfoResult(AddrInfoResult &&other)
{
	if (data)
		freeaddrinfo(data);
	data = other.data;
	other.data = nullptr;
}

static AddrInfoResult _getaddrinfo(const char *node, const char *service, const struct addrinfo *hints)
{
	struct addrinfo *result = nullptr;
	int error = ::getaddrinfo(node, service, hints, &result);
	if (error or result == nullptr) {
		throw std::system_error(error, gai_error_category, "getaddrinfo() failed");
	} else {
		return AddrInfoResult(result);
	}
	
}

AddrInfoResult getaddrinfo(const std::string &node, const std::string &service)
{
	return _getaddrinfo(node.c_str(), service.c_str(), nullptr);
}

AddrInfoResult getaddrinfo(const std::string &node, const std::string &service, int flags, int family, int socktype, int protocol)
{
	struct addrinfo hints = {flags, family, socktype, protocol, 0, nullptr, nullptr, nullptr};
	return _getaddrinfo(node.c_str(), service.c_str(), &hints);
}

AddrInfoResult getaddrinfo(const std::string &node_service)
{
	std::string node = node_service;
	std::string service;
	size_t i = node.rfind(':');
	size_t j = node.find(']');
	if (i != std::string::npos and (j == std::string::npos or i == j+1)) {
		service = node.substr(i+1);
		if (j != std::string::npos)
			node = node.substr(1, i-2);
		else
			node.resize(i-1);
	}
	if (!service.length())
		throw std::invalid_argument("no port specified");
	return _getaddrinfo(node.c_str(), service.c_str(), nullptr);
}

AddrInfoResult getaddrinfo(const std::string &node_service, int flags, int family, int socktype, int protocol)
{
	struct addrinfo hints = {flags, family, socktype, protocol, 0, nullptr, nullptr, nullptr};
	std::string node = node_service;
	std::string service;
	auto i = node.rfind(':');
	auto j = node.find(']');
	if (i != std::string::npos and (j == std::string::npos or i == j+1)) {
		service = node.substr(i+1);
		if (j != std::string::npos)
			node = node.substr(1, i-2);
		else
			node.resize(i);
	}
	if (!service.length())
		throw std::invalid_argument("no port specified");
	return _getaddrinfo(node.c_str(), service.c_str(), &hints);
}

