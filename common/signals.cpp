/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************
 * C++-style sigaction() wrappers.                                          *
 ****************************************************************************/

#include "signals.h"

#include <cstring>

signalset::signalset(const sigset_t &sigset) noexcept
{
	std::memcpy(&sigs, &sigset, sizeof(sigset_t));
}

struct sigaction sigaction(int signum)
{
	int result;
	struct sigaction action;
	zero_init(action);
	
	result = ::sigaction(signum, nullptr, &action);
	if (result == -1)
		throw_errno("sigaction() failed");
	return action;
}

void sigaction(std::initializer_list<int> signums, const struct sigaction &action)
{
	int result;
	
	for (int signum: signums) {
		result = ::sigaction(signum, &action, nullptr);
		if (result == -1)
			throw_errno("sigaction() failed");
	}
}

void sigaction(std::initializer_list<int> signums, void (*handler)(int), const signalset &sig_mask, int flags)
{
	struct sigaction action;
	zero_init(action);
	action.sa_handler = handler;
	std::memcpy(&action.sa_mask, &sig_mask.sigs, sizeof(sigset_t));
	action.sa_flags = flags & (~SA_SIGINFO);
	sigaction(signums, action);
}

void sigaction(std::initializer_list<int> signums, void (*handler)(int, siginfo_t *, void *), const signalset &sig_mask, int flags)
{
	struct sigaction action;
	zero_init(action);
	action.sa_sigaction = handler;
	std::memcpy(&action.sa_mask, &sig_mask.sigs, sizeof(sigset_t));
	action.sa_flags = flags | SA_SIGINFO;
	sigaction(signums, action);
}

