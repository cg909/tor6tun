#include "base32.h"
#include <iostream>
#include <stdexcept>
#include <cstring>

[[gnu::const]]
static inline uint8_t ctzb(uint8_t v)
{
	return __builtin_ctz(v);
}

[[gnu::const]]
static inline uint8_t popcountb(uint8_t v)
{
	return __builtin_popcount(v);
}

static const char base32_table[] = "abcdefghijklmnopqrstuvwxyz234567";

std::string b32encode(const void *ptr, size_t size)
{
	const uint8_t *p = static_cast<const uint8_t*>(ptr);
	uint8_t mask1 = 0xF8, mask2 = 0, tmp, next;
	std::string result;
	result.reserve((size*160+99)/100);
	while (size > 0) {
		next = (*p & mask1) >> ctzb(mask1) << (5-popcountb(mask1));
		if (size > 1 && mask2) {
				next |= (*(p+1) & mask2) >> ctzb(mask2);
		}
		result.push_back(base32_table[next]);
		if ((mask1 >> 5) == 0) {
			std::swap(mask1, mask2);
			--size;
			++p;
		}
		// Shift masks
		tmp = mask1;
		mask1 = (mask1 >> 5) | (mask2 << (8 - 5));
		mask2 = (mask2 >> 5) | (tmp << (8 - 5));
	}
	return result;
}

size_t b32decode(const std::string &input, void *dest, size_t destsize)
{
	uint8_t *p = static_cast<uint8_t*>(dest);
	uint8_t mask1 = 0xF8, mask2 = 0, tmp;
	if (!dest)
		throw std::invalid_argument("null pointer");
	if ((input.length()*100+99)/160 > destsize)
		throw std::out_of_range("buffer too small");
	std::memset(dest, 0, destsize);
	for (uint8_t c: input) {
		if (c >= 'A' and c <= 'Z')
			c -= 'A';
		else if (c >= 'a' and c <= 'z')
			c -= 'a';
		else if (c >= '2' and c <= '7')
			c -= '2' - 26;
		else {
			throw std::invalid_argument("not valid base32");
		}

		*p |= (c >> (5-popcountb(mask1))) << ctzb(mask1);

		if (mask2) {
			std::swap(mask1, mask2);
			--destsize;
			++p;
			if (!destsize)
				throw std::out_of_range("buffer too small");

			*p |= ((c << ctzb(mask1)) & mask1);
		}
		// Shift masks
		tmp = mask1;
		mask1 = (mask1 >> 5) | (mask2 << (8 - 5));
		mask2 = (mask2 >> 5) | (tmp << (8 - 5));
	}
	
	return 0;
}
