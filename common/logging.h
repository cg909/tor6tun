/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************/

/**
 * @addtogroup common Common helper functions and classes
 * @{
 * @file
 * Logging functions
 *
 */

#ifndef _LOGGING_H
#define _LOGGING_H

#include "util.h"

/**
 * Simple logging framework
 *
 * Currently all logging consists of writing the log lines to std::clog.
 * The prefixes are given by log::_debug, etc. and default to "debug: ", etc.
 * except when running under systemd. Then the prefixes are chosen so that
 * journald understands the severity.
 */
namespace log {

	/**
	 * @internal
	 * @{
	 */
	extern const char *_debug, *_info, *_warning, *_err, *_crit;
	/**
	 * @}
	 */

	/**
	 * Possible logging levels
	 */
	extern enum loglevels: uint_fast8_t {
		CRITICAL = 1, /**< critical errors @see log::critical() */
		ERROR, /**< errors @see log::error(), log::error_errno() */
		WARNING, /**< warnings @see log::warn() */
		INFO, /**< informational @see log::info() */
		DEBUG /**< debugging information @see log::debug() */
	} loglevel;
	/**<
	 * Current logging level
	 *
	 * Everything less critical than the set log level will not be logged.
	 *
	 * Usually defaults to log::INFO
	 */

	/**
	 * Log debugging information
	 */
	template<typename... Types>
	inline void debug(Types... rest)
	{
		if (__builtin_expect(loglevel >= DEBUG, 0))
			print(std::clog, _debug, rest...);
	}

	/**
	 * Log notable information
	 */
	template<typename... Types>
	inline void info(Types... rest)
	{
		if (loglevel >= INFO)
			print(std::clog, _info, rest...);
	}

	/**
	 * Log a warning
	 */
	template<typename... Types>
	inline void warn(Types... rest)
	{
		if (loglevel >= WARNING)
			print(std::cerr, _warning, rest...);
	}

	/**
	 * Log an error
	 */
	template<typename... Types>
	[[gnu::cold]]
	inline void error(Types... rest)
	{
		if (loglevel >= ERROR)
			print(std::cerr, _err, rest...);
	}

	/**
	 * Log a critical error
	 */
	template<typename... Types>
	[[gnu::cold]]
	inline void critical(Types... rest)
	{
		if (loglevel >= CRITICAL)
			print(std::cerr, _crit, rest...);
	}

	/**
	 * Log an error and append the error code and description of *errno*
	 */
	template<typename... Types>
	[[gnu::cold]]
	inline void error_errno(Types... rest)
	{
		if (loglevel >= ERROR) {
			auto error = std::error_code(errno, std::generic_category());
			log::error(rest..., ": ", error.message(), " [", error.value(), "]");
		}
	}

}

#endif
/**@}*/
