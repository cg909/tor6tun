/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************/

/**
 * @addtogroup common Common helper functions and classes
 * @{
 * @file
 * Utility functions
 */

#ifndef _UTIL_H
#define _UTIL_H

#include <string>
#include <cstring>
#include <iostream>
#include <exception>
#include <system_error>
#include <arpa/inet.h>

namespace {
	inline void print_nonl() {}
	inline void print_nonl(std::ostream &) {}

	template<typename T, typename... Types>
	inline void print_nonl (std::ostream &stream, T first, Types... rest)
	{
		stream << first;
		print_nonl(stream, rest...);
	}

	template<typename... Types>
	inline void print_nonl (Types... rest)
	{
		print_nonl(std::cout, rest...);
	}
}

/**
 * Print data to an output stream
 *
 * The outputs of multiple arguments will be concatenated
 *
 * @param stream Output stream
 * @param rest
 */
template<typename... Types>
void print (std::ostream &stream, Types... rest)
{
	print_nonl(stream, rest...);
	stream << std::endl;
}

/**
 * @overload
 * Print data to stdout
 */
template<typename... Types>
void print (Types... rest)
{
	print_nonl(std::cout, rest...);
	std::cout << std::endl;
}

/**
 * Throw an exception containing the current errno
 *
 * @param msg error message
 * @throws std::system_error
 */
[[noreturn, gnu::cold]]
inline void throw_errno(const std::string& msg)
{
	throw std::system_error(errno, std::generic_category(), msg);
}

/**
 * Throw an exception containing an errno
 *
 * @overload
 * @param msg error message
 * @param error_number errno-style error code
 * @throws std::system_error
 */
[[noreturn, gnu::cold]]
inline void throw_errno(const std::string& msg, int error_number)
{
	throw std::system_error(error_number, std::generic_category(), msg);
}

/**
 * Zero-initialize a variable
 *
 * @param target standard-layout, non-pointer variable to overwrite
 * @throws std::system_error
 */
template<typename T>
inline T& zero_init(T &target)
{
	static_assert(std::is_standard_layout<T>::value or !std::is_polymorphic<T>::value,
		"Cannot zero-init polymorphic objects");
	static_assert(!std::is_pointer<T>::value, "Won't zero-init pointers");

	std::memset(&target, 0, sizeof(T));
	return target;
}

/**
 * Zero-initialize an array
 *
 * @overload
 * @param target array of standard-layout variables to overwrite
 * @throws std::system_error
 */
template<typename T, int N>
inline T (&zero_init(T (&target)[N]))[N]
{
	static_assert(std::is_standard_layout<T>::value,
		"Cannot zero-init arrays of polymorphic objects");

	std::memset(&target, 0, sizeof(T)*N);
	return target;
}


#endif
/**@}*/
