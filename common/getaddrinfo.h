/****************************************************************************
 *   Copyright (c) 2016 by Christoph Grenz                                  *
 *   christophg@grenz-bonn.de                                               *
 *                                                                          *
 * This program is free software; you can redistribute it and/or            *
 * modify it under the terms of the GNU General Public License as           *
 * published by the Free Software Foundation; either version 3 of           *
 * the License, or (at your option) any later version.                      *
 *                                                                          *
 * This program is distributed in the hope that it will be useful,          *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        *
 * General Public License for more details.                                 *
 *                                                                          *
 * You should have received a copy of the GNU General Public License        *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                          *
 ****************************************************************************/

/**
 * @addtogroup common Common helper functions and classes
 * @{
 * @file
 * getaddrinfo() wrapper
 */

#ifndef _GETADDRINFO_H
#define _GETADDRINFO_H

#include <string>
#include <system_error>
#include <netdb.h>

struct addrinfo;

/**
 * error category singleton for gai-related errors
 */
[[gnu::pure]][[gnu::cold]]
const std::error_category& gai_category ();

/**
 * Iterator type of AddrInfoResult
 *
 * Also known as AddrInfoResult::iterator
 */
class AddrInfoResultIterator final {
	struct addrinfo *ptr;
public:
	/** @internal */
	explicit constexpr AddrInfoResultIterator(): ptr(nullptr) {}
	/** @internal */
	explicit constexpr AddrInfoResultIterator(struct addrinfo *ptr): ptr(ptr) {}

	/**
	 * Iterator dereference operator
	 *
	 * @return reference to current addrinfo struct
	 */
	constexpr const struct addrinfo &operator *() const { return *ptr; }

	/**
	 * @overload
	 */
	struct addrinfo &operator *() { return *ptr; }

	/**
	 * Validity check
	 *
	 * @return true if this is a valid iterator
	 */
	explicit constexpr operator bool() const { return ptr != nullptr; }

	/**
	 * Iterate to the next element in the result list
	 *
	 * @attention This MUST NOT be called on an iterator at the end of the list
	 * or for which conversion to bool returns false.
	 */
	AddrInfoResultIterator& operator ++();
	/** @overload */
	AddrInfoResultIterator operator ++(int);

	/**
	 * Checks if the other iterator points to the same point in the result list
	 */
	constexpr bool operator ==(const AddrInfoResultIterator &other) const
	{ return other.ptr == ptr; }

	/**
	 * Checks if the other iterator doesn't point to the same point in the result list
	 */
	constexpr bool operator !=(const AddrInfoResultIterator &other) const
	{ return other.ptr != ptr; }
};

/**
 * getaddrinfo() result class
 *
 * Forward-iterable object that yields addrinfo structs in a way compatible to
 * range-based for loops.
 *
 * It is non-copyable, but movable.
 */
class AddrInfoResult final {
public:
	typedef AddrInfoResultIterator iterator;
private:
	static constexpr const iterator null_it {};
	struct addrinfo *data;
public:
	/**
	 * @internal
	 * @brief Constructs the result from a ::getaddrinfo() call
	 */
	[[gnu::nonnull]]
	explicit constexpr AddrInfoResult(struct addrinfo *ptr): data(ptr) {}
	/**
	 * Move constructor
	 *
	 * Allows moving an instance with std::move() or through a function return value.
	 */
	AddrInfoResult(AddrInfoResult &&other);
	/**
	 * *Deleted copy constructor*
	 *
	 * Prevents the copying of instances to couple the existence of
	 * the underlying result list to the lifetime of the instance.
	 */
	AddrInfoResult(const AddrInfoResult &) = delete;
	/**
	 * Destructor
	 *
	 * Deletes the result list using ::freeaddrinfo()
	 */
	~AddrInfoResult();
	/**
	 * Returns an iterator to the start of the result list.
	 */
	iterator begin() const { return iterator {data}; }
	/**
	 * Returns an iterator that marks the end of a result list.
	 */
	const iterator& end() const { return null_it; }
};

/**
 * Translates internet host and service name to adresses and ports
 *
 * @relatesalso AddrInfoResult
 * @param node host name or address
 * @param service service name or number
 * @return iterable list of addrinfo structs
 */
AddrInfoResult getaddrinfo(const std::string &node, const std::string &service);

/**
 * @details @overload
 * @relatesalso AddrInfoResult
 * @param node host name or address
 * @param service service name or number
 * @param flags additional options
 * @param family desired address family (AF_INET/AF_INET6)
 * @param socktype desired socket type (SOCK_STREAM/...)
 * @param protocol desired protocol (IPPROTO_TCP/...)
 */
AddrInfoResult getaddrinfo(const std::string &node, const std::string &service, int flags, int family=AF_UNSPEC, int socktype=0, int protocol=0);

/**
 * @details @overload
 * @relatesalso AddrInfoResult
 * @param node_service host and service separated by a colon
 */
AddrInfoResult getaddrinfo(const std::string &node_service);

/**
 * @details @overload
 * @relatesalso AddrInfoResult
 * @param node_service host and service separated by a colon
 * @param flags additional options
 * @param family desired address family (AF_INET/AF_INET6)
 * @param socktype desired socket type (SOCK_STREAM/...)
 * @param protocol desired protocol (IPPROTO_TCP/...)
 */
AddrInfoResult getaddrinfo(const std::string &node_service, int flags, int family=AF_UNSPEC, int socktype=0, int protocol=0);

#endif
/**@}*/
